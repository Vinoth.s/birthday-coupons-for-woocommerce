<?php

/**
 * Admin Ajax.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
if ( ! class_exists( 'BCN_Admin_Ajax' ) ) {

	/**
	 * Class.
	 */
	class BCN_Admin_Ajax {

		/**
		 *  Class initialization.
		 */
		public static function init() {

			$actions = array(
				'product_search'     => false ,
				'customers_search'   => false ,
								'remove_unsubscriber'=> false ,
				'additional_details' => true
					) ;

			foreach ( $actions as $action => $nopriv ) {
				add_action( 'wp_ajax_bcn_' . $action , array( __CLASS__ , $action ) ) ;

				if ( $nopriv ) {
					add_action( 'wp_ajax_nopriv_bcn_' . $action , array( __CLASS__ , $action ) ) ;
				}
			}
		}

		/**
		 * Product search.
		 */
		public static function product_search() {
			check_ajax_referer( 'bcn-search-nonce' , 'bcn_security' ) ;

			try {
				$term = isset( $_GET[ 'term' ] ) ? ( string ) wc_clean( wp_unslash( $_GET[ 'term' ] ) ) : '' ;

				if ( empty( $term ) ) {
					throw new exception( esc_html__( 'No Product(s) found' , 'birthday-coupons-for-woocommerce' ) ) ;
				}

				$data_store = WC_Data_Store::load( 'product' ) ;
				$ids        = $data_store->search_products( $term , '' , true , false , 30 ) ;

				$product_objects = array_filter( array_map( 'wc_get_product' , $ids ) , 'wc_products_array_filter_readable' ) ;
				$products        = array() ;

				foreach ( $product_objects as $product_object ) {
					$products[ $product_object->get_id() ] = rawurldecode( $product_object->get_formatted_name() ) ;
				}
				wp_send_json( $products ) ;
			} catch ( Exception $ex ) {
				wp_die() ;
			}
		}

		/**
		 * Customer search.
		 */
		public static function customers_search() {
			check_ajax_referer( 'bcn-search-nonce' , 'bcn_security' ) ;

			try {
				$term = isset( $_GET[ 'term' ] ) ? ( string ) wc_clean( wp_unslash( $_GET[ 'term' ] ) ) : '' ;

				if ( empty( $term ) ) {
					throw new exception( esc_html__( 'No Customer(s) found' , 'birthday-coupons-for-woocommerce' ) ) ;
				}

				$exclude = isset( $_GET[ 'exclude' ] ) ? ( string ) wc_clean( wp_unslash( $_GET[ 'exclude' ] ) ) : '' ;
				$exclude = ! empty( $exclude ) ? array_map( 'intval' , explode( ',' , $exclude ) ) : array() ;

				$found_customers = array() ;

				$customers_query = new WP_User_Query( array(
					'fields'         => 'all' ,
					'orderby'        => 'display_name' ,
					'search'         => '*' . $term . '*' ,
					'search_columns' => array( 'ID' , 'user_login' , 'user_email' , 'user_nicename' )
						) ) ;

				$customers = $customers_query->get_results() ;

				if ( bcn_check_is_array( $customers ) ) {
					foreach ( $customers as $customer ) {
						if ( ! in_array( $customer->ID , $exclude ) ) {
							$found_customers[ $customer->ID ] = $customer->display_name . ' (#' . $customer->ID . ' &ndash; ' . sanitize_email( $customer->user_email ) . ')' ;
						}
					}
				}

				wp_send_json( $found_customers ) ;
			} catch ( Exception $ex ) {
				wp_die() ;
			}
		}
				
				/**
		 * Remove Unsubscriber.
		 */
		public static function remove_unsubscriber() {
			check_ajax_referer( 'bcn-unsubscribe-nonce' , 'bcn_security' ) ;

			try {
				if ( ! isset( $_POST[ 'unsubid' ] ) ) {
					throw new exception( esc_html__( 'Invalid Request' , 'birthday-coupons-for-woocommerce' ) ) ;
				}

				$unsubid = absint( $_POST[ 'unsubid' ] ) ;

				bcn_delete_unsubscriber( $unsubid ) ;

				wp_send_json_success() ;
			} catch ( Exception $ex ) {
				wp_send_json_error( array( 'error' => $ex->getMessage() ) ) ;
			}
		}

		/**
		 * Display the additional details for Coupon.
		 */
		public static function additional_details() {
			check_ajax_referer( 'bcn-info-nonce' , 'bcn_security' ) ;

			try {
				if ( ! isset( $_POST[ 'post_id' ] ) ) {
					throw new exception( esc_html__( 'Invalid Request' , 'birthday-coupons-for-woocommerce' ) ) ;
				}

				$coupon_id  = absint( $_POST[ 'post_id' ] ) ;
				$coupon_obj = bcn_get_coupon_details( $coupon_id ) ;

				$html = bcn_get_template_html(
						'popup-layout.php' , array( 'coupon_obj' => $coupon_obj )
						) ;

				wp_send_json_success( array( 'html' => $html ) ) ;
			} catch ( Exception $ex ) {
				wp_send_json_error( array( 'error' => $ex->getMessage() ) ) ;
			}
		}

	}

	BCN_Admin_Ajax::init() ;
}

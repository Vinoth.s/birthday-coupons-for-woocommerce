<?php

/**
 * Enqueue Admin Assets Files.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
if ( ! class_exists( 'BCN_Admin_Assets' ) ) {

	/**
	 * BCN_Admin_Assets Class.
	 */
	class BCN_Admin_Assets {

		/**
		 * BCN_Admin_Assets Class Initialization.
		 */
		public static function init() {
			add_action( 'admin_enqueue_scripts' , array( __CLASS__ , 'external_js_files' ) ) ;
			add_action( 'admin_enqueue_scripts' , array( __CLASS__ , 'external_css_files' ) ) ;
		}

		/**
		 * Enqueue external css files.
		 */
		public static function external_css_files() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;

			$screen_ids   = bcn_page_screen_ids() ;
			$newscreenids = get_current_screen() ;
			$screenid     = str_replace( 'edit-' , '' , $newscreenids->id ) ;

			if ( ! in_array( $screenid , $screen_ids ) ) {
				return ;
			}

			// Lightcase CSS.
			wp_enqueue_style( 'bcn-lightcase' , BCN_PLUGIN_URL . '/assets/css/lightcase' . $suffix . '.css' , array() , BCN_VERSION ) ;
			wp_enqueue_style( 'bcn-admin' , BCN_PLUGIN_URL . '/assets/css/admin.css' , array() , BCN_VERSION ) ;

			self::add_inline_style() ;
			/**
			 * After enqueue css.
			 * 
			 * @since 1.0
			 */
			do_action( 'bcn_admin_after_enqueue_css' ) ;
		}

		/**
		 * Add Inline Style.
		 */
		public static function add_inline_style() {
			$contents = '' ;

			wp_add_inline_style( 'bcn-admin' , $contents ) ;
		}

		/**
		 * Enqueue Admin end required JS files.
		 */
		public static function external_js_files() {
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;

			$screen_ids   = bcn_page_screen_ids() ;
			$newscreenids = get_current_screen() ;
			$screenid     = str_replace( 'edit-' , '' , $newscreenids->id ) ;

			$enqueue_array = array(
				'bcn-toggle'    => array(
					'callable' => array( 'BCN_Admin_Assets' , 'toggle_section' ) ,
					'restrict' => in_array( $screenid , $screen_ids ) ,
				) ,
				'bcn-select2'   => array(
					'callable' => array( 'BCN_Admin_Assets' , 'select2' ) ,
					'restrict' => in_array( $screenid , $screen_ids ) ,
				) ,
				'bcn-lightcase' => array(
					'callable' => array( 'BCN_Admin_Assets' , 'enqueue_lightcase' ) ,
					'restrict' => true ,
				) ,
					) ;
			/**
			 * Admin scripts.
			 * 
			 * @since 1.0
			 */
			$enqueue_array = apply_filters( 'bcn_admin_enqueue_scripts' , $enqueue_array ) ;
			if ( ! bcn_check_is_array( $enqueue_array ) ) {
				return ;
			}

			foreach ( $enqueue_array as $key => $enqueue ) {
				if ( ! bcn_check_is_array( $enqueue ) ) {
					continue ;
				}

				if ( $enqueue[ 'restrict' ] ) {
					call_user_func_array( $enqueue[ 'callable' ] , array( $suffix ) ) ;
				}
			}
			/**
			 * After enqueue js.
			 * 
			 * @since 1.0
			 */
			do_action( 'bcn_admin_after_enqueue_js' ) ;
		}

		/**
		 * Enqueue Section Toggle scripts.
		 */
		public static function toggle_section() {
			wp_enqueue_script( 'bcn-admin' , BCN_PLUGIN_URL . '/assets/js/admin.js' , array( 'jquery' ) , BCN_VERSION ) ;
			wp_localize_script(
					'bcn-admin' , 'bcn_admin_param' , array(
				'ajaxurl'           => BCN_ADMIN_AJAX_URL ,
				'rule_nonce'        => wp_create_nonce( 'bcn-rule-nonce' ) ,
				'delete_rule'       => esc_html__( 'Are you sure, do you want to delete this rule?' , 'birthday-coupons-for-woocommerce' ) ,
				'unsubscribe_nonce' => wp_create_nonce( 'bcn-unsubscribe-nonce' ) ,
				'unsubscribe_alert' => esc_html__( 'Are you sure, do you want to delete this user?' , 'birthday-coupons-for-woocommerce' ) ,
					)
			) ;
		}

		/**
		 * Enqueue select2 scripts.
		 */
		public static function select2() {
			wp_enqueue_script( 'bcn-enhanced' , BCN_PLUGIN_URL . '/assets/js/bcn-enhanced.js' , array( 'jquery' , 'select2' , 'jquery-ui-datepicker' ) , BCN_VERSION ) ;
			wp_localize_script(
					'bcn-enhanced' , 'bcn_enhanced_select_params' , array(
				'search_nonce' => wp_create_nonce( 'bcn-search-nonce' ) ,
				'ajaxurl'      => BCN_ADMIN_AJAX_URL
					)
			) ;
		}

		/**
		 * Enqueue Lightcase.
		 */
		public static function enqueue_lightcase() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;
			// Lightcase.
			wp_register_script( 'lightcase' , BCN_PLUGIN_URL . '/assets/js/lightcase' . $suffix . '.js' , array( 'jquery' ) , BCN_VERSION ) ;
			// Enhanced lightcase.
			wp_enqueue_script( 'bcn-lightcase' , BCN_PLUGIN_URL . '/assets/js/bcn-lightcase-enhanced.js' , array( 'jquery' , 'jquery-blockui' , 'lightcase' ) , BCN_VERSION ) ;
			wp_localize_script(
					'bcn-lightcase' , 'bcn_lightcase_param' , array(
				'bcn_info_nonce' => wp_create_nonce( 'bcn-info-nonce' ) ,
				'ajaxurl'        => BCN_ADMIN_AJAX_URL
					)
			) ;
		}

	}

	BCN_Admin_Assets::init() ;
}

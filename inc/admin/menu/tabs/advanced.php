<?php

/**
 * Advanced Tab
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( class_exists( 'BCN_Advacned_Tab' ) ) {
	return new BCN_Advacned_Tab() ;
}

/**
 * BCN_Advacned_Tab.
 */
class BCN_Advacned_Tab extends BCN_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id           = 'advanced' ;
		$this->label        = esc_html__( 'Advanced' , 'birthday-coupons-for-woocommerce' ) ;

		parent::__construct() ;
	}

	/**
	 * Get Advanced Settings section array.
	 */
	public function advanced_section_array() {
		$section_fields = array() ;

		//Email Section Start.
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_start' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Custom CSS' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_custom_css' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Custom CSS' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'custom_css' ) ,
			'type'    => 'textarea' ,
			'default' => '' ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_custom_css' ,
				) ;
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_end' ,
				) ;
		//Email Section End.

		return $section_fields ;
	}

}

return new BCN_Advacned_Tab() ;

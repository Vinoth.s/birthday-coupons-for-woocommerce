<?php

/**
 * General Tab
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( class_exists( 'BCN_General_Tab' ) ) {
	return new BCN_General_Tab() ;
}

/**
 * BCN_General_Tab.
 */
class BCN_General_Tab extends BCN_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'general' ;
		$this->label = esc_html__( 'General' , 'birthday-coupons-for-woocommerce' ) ;

		parent::__construct() ;
	}

	/**
	 * Get settings for general section array.
	 */
	public function general_section_array() {
		$section_fields = array() ;

		// General Section Start
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'General Settings' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_general_options' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Enable Birthday Reward Coupon For Guest Users' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'checkbox' ,
			'default' => 'no' ,
			'desc'    => 'By enabling this checkbox, guest users can enter their birthday to receive a coupon.' ,
			'id'      => $this->get_option_key( 'guest_coupon' ) ,
				) ;
				$section_fields[] = array(
			'title'   => esc_html__( 'Enable Birthday Reward Coupon for Logged-in Users' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'checkbox' ,
			'default' => 'no' ,
			'desc'    => 'By enabling this checkbox, logged-in users can enter their birthday to receive a coupon.' ,
			'id'      => $this->get_option_key( 'logged_in_user_coupon' ) ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Make Field Mandatory', 'birthday-coupons-for-woocommerce' ),
				'type'    => 'checkbox',
				'default' => 'no',
				'desc'    => esc_html__( 'Enable Required Field', 'birthday-coupons-for-woocommerce' ),
				'id'      => $this->get_option_key( 'mandatory_field' ),
				);
				$section_fields[] = array(
				'type' => 'sectionend' ,
				'id'   => 'bcn_general_options' ,
				) ;
				$section_fields[] = array(
				'type'  => 'title' ,
				'title' => esc_html__( 'Birthday Coupon Settings' , 'birthday-coupons-for-woocommerce' ) ,
				'id'    => 'bcn_coupon_options' ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Coupon Code Type' , 'birthday-coupons-for-woocommerce' ) ,
				'type'    => 'select' ,
				'default' => '1' ,
				'options' => array(
				'1' => esc_html__( 'Numeric' , 'birthday-coupons-for-woocommerce' ) ,
				'2' => esc_html__( 'Alphanumeric' , 'birthday-coupons-for-woocommerce' ) ,
				) ,
				'id'      => $this->get_option_key( 'coupon_code_type' ) ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Coupon Prefix' , 'birthday-coupons-for-woocommerce' ) ,
				'type'    => 'text' ,
				'default' => 'bcn' ,
				'id'      => $this->get_option_key( 'coupon_prefix' ) ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Coupon Suffix' , 'birthday-coupons-for-woocommerce' ) ,
				'type'    => 'text' ,
				'default' => 'bcn' ,
				'id'      => $this->get_option_key( 'coupon_suffix' ) ,
				) ;
				$section_fields[] = array(
				'title'             => esc_html__( 'Coupon Length' , 'birthday-coupons-for-woocommerce' ) ,
				'type'              => 'number' ,
				'default'           => '12' ,
				'custom_attributes' => array(
				'min' => '1' ,
				) ,
				'id'                => $this->get_option_key( 'coupon_length' ) ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Send Birthday Coupon' , 'birthday-coupons-for-woocommerce' ) ,
				'type'    => 'select' ,
				'default' => '1' ,
				'options' => array(
				'1' => esc_html__( 'On Birthday Date' , 'birthday-coupons-for-woocommerce' ) ,
				'2' => esc_html__( 'Before Birthday Date' , 'birthday-coupons-for-woocommerce' ) ,
				'3' => esc_html__( 'After Birthday Date' , 'birthday-coupons-for-woocommerce' ) ,
				) ,
				'id'      => $this->get_option_key( 'send_coupon_on' ) ,
				) ;
				$section_fields[] = array(
				'title'             => esc_html__( 'Send Birthday Coupon before' , 'birthday-coupons-for-woocommerce' ) ,
				'type'              => 'number' ,
				'default'           => '' ,
				'custom_attributes' => array(
				'min' => '1' ,
				) ,
				'id'                => $this->get_option_key( 'send_coupon_before_birthday' ) ,
				) ;
				$section_fields[] = array(
				'title'             => esc_html__( 'Send Birthday Coupon after' , 'birthday-coupons-for-woocommerce' ) ,
				'type'              => 'number' ,
				'default'           => '' ,
				'custom_attributes' => array(
				'min' => '1' ,
				) ,
				'id'                => $this->get_option_key( 'send_coupon_after_birthday' ) ,
				) ;
				$section_fields[] = array(
				'type' => 'sectionend' ,
				'id'   => 'bcn_coupon_options' ,
				) ;
				$section_fields[] = array(
				'type'  => 'title' ,
				'title' => esc_html__( 'Birthday Rules Settings' , 'birthday-coupons-for-woocommerce' ) ,
				'id'    => 'bcn_coupon_rules_options' ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Rule Priority' , 'birthday-coupons-for-woocommerce' ) ,
				'type'    => 'select' ,
				'default' => '1' ,
				'options' => array(
				'1' => esc_html__( 'First Matched Rule' , 'birthday-coupons-for-woocommerce' ) ,
				'2' => esc_html__( 'Last Matched Rule' , 'birthday-coupons-for-woocommerce' ) ,
				) ,
				'id'      => $this->get_option_key( 'rule_priority' ) ,
				) ;
				$section_fields[] = array(
				'type' => 'sectionend' ,
				'id'   => 'bcn_coupon_rules_options' ,
				) ;
				// General Section End

				return $section_fields ;
	}

}

return new BCN_General_Tab() ;

<?php

/**
 * Localization Tab
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( class_exists( 'BCN_Localization_Tab' ) ) {
	return new BCN_Localization_Tab() ;
}

/**
 * BCN_Localization_Tab.
 */
class BCN_Localization_Tab extends BCN_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'localization' ;
		$this->label = esc_html__( 'Localization' , 'birthday-coupons-for-woocommerce' ) ;

		parent::__construct() ;
	}

	/**
	 * Get Localization Settings section array.
	 */
	public function localization_section_array() {
		$section_fields = array() ;

		//Email Section Start.
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_start' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'My Coupons Labels' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_coupon_labels' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Coupon Code Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'coupon_code_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Coupon Code' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Coupon Value Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'coupon_value_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Coupon Value' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Coupon Restriction Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'coupon_restriction_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Coupon Restriction' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Coupon Issued on Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'coupon_issued_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Coupon Issued on' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Coupon Expires on Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'coupon_expires_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Coupon Expires on' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Coupon Status Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'coupon_status_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Coupon Status' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Used Orders Label' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'used_order_label' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( 'Used Orders' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_coupon_labels' ,
				) ;
				$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Birthday Field Settings' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_birthday_field_settings' ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Birthday Field Label' , 'birthday-coupons-for-woocommerce' ) ,
				'id'      => $this->get_option_key( 'birthday_field_label' ) ,
				'type'    => 'text' ,
				'default' => esc_html__( 'Birthday' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Show Birthday Field Reason' , 'birthday-coupons-for-woocommerce' ) ,
				'type'    => 'checkbox' ,
				'default' => 'no' ,
				'desc'    => 'By enabling this checkbox, the reason will be displayed below birthday field.' ,
				'id'      => $this->get_option_key( 'enable_reason' ) ,
				) ;
				$section_fields[] = array(
				'title'   => esc_html__( 'Birthday Field Reason Label' , 'birthday-coupons-for-woocommerce' ) ,
				'id'      => $this->get_option_key( 'birthday_field_reason_label' ) ,
				'type'    => 'textarea' ,
				'default' => esc_html__( 'Enter your birthday date to receive a Birthday Coupon on that day' , 'birthday-coupons-for-woocommerce' ) ,
				) ;
								$section_fields[] = array(
				'title'   => esc_html__( 'Birthday Coupon Email Obtain Consent Label' , 'birthday-coupons-for-woocommerce' ) ,
				'id'      => $this->get_option_key( 'unsubscribe_email_label' ) ,
				'type'    => 'text' ,
				'default' => esc_html__( "Don't Send Me Birthday Coupon Emails" , 'birthday-coupons-for-woocommerce' ) ,
								) ;
								$section_fields[] = array(
								'type' => 'sectionend' ,
								'id'   => 'bcn_birthday_field_settings' ,
								) ;
								$section_fields[] = array(
								'type'      => 'bcn_custom_fields' ,
								'bcn_field' => 'section_end' ,
								) ;
								//Email Section End.

								return $section_fields ;
	}

}

return new BCN_Localization_Tab() ;

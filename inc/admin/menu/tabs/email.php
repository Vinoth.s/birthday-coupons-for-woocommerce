<?php

/**
 * Email Tab
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( class_exists( 'BCN_Email_Tab' ) ) {
	return new BCN_Email_Tab() ;
}

/**
 * BCN_Email_Tab.
 */
class BCN_Email_Tab extends BCN_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'email' ;
		$this->label = esc_html__( 'Email' , 'birthday-coupons-for-woocommerce' ) ;

		parent::__construct() ;
	}

	/**
	 * Get Email Settings section array.
	 */
	public function email_section_array() {
		$section_fields = array() ;

		//Email Section Start.
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_start' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Email Sender Options' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_email_sender_options' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Email Type' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'template_type' ) ,
			'type'    => 'select' ,
			'default' => '1' ,
			'options' => array(
				'1' => esc_html__( 'HTML' , 'birthday-coupons-for-woocommerce' ) ,
				'2' => esc_html__( 'WooCommerce Template' , 'birthday-coupons-for-woocommerce' ) ,
			) ) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'From Name' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'text' ,
			'default' => get_option( 'woocommerce_email_from_name' ) ,
			'id'      => $this->get_option_key( 'from_name' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'From Address' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'text' ,
			'default' => get_option( 'woocommerce_email_from_address' ) ,
			'id'      => $this->get_option_key( 'from_address' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_email_sender_options' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Email Settings For Guest Users' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_guest_options' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'CC' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'textarea' ,
			'default' => '' ,
			'id'      => $this->get_option_key( 'cc_for_guest' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'BCC' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'textarea' ,
			'default' => '' ,
			'id'      => $this->get_option_key( 'bcc_for_guest' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Email Subject' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( '{site_name} – Happy Birthday {user_name} !!!' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'subject_for_guest' ) ,
				) ;
		$section_fields[] = array(
			'title'     => esc_html__( 'Email Message' , 'birthday-coupons-for-woocommerce' ) ,
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'wpeditor' ,
			'default'   => esc_html__( 'Hi, <br><br>We wish you a very Happy Birthday!!! from {site_name}. Please use the coupon code “{coupon_name}” which is worth {coupon_value}. The coupon can be used on {site_url}. Please find the coupon details below, <br><br>{coupon_details} <br><br>Thanks.' , 'birthday-coupons-for-woocommerce' ) ,
			'id'        => $this->get_option_key( 'msg_for_guest' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_guest_options' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Email Settings For Logged in Users' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_user_options' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'CC' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'textarea' ,
			'default' => '' ,
			'id'      => $this->get_option_key( 'cc_for_user' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'BCC' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'textarea' ,
			'default' => '' ,
			'id'      => $this->get_option_key( 'bcc_for_user' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Email Subject' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( '{site_name} – Happy Birthday {user_name} !!!' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'subject_for_user' ) ,
				) ;
		$section_fields[] = array(
			'title'     => esc_html__( 'Email Message' , 'birthday-coupons-for-woocommerce' ) ,
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'wpeditor' ,
			'default'   => esc_html__( 'Hi, <br><br>We wish you a very Happy Birthday!!! from {site_name}. Please use the coupon code “{coupon_name}” which is worth {coupon_value}. The coupon can be used on {site_url}. Please find the coupon details below, <br><br>{coupon_details} <br><br>Thanks.' , 'birthday-coupons-for-woocommerce' ) ,
			'id'        => $this->get_option_key( 'msg_for_user' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_user_options' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Email Settings for Birthday Coupon Expiry Reminder' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_expiry_remainder_options' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Enable Coupon Expiration Reminder Email' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'checkbox' ,
			'default' => 'no' ,
			'id'      => $this->get_option_key( 'expiry_remainder_email' ) ,
				) ;
		$section_fields[] = array(
			'title'             => esc_html__( 'Send Coupon Expiration Email Before' , 'birthday-coupons-for-woocommerce' ) ,
			'type'              => 'number' ,
			'default'           => '' ,
			'custom_attributes' => array(
				'min' => '1' ,
			) ,
			'id'                => $this->get_option_key( 'expiration_days' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Email Subject' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( '{site_name} – Coupon Expiry Reminder' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'subject_for_expiry_notification' ) ,
				) ;
		$section_fields[] = array(
			'title'     => esc_html__( 'Email Message' , 'birthday-coupons-for-woocommerce' ) ,
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'wpeditor' ,
			'default'   => esc_html__( 'Hi, <br><br>The validity for the coupon “{coupon_name}” is going to expire. Please use the coupon on {site_name} before {coupon_expiry_date} to receive a {coupon_value} discount. Please find the coupon details below, <br><br>{coupon_details} <br><br>Thanks.' , 'birthday-coupons-for-woocommerce' ) ,
			'id'        => $this->get_option_key( 'msg_for_expiry_notification' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_expiry_remainder_options' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Email Settings for Birthday Coupon Expired' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_expired_options' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Enable Coupon Expired Email' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'checkbox' ,
			'default' => 'no' ,
			'id'      => $this->get_option_key( 'expired_email' ) ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Email Subject' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'text' ,
			'default' => esc_html__( '{site_name} – Coupon Expired' , 'birthday-coupons-for-woocommerce' ) ,
			'id'      => $this->get_option_key( 'subject_for_expired' ) ,
				) ;
		$section_fields[] = array(
			'title'     => esc_html__( 'Email Message' , 'birthday-coupons-for-woocommerce' ) ,
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'wpeditor' ,
			'default'   => esc_html__( 'Hi, <br><br>The coupon “{coupon_name}” on {site_url} has expired on {coupon_expiry_date}. <br><br>{coupon_details} <br><br>Thanks.' , 'birthday-coupons-for-woocommerce' ) ,
			'id'        => $this->get_option_key( 'msg_for_expired' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_expired_options' ,
				) ;
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_end' ,
				) ;
		//Email Section End.

		return $section_fields ;
	}

}

return new BCN_Email_Tab() ;

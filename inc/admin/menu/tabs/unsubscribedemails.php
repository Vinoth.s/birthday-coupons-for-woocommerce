<?php

/**
 * Unsubscribed Email List Tab
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( class_exists( 'BCN_Unsubscribed_Emails_Tab' ) ) {
	return new BCN_Unsubscribed_Emails_Tab() ;
}

/**
 * BCN_Unsubscribed_Emails_Tab.
 */
class BCN_Unsubscribed_Emails_Tab extends BCN_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'unsubscribedemails' ;
		$this->label = esc_html__( 'Unsubscribed Emails' , 'birthday-coupons-for-woocommerce' ) ;

		parent::__construct() ;

		add_action( 'woocommerce_admin_field_bcn_unsubscribed_email_list' , array( __CLASS__ , 'unsubscribed_email_list' ) ) ;
	}

	/**
	 * Get Unsubscribed Email Settings section array.
	 */
	public function unsubscribedemails_section_array() {
		$section_fields = array() ;

		//Email Section Start.
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_start' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Birthday Coupon Email Restriction Settings' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_email_restrictions' ,
				) ;
		$section_fields[] = array(
			'title'   => esc_html__( 'Obtain Consent from Logged-in-User for Sending Birthday Coupons Emails' , 'birthday-coupons-for-woocommerce' ) ,
			'type'    => 'checkbox' ,
			'default' => 'no' ,
			'desc'    => 'Enable to allow logged-in-users to stop receiving birthday coupon emails.' ,
			'id'      => $this->get_option_key( 'unsubscribe_emails' ) ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_email_restrictions' ,
				) ;
		$section_fields[] = array(
			'type'  => 'title' ,
			'title' => esc_html__( 'Unsubscribed Email List' , 'birthday-coupons-for-woocommerce' ) ,
			'id'    => 'bcn_blocked_email_list' ,
				) ;
		$section_fields[] = array(
			'type' => 'bcn_unsubscribed_email_list' ,
				) ;
		$section_fields[] = array(
			'type' => 'sectionend' ,
			'id'   => 'bcn_blocked_email_list' ,
				) ;
		$section_fields[] = array(
			'type'      => 'bcn_custom_fields' ,
			'bcn_field' => 'section_end' ,
				) ;
		//Email Section End.

		return $section_fields ;
	}

	/**
	 * Output the Unsubscribed Email List.
	 */
	public static function unsubscribed_email_list() {
		if ( ! class_exists( 'BCN_Unsubscribed_Email_Table' ) ) {
			require_once( BCN_PLUGIN_PATH . '/inc/admin/menu/wp-list-table/class-unsubscribed-email-list-table.php' ) ;
		}
		$post_table = new BCN_Unsubscribed_Email_Table() ;
		$post_table->prepare_items() ;
		echo '<div class="bcn_table_wrap">' ;
		$post_table->views() ;
		$post_table->display() ;
	}

}

return new BCN_Unsubscribed_Emails_Tab() ;

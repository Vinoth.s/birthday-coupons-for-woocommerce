<?php
/* Admin HTML Birthday Coupon Details */

if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
?>
<table class="bcn-basic-details">
	<tr>
		<th>
			<?php esc_html_e( 'Username' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</th>
		<td>
			<?php echo ! empty( $user_id ) ? esc_html( $user_data->user_login ) : esc_html( 'Guest' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</td>
	</tr>
	<tr>
		<th>
			<?php esc_html_e( 'Email' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</th>
		<td>
			<?php echo ! empty( $user_id ) ? esc_html( $user_data->user_email ) : esc_html( $birthday_coupon_obj->get_user_email() ) ; ?>
		</td>
	</tr>
	<tr>
		<th>
			<?php esc_html_e( 'Birthday' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</th>
		<td>
			<?php echo ! empty( $birthday_coupon_obj->get_birthday_date() ) ? esc_html( $birthday_coupon_obj->get_birthday_date() ) : '-' ; ?>
		</td>
	</tr>
	<tr>
		<th>
			<?php esc_html_e( 'Birthday Updated on' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</th>
		<td>
			<?php echo ! empty( $birthday_coupon_obj->get_birthday_updated_on() ) ? esc_html( $birthday_coupon_obj->get_birthday_updated_on() ) : '-' ; ?>
		</td>
	</tr>
	<tr>
		<th>
			<?php esc_html_e( 'No of Orders Placed' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</th>
		<td>
			<?php echo ! empty( wc_get_customer_order_count( $user_id ) ) ? esc_html( wc_get_customer_order_count( $user_id ) ) : '-' ; ?>
		</td>
	</tr>
	<tr>
		<th>
			<?php esc_html_e( 'Amount Spent on the Site' , 'birthday-coupons-for-woocommerce' ) ; ?>
		</th>
		<td>
			<?php echo ! empty( wc_get_customer_total_spent( $user_id ) ) ? wp_kses_post( bcn_price( wc_get_customer_total_spent( $user_id ) , false ) ) : '-' ; ?>
		</td>
	</tr>
</table>
<?php

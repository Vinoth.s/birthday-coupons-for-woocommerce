<?php
/**
 * Rule Status Meta box.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ;
}
?>
<div class="submitbox bcn-rule-status" id="submitpost">
	<div id="minor-publishing">
		<div id="misc-publishing-actions">
			<label><?php esc_html_e( 'Status' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select name="<?php echo esc_html( $this->post_type ) ; ?>_post_status" id="<?php echo esc_html( $this->post_type ) ; ?>_post_status">
				<option value="bcn_active" <?php selected( $status , 'bcn_active' ) ; ?>><?php esc_html_e( 'Active' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="bcn_inactive" <?php selected( $status , 'bcn_inactive' ) ; ?>><?php esc_html_e( 'Deactive' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
			</select>
		</div>
	</div>
	<div id="major-publishing-actions">
		<div id="publishing-action">
			<input type="submit" class="button button-primary tips <?php echo esc_html( $this->post_type ) ; ?>_post_status_btn" id="bcn_rule_save_btn" name="publish" value="<?php esc_attr_e( 'Save' , 'birthday-coupons-for-woocommerce' ) ; ?>" data-tip="<?php esc_html_e( 'Save/Update' , 'birthday-coupons-for-woocommerce' ) ; ?>" />
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php

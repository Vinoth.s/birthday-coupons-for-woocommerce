<?php
/* Admin HTML Birthday Coupon Settings */

if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
?>
<div class="bcn-rules-content-wrapper">
	<div class="bcn-rule-fields">
		<p>
			<label><?php esc_html_e( 'Enable this Rule' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="checkbox" name="bcn_coupon_rules[bcn_enable]" <?php echo checked( $rule->get_enable() , 'yes' , true ) ; ?>/>
		</p>
		<p>
			<label><?php esc_html_e( 'Rule Name' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="text" name="bcn_coupon_rules[bcn_name]" value="<?php echo esc_attr( $rule->get_name() ) ; ?>"/>
		</p>
		<p>
			<label><?php esc_html_e( 'Product Filter' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn-product-filter-type" name="bcn_coupon_rules[bcn_product_filter_type]">
				<option value="1" <?php echo selected( $rule->get_product_filter_type() , '1' , true ) ; ?>><?php esc_html_e( 'All Product(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="2" <?php echo selected( $rule->get_product_filter_type() , '2' , true ) ; ?>><?php esc_html_e( 'Include Product(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="3" <?php echo selected( $rule->get_product_filter_type() , '3' , true ) ; ?>><?php esc_html_e( 'Exclude Product(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="4" <?php echo selected( $rule->get_product_filter_type() , '4' , true ) ; ?>><?php esc_html_e( 'Include Category' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="5" <?php echo selected( $rule->get_product_filter_type() , '5' , true ) ; ?>><?php esc_html_e( 'Exclude Category' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
			</select>
		</p>
		<p class="bcn-inc-product">
			<label><?php esc_html_e( 'Include product(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<?php
			$args = array(
				'id'          => 'bcn_inc_product' ,
				'name'        => 'bcn_coupon_rules[bcn_inc_product]' ,
				'list_type'   => 'products' ,
				'action'      => 'bcn_product_search' ,
				'placeholder' => esc_html__( 'Search a product' , 'birthday-coupons-for-woocommerce' ) ,
				'options'     => $rule->get_inc_product() ,
					) ;
			bcn_select2_html( $args ) ;
			?>
		</p>
		<p class="bcn-exc-product">
			<label><?php esc_html_e( 'Exclude product(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<?php
			$args = array(
				'id'          => 'bcn_exc_product' ,
				'name'        => 'bcn_coupon_rules[bcn_exc_product]' ,
				'list_type'   => 'products' ,
				'action'      => 'bcn_product_search' ,
				'placeholder' => esc_html__( 'Search a product' , 'birthday-coupons-for-woocommerce' ) ,
				'options'     => $rule->get_exc_product() ,
					) ;
			bcn_select2_html( $args ) ;
			?>
		</p>
		<p class="bcn-inc-category">
			<label><?php esc_html_e( 'Include Category' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn_select2" name="bcn_coupon_rules[bcn_inc_category][]" multiple="multiple">
				<?php
				foreach ( bcn_get_wc_categories() as $inc_cat_id => $inc_cat_name ) :
					$selected = ( in_array( $inc_cat_id , $rule->get_inc_category() ) ) ? ' selected="selected"' : '' ;
					?>
					<option value="<?php echo esc_attr( $inc_cat_id ) ; ?>"<?php echo esc_attr( $selected ) ; ?>><?php echo esc_html( $inc_cat_name ) ; ?></option>
				<?php endforeach ; ?>
			</select>
		</p>
		<p class="bcn-exc-category">
			<label><?php esc_html_e( 'Exclude Category' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn_select2" name="bcn_coupon_rules[bcn_exc_category][]" multiple="multiple">
				<?php
				foreach ( bcn_get_wc_categories() as $exc_cat_id => $exc_cat_name ) :
					$selected = ( in_array( $exc_cat_id , $rule->get_exc_category() ) ) ? ' selected="selected"' : '' ;
					?>
					<option value="<?php echo esc_attr( $exc_cat_id ) ; ?>"<?php echo esc_attr( $selected ) ; ?>><?php echo esc_html( $exc_cat_name ) ; ?></option>
				<?php endforeach ; ?>
			</select>
		</p>
		<p>
			<label><?php esc_html_e( 'User Filter' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn-user-filter-type" name="bcn_coupon_rules[bcn_user_filter_type]">
				<option value="1" <?php echo selected( $rule->get_user_filter_type() , '1' , true ) ; ?>><?php esc_html_e( 'All User(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="2" <?php echo selected( $rule->get_user_filter_type() , '2' , true ) ; ?>><?php esc_html_e( 'Include User(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="3" <?php echo selected( $rule->get_user_filter_type() , '3' , true ) ; ?>><?php esc_html_e( 'Exclude User(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="4" <?php echo selected( $rule->get_user_filter_type() , '4' , true ) ; ?>><?php esc_html_e( 'Include Userrole' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="5" <?php echo selected( $rule->get_user_filter_type() , '5' , true ) ; ?>><?php esc_html_e( 'Exclude Userrole' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
			</select>
		</p>
		<p class="bcn-inc-user">
			<label><?php esc_html_e( 'Include User(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<?php
			$args = array(
				'id'          => 'bcn_inc_user' ,
				'name'        => 'bcn_coupon_rules[bcn_inc_user]' ,
				'list_type'   => 'customers' ,
				'action'      => 'bcn_customers_search' ,
				'placeholder' => esc_html__( 'Search a user' , 'birthday-coupons-for-woocommerce' ) ,
				'options'     => $rule->get_inc_user() ,
					) ;
			bcn_select2_html( $args ) ;
			?>
		</p>
		<p class="bcn-exc-user">
			<label><?php esc_html_e( 'Exclude User(s)' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<?php
			$args = array(
				'id'          => 'bcn_exc_user' ,
				'name'        => 'bcn_coupon_rules[bcn_exc_user]' ,
				'list_type'   => 'customers' ,
				'action'      => 'bcn_customers_search' ,
				'placeholder' => esc_html__( 'Search a user' , 'birthday-coupons-for-woocommerce' ) ,
				'options'     => $rule->get_exc_user() ,
					) ;
			bcn_select2_html( $args ) ;
			?>
		</p>
		<p class="bcn-inc-userrole">
			<label><?php esc_html_e( 'Include Userrole' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn_select2" name="bcn_coupon_rules[bcn_inc_user_role][]" multiple="multiple">
				<?php
				foreach ( bcn_get_wp_user_roles() as $user_role_id => $user_role_name ) :
					$selected = ( in_array( $user_role_id , $rule->get_inc_user_role() ) ) ? ' selected="selected"' : '' ;
					?>
					<option value="<?php echo esc_attr( $user_role_id ) ; ?>"<?php echo esc_attr( $selected ) ; ?>><?php echo esc_html( $user_role_name ) ; ?></option>
				<?php endforeach ; ?>
			</select>
		</p>
		<p class="bcn-exc-userrole">
			<label><?php esc_html_e( 'Exclude Userrole' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn_select2" name="bcn_coupon_rules[bcn_exc_user_role][]" multiple="multiple">
				<?php
				foreach ( bcn_get_wp_user_roles() as $user_role_id => $user_role_name ) :
					$selected = ( in_array( $user_role_id , $rule->get_exc_user_role() ) ) ? ' selected="selected"' : '' ;
					?>
					<option value="<?php echo esc_attr( $user_role_id ) ; ?>"<?php echo esc_attr( $selected ) ; ?>><?php echo esc_html( $user_role_name ) ; ?></option>
				<?php endforeach ; ?>
			</select>
		</p>
		<p>
			<label><?php esc_html_e( 'Restrict Birthday Coupon to Award Only once to the Users' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="checkbox" name="bcn_coupon_rules[bcn_award_once_per_user]" <?php echo checked( $rule->get_award_once_per_user() , 'yes' , true ) ; ?>/>
		</p>
		<p>
			<label><?php esc_html_e( 'Minimum Cart Total for Birthday Coupon Usage' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_min_spend]" value="<?php echo esc_attr( $rule->get_min_spend() ) ; ?>"/>
		</p>
		<p>
			<label><?php esc_html_e( 'Maximum Cart Total for Birthday Coupon Usage' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_max_spend]" value="<?php echo esc_attr( $rule->get_max_spend() ) ; ?>"/>
		</p>
		<p>
			<label><?php esc_html_e( 'Issue Coupon Based on Users Purchase history' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input class="bcn-purchase-history" type="checkbox" name="bcn_coupon_rules[bcn_purchase_history]" <?php echo checked( $rule->get_purchase_history() , 'yes' , true ) ; ?>/>
		</p>
		<p class="bcn-issue-coupon">
			<label><?php esc_html_e( 'Issue Coupon Based on' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn-purchase-history-type" name="bcn_coupon_rules[bcn_purchase_history_type]">
				<option value="1" <?php echo selected( $rule->get_purchase_history_type() , '1' , true ) ; ?>><?php esc_html_e( 'No of Order Placed' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="2" <?php echo selected( $rule->get_purchase_history_type() , '2' , true ) ; ?>><?php esc_html_e( 'Amount Spent in Site' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
			</select>
		</p>
		<p class="bcn-no-of-order">
			<label><?php esc_html_e( 'No of Order Placed' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_no_of_order]" value="<?php echo esc_attr( $rule->get_no_of_order() ) ; ?>"/>
		</p>
		<p class="bcn-total-amount-spent">
			<label><?php esc_html_e( 'Amount Spent in Site' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_total_amount_spent]" value="<?php echo esc_attr( $rule->get_total_amount_spent() ) ; ?>"/>
		</p>
		<p>
			<label><?php esc_html_e( 'Coupon Type' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<select class="bcn-coupon-type" name="bcn_coupon_rules[bcn_coupon_type]">
				<option value="percent" <?php echo selected( $rule->get_coupon_type() , 'percent' , true ) ; ?>><?php esc_html_e( 'Percentage of Order Total' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
				<option value="fixed_cart" <?php echo selected( $rule->get_coupon_type() , 'fixed_cart' , true ) ; ?>><?php esc_html_e( 'Fixed Discount' , 'birthday-coupons-for-woocommerce' ) ; ?></option>
			</select>
		</p>
		<p class="bcn-coupon-percent">
			<label><?php esc_html_e( 'Coupon Percentage' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_coupon_percent]" value="<?php echo esc_attr( $rule->get_coupon_percent() ) ; ?>"/>
		</p>
		<p class="bcn-coupon-fixed">
			<label><?php esc_html_e( 'Coupon Fixed' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_coupon_fixed]" value="<?php echo esc_attr( $rule->get_coupon_fixed() ) ; ?>"/>
		</p>
		<p>
			<label><?php esc_html_e( 'Coupon Validity in Days' , 'birthday-coupons-for-woocommerce' ) ; ?></label>
			<input type="number" min="0" name="bcn_coupon_rules[bcn_validity]" value="<?php echo esc_attr( $rule->get_validity() ) ; ?>"/>
		</p>
	</div>
	<?php wp_nonce_field( 'bcn_save_data' , 'bcn_meta_nonce' ) ; ?>
</div>
<?php

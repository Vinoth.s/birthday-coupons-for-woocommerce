<?php
/* Admin HTML Birthday Coupon Details */

if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
?>
<table class="bcn-coupon-history widefat fixed striped">
	<tr>
		<th><?php esc_html_e( 'Coupon Code' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
		<th><?php esc_html_e( 'Coupon Value' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
		<th><?php esc_html_e( 'Issued Rule' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
		<th><?php esc_html_e( 'Coupon Restriction' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
		<th><?php esc_html_e( 'Coupon Expires on' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
		<th><?php esc_html_e( 'Coupon Status' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
		<th><?php esc_html_e( 'Used Orders' , 'birthday-coupons-for-woocommerce' ) ; ?></th>
	</tr>
	<?php
	if ( bcn_check_is_array( $birthday_coupon_ids ) ) :
		foreach ( $birthday_coupon_ids as $birthday_coupon_id ) :

			$coupon_obj = bcn_get_coupon( $birthday_coupon_id ) ;

			if ( ! is_object( $coupon_obj ) ) {
				continue ;
			}

			$rule_obj = $coupon_obj->get_rule() ;

			if ( ! is_object( $rule_obj ) ) {
				continue ;
			}
			?>
			<tr>
				<td>
					<?php echo esc_html( $coupon_obj->get_coupon_code() ) ; ?>
				</td>
				<td>
					<?php
					if ( 'fixed_cart' == $rule_obj->get_coupon_type() ) {
						$coupon_value = bcn_price( wp_kses_post( $coupon_obj->get_coupon_value() ) , false ) ;
					} else {
						$coupon_value = wp_kses_post( $coupon_obj->get_coupon_value() . ' %' ) ;
					}
					echo wp_kses_post( $coupon_value ) ;
					?>
				</td>
				<td>
					<?php echo ! empty($rule_obj->get_name()) ? esc_html( $rule_obj->get_name() ) : '-' ; ?>
				</td>
				<td>
					<button class="bcn-popup-more-info" data-postid="<?php echo esc_attr( $coupon_obj->get_coupon_id() ) ; ?>"><?php esc_html_e( 'More Info' , 'birthday-coupons-for-woocommerce' ) ; ?></button>
				</td>
				<td>
					<?php echo ! empty( $coupon_obj->get_expiry_date() ) ? esc_html( $coupon_obj->get_expiry_date() ) : '-' ; ?>
				</td>
				<td>
					<?php echo wp_kses_post( bcn_display_status( $coupon_obj->get_status() ) ) ; ?>
				</td>
				<td>
					<?php echo bcn_check_is_array( $coupon_obj->get_used_orders() ) ? esc_html( implode( ',' , $coupon_obj->get_used_orders() ) ) : '-' ; ?>
				</td>
			</tr>
			<?php
		endforeach ;
	endif ;
	?>
</table>
<div class="bcn-hide">
	<div id="bcn_more_info_modal"></div>
</div>
<?php

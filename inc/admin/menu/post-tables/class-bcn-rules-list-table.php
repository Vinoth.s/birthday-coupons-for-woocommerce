<?php

/**
 * Rules List Table.
 */
if ( ! defined( 'ABSPATH' ) ) {
	bcnt ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Post_List_Table' ) ) {
	require_once( BCN_PLUGIN_PATH . '/inc/abstracts/abstract-bcn-admin-post-list-table.php') ;
}

if ( ! class_exists( 'BCN_Rules_List_Table' ) ) {

	/**
	 * BCN_Rules_List_Table Class.
	 * */
	class BCN_Rules_List_Table extends BCN_Post_List_Table {

		/**
		 * Post Type.
		 * 
		 * @var String
		 */
		protected $post_type = BCN_Register_Post_Type::BIRTHDAY_RULES_POSTTYPE ;

		/**
		 * Plugin Slug.
		 * 
		 * @var String
		 */
		protected $plugin_slug = 'bcn' ;

		/**
		 * Base URL
		 * */
		private $base_url ;

		/**
		 * Class initialization.
		 */
		public function __construct() {

			parent::__construct() ;

			$this->base_url = get_permalink() ;
		}

		/**
		 * Define the which columns to show on this screen.
		 *
		 * @return array
		 */
		public function define_columns( $columns ) {
			if ( empty( $columns ) && ! is_array( $columns ) ) {
				$columns = array() ;
			}

			unset( $columns[ 'comments' ] , $columns[ 'date' ] , $columns[ 'title' ] ) ;

			$columns[ 'rule_name' ]    = esc_html__( 'Rule Name' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'coupon_type' ]  = esc_html__( 'Coupon Type' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'coupon_value' ] = esc_html__( 'Coupon Value' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'status' ]       = esc_html__( 'Status' , 'birthday-coupons-for-woocommerce' ) ;

			return $columns ;
		}

		/**
		 * Define primary column.
		 *
		 * @return array
		 */
		protected function get_primary_column() {
			return 'rule_name' ;
		}

		/**
		 * Define which columns are sortable.
		 *
		 * @return array
		 */
		public function define_sortable_columns( $columns ) {
			$custom_columns = array(
				'rule_name' => array( 'rule_name' , true ) ,
					) ;

			return wp_parse_args( $custom_columns , $columns ) ;
		}

		/**
		 * Disable the month dropdown.
		 * 
		 * @return bool
		 */
		public function disable_months_dropdown( $bool, $post_type ) {
			return true ;
		}

		/**
		 * Get row actions to show in the list table.
		 *
		 * @return array
		 */
		protected function get_row_actions( $actions, $post ) {

			//Unset the Quick edit.
			unset( $actions[ 'inline hide-if-no-js' ] ) ;

			return $actions ;
		}

		/**
		 * Initialize the bulk actions
		 *
		 * @Since: 1.0
		 * @Return: Array
		 * */
		public function define_bulk_actions( $actions ) {

			$actions[ 'bcn_active' ]   = esc_html__( 'Active' , 'birthday-coupons-for-woocommerce' ) ;
			$actions[ 'bcn_inactive' ] = esc_html__( 'Deactive' , 'birthday-coupons-for-woocommerce' ) ;
			/**
			 * List of rules.
			 * 
			 * @since 1.0
			 */
			$actions                   = apply_filters( $this->plugin_slug . '_list_of_rules' , $actions ) ;

			return $actions ;
		}

		/**
		 * Bulk action functionality
		 *
		 * @Since: 1.0
		 * */
		public function handle_bulk_actions( $redirect_to, $action, $ids ) {

			if ( ! bcn_check_is_array( $ids ) ) {
				return ;
			}

			// Return if current user not have permission.
			if ( ! current_user_can( 'edit_posts' ) ) {
				throw new exception( esc_html__( "You don't have permission to do this action" , 'birthday-coupons-for-woocommerce' ) ) ;
			}
			
			foreach ( $ids as $id ) {
				switch ( $action ) {
					case 'bcn_active':
					case 'bcn_inactive':
						bcn_get_rule( $id )->update_status( $action ) ;
						break ;
				}
			}

			wp_safe_redirect( $redirect_to ) ;
			exit() ;
		}

		/**
		 * Pre-fetch any data for the row each column has access to it.
		 */
		protected function prepare_row_data( $post_id ) {
			if ( empty( $this->object ) || $this->object->get_id() !== $post_id ) {
				$this->object = bcn_get_rule( $post_id ) ;
			}
		}

		/**
		 * Render the rule name column.
		 *
		 * @return void
		 */
		public function render_rule_name_column() {
			$rule_name = $this->object->get_name() ;
			echo ! empty( $this->object->get_name() ) ? wp_kses_post( $this->object->get_name() ) : esc_html( 'Untitled' , 'birthday-coupons-for-woocommerce' ) ;
		}

		/**
		 * Render the coupon type column.
		 *
		 * @return void
		 */
		public function render_coupon_type_column() {

			if ( 'fixed_cart' == $this->object->get_coupon_type() ) {
				esc_html_e( 'Fixed Cart' , 'birthday-coupons-for-woocommerce' ) ;
			} else {
				esc_html_e( 'Percentage discount' , 'birthday-coupons-for-woocommerce' ) ;
			}
		}

		/**
		 * Render the coupon value column.
		 *
		 * @return void
		 */
		public function render_coupon_value_column() {

			if ( 'fixed_cart' == $this->object->get_coupon_type() ) {
				echo wp_kses_post( bcn_price( $this->object->get_coupon_fixed() ) ) ;
			} else {
				echo wp_kses_post( $this->object->get_coupon_percent() . '%' ) ;
			}
		}

		/**
		 * Render the status column.
		 *
		 * @return void
		 */
		public function render_status_column() {

			echo wp_kses_post( bcn_display_status( $this->object->get_status() ) ) ;
		}

		/**
		 * Add meta boxes for this post type.
		 * 
		 * @return void
		 */
		public function add_meta_boxes() {
			// Remove the publish meta box.
			remove_meta_box( 'submitdiv' , $this->post_type , 'side' ) ;
			// Remove the publish meta box.
			remove_meta_box( 'postdivrich' , $this->post_type , 'side' ) ;
			// Rule data meta box.
			add_meta_box( 'bcn-rule-data' , esc_html__( 'Rule(s)' , 'birthday-coupons-for-woocommerce' ) , array( $this , 'render_rule_data_meta_box' ) , $this->post_type , 'normal' , 'high' ) ;
			// Status data meta box.
			add_meta_box( 'bcn-status' , esc_html__( 'Status' , 'birthday-coupons-for-woocommerce' ) , array( $this , 'render_status_meta_box' ) , $this->post_type , 'side' , 'high' ) ;
		}

		/**
		 * Handle search filters.
		 *
		 * @return array
		 */
		public function get_search_post_ids( $terms ) {
			$post_ids = array() ;
			foreach ( $terms as $term ) {

				$term = $this->database->esc_like( ( $term ) ) ;

				$post_query = new BCN_Query( $this->database->prefix . 'posts' , 'p' ) ;

				$post_query->select( 'DISTINCT `p`.ID' )
						->leftJoin( $this->database->prefix . 'postmeta' , 'pm' , '`p`.`ID` = `pm`.`post_id`' )
						->where( '`p`.post_type' , $this->post_type )
						->whereIn( '`p`.post_status' , array( 'bcn_active' , 'bcn_inactive' ) )
						->where( '`pm`.meta_key' , 'bcn_name' )
						->whereLike( '`pm`.meta_value' , '%' . $term . '%' ) ;

				$post_ids = $post_query->fetchCol( 'ID' ) ;
			}

			return array_merge( $post_ids , array( 0 ) ) ;
		}

		/**
		 * Render the coupon history meta box.
		 *
		 * @return void
		 */
		public function render_rule_data_meta_box( $post ) {
			$rule_id = ( isset( $post->ID ) ) ? $post->ID : '' ;
			$rule    = bcn_get_rule( $rule_id ) ;

			include ( BCN_PLUGIN_PATH . '/inc/admin/menu/views/meta-boxes/birthday/birthday-coupon-settings.php' ) ;
		}

		/**
		 * Render the status meta box.
		 *
		 * @return void
		 */
		public function render_status_meta_box( $post ) {

			$rule_id = ( isset( $post->ID ) ) ? $post->ID : '' ;
			$rules   = bcn_get_rule( $rule_id ) ;
			$status  = $rules->get_status() ;

			include ( BCN_PLUGIN_PATH . '/inc/admin/menu/views/meta-boxes/html-rule-status.php' ) ;
		}

	}

}

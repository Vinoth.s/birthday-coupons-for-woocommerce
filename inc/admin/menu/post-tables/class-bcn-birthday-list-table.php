<?php

/**
 * Birthday Coupon Details List Table.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Post_List_Table' ) ) {
	require_once( BCN_PLUGIN_PATH . '/inc/abstracts/abstract-bcn-admin-post-list-table.php') ;
}

if ( ! class_exists( 'BCN_Coupon_Details_Table' ) ) {

	/**
	 * BCN_Coupon_Details_Table Class.
	 * */
	class BCN_Coupon_Details_Table extends BCN_Post_List_Table {

		/**
		 * Post Type.
		 * 
		 * @var String
		 */
		protected $post_type = BCN_Register_Post_Type::BCN_BIRTHDAY_POSTTYPE ;

		/**
		 * Plugin Slug.
		 * 
		 * @var String
		 */
		protected $plugin_slug = 'bcn' ;

		/**
		 * Base URL
		 * */
		private $base_url ;

		/**
		 * Class initialization.
		 */
		public function __construct() {

			parent::__construct() ;

			$this->base_url = get_permalink() ;
		}

		/**
		 * Define the which columns to show on this screen.
		 *
		 * @return array
		 */
		public function define_columns( $columns ) {
			if ( empty( $columns ) && ! is_array( $columns ) ) {
				$columns = array() ;
			}

			unset( $columns[ 'comments' ] , $columns[ 'date' ] , $columns[ 'title' ] , $columns[ 'cb' ] ) ;

			$columns[ 'user_details' ]  = esc_html__( 'User Details' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'birthday_date' ] = esc_html__( 'Birthday' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'coupon_count' ]  = esc_html__( 'No of Coupons Issued' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'issued_date' ]   = esc_html__( 'Last Coupon Issued on' , 'birthday-coupons-for-woocommerce' ) ;
			$columns[ 'more_details' ]  = esc_html__( 'More Details' , 'birthday-coupons-for-woocommerce' ) ;

			return $columns ;
		}

		/**
		 * Define primary column.
		 *
		 * @return array
		 */
		protected function get_primary_column() {
			return 'user_details' ;
		}

		/**
		 * Define which columns are sortable.
		 *
		 * @return array
		 */
		public function define_sortable_columns( $columns ) {
			$custom_columns = array(
				'issued_date'  => array( 'issued_date' , true ) ,
				'user_details' => array( 'user_details' , true ) ,
					) ;

			return wp_parse_args( $custom_columns , $columns ) ;
		}

		/**
		 * Define bulk actions.
		 * 
		 * @return array
		 */
		public function define_bulk_actions( $actions ) {

			unset( $actions[ 'edit' ] , $actions[ 'trash' ] ) ;

			return $actions ;
		}

		/**
		 * Disable the month dropdown.
		 * 
		 * @return bool
		 */
		public function disable_months_dropdown( $bool, $post_type ) {
			return true ;
		}

		/**
		 * Get row actions to show in the list table.
		 *
		 * @return array
		 */
		protected function get_row_actions( $actions, $post ) {

			//Unset the Quick edit.
			unset( $actions[ 'inline hide-if-no-js' ] , $actions[ 'edit' ] , $actions[ 'trash' ] ) ;

			return $actions ;
		}

		/**
		 * Pre-fetch any data for the row each column has access to it.
		 */
		protected function prepare_row_data( $post_id ) {
			if ( empty( $this->object ) || $this->object->get_id() !== $post_id ) {
				$this->object = bcn_get_birthday_coupon( $post_id ) ;
			}
		}

		/**
		 * Render the user details column.
		 *
		 * @return void
		 */
		public function render_user_details_column() {
			$user_id   = $this->object->get_user_id() ;
			$user_data = get_userdata( $user_id ) ;
			echo is_object( $user_data ) ? wp_kses_post( $user_data->user_login . ' (' . $user_data->user_email . ')' ) : wp_kses_post( $this->object->get_user_email() ) ;
		}

		/**
		 * Render the birthday date column.
		 *
		 * @return void
		 */
		public function render_birthday_date_column() {

			echo wp_kses_post( $this->object->get_birthday_date() ) ;
		}

		/**
		 * Render the coupon count column.
		 *
		 * @return void
		 */
		public function render_coupon_count_column() {

			echo ! empty( $this->object->get_coupon_count() ) ? wp_kses_post( $this->object->get_coupon_count() ) : '-' ;
		}

		/**
		 * Render the created date column.
		 *
		 * @return void
		 */
		public function render_issued_date_column() {

			echo ! empty( $this->object->get_issued_date() ) ? wp_kses_post( $this->object->get_issued_date() ) : '-' ;
		}

		/**
		 * Render the more details column.
		 *
		 * @return void
		 */
		public function render_more_details_column() {

			$more_details_url = admin_url( 'post.php?post=' . absint( $this->object->get_id() ) . '&action=edit' ) ;
			echo '<a href=' . esc_url( $more_details_url ) . '>' . esc_html__( 'View More' , 'birthday-coupons-for-woocommerce' ) . '</a>' ;
		}

		/**
		 * Add meta boxes for this post type.
		 * 
		 * @return void
		 */
		public function add_meta_boxes() {
			// Remove the publish meta box.
			remove_meta_box( 'submitdiv' , $this->post_type , 'side' ) ;
			// Remove the publish meta box.
			remove_meta_box( 'postdivrich' , $this->post_type , 'side' ) ;
			// User Details data meta box.
			add_meta_box( 'bcn-user-details' , esc_html__( 'User Details' , 'birthday-coupons-for-woocommerce' ) , array( $this , 'render_user_details_meta_box' ) , $this->post_type , 'normal' , 'high' ) ;
			// Coupon History data meta box.
			add_meta_box( 'bcn-coupon-history' , esc_html__( 'Coupon History' , 'birthday-coupons-for-woocommerce' ) , array( $this , 'render_coupon_history_meta_box' ) , $this->post_type , 'normal' , 'high' ) ;
		}

		/**
		 * Render the user details meta box.
		 *
		 * @return void
		 */
		public function render_user_details_meta_box( $post ) {

			$birthday_coupon_id  = ( isset( $post->ID ) ) ? $post->ID : '' ;
			$birthday_coupon_obj = bcn_get_birthday_coupon( $birthday_coupon_id ) ;
			$user_id             = $birthday_coupon_obj->get_user_id() ;
			$user_data           = get_userdata( $user_id ) ;

			$args = array() ;

			$args[ 'meta_query' ] = array(
				array(
					'key'     => 'bcn_birthday_coupon_id' ,
					'value'   => $birthday_coupon_id ,
					'compare' => '==' ,
				) ,
					) ;

			$birthday_coupon_ids = bcn_get_coupon_ids( $args ) ;

			include ( BCN_PLUGIN_PATH . '/inc/admin/menu/views/birthday-user-details.php' ) ;
		}

		/**
		 * Render the coupon history meta box.
		 *
		 * @return void
		 */
		public function render_coupon_history_meta_box( $post ) {
			$birthday_coupon_id  = ( isset( $post->ID ) ) ? $post->ID : '' ;
			$birthday_coupon_obj = bcn_get_birthday_coupon( $birthday_coupon_id ) ;
			$user_id             = $birthday_coupon_obj->get_user_id() ;
			$user_data           = get_userdata( $user_id ) ;

			$args = array() ;

			$args[ 'meta_query' ] = array(
				array(
					'key'     => 'bcn_birthday_coupon_id' ,
					'value'   => $birthday_coupon_id ,
					'compare' => '==' ,
				) ,
					) ;

			$birthday_coupon_ids = bcn_get_coupon_ids( $args ) ;

			include ( BCN_PLUGIN_PATH . '/inc/admin/menu/views/birthday-coupon-history.php' ) ;
		}

	}

}

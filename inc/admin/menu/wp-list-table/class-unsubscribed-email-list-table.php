<?php

/**
 * Unsubscribed Email List Table
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ) ;
}

if ( ! class_exists( 'BCN_Unsubscribed_Email_Table' ) ) {

	/**
	 * BCN_Unsubscribed_Email_Table Class.
	 * */
	class BCN_Unsubscribed_Email_Table extends WP_List_Table {

		/**
		 * Total Count of Table
		 * */
		private $total_items ;

		/**
		 * Per page count
		 * */
		private $perpage ;

		/**
		 * Database
		 * */
		private $database ;

		/**
		 * Offset
		 * */
		private $offset ;

		/**
		 * Order BY
		 * */
		private $orderby = 'ORDER BY ID DESC' ;

		/**
		 * Post type
		 * */
		private $post_type = BCN_Register_Post_Type::BCN_UNSUBSCRIBER_POSTTYPE ;

		/**
		 * Base URL
		 * */
		private $base_url ;

		/**
		 * Current URL
		 * */
		private $current_url ;

		/**
		 * Prepare the table Data to display table based on pagination.
		 * */
		public function prepare_items() {
			global $wpdb ;
			$this->database = $wpdb ;

			$this->base_url = add_query_arg( array( 'page' => 'bcn_settings' , 'tab' => 'unsubscribedemails' ) , admin_url( 'admin.php' ) ) ;

			add_filter( sanitize_key( $this->table_slug . '_query_orderby' ) , array( $this , 'query_orderby' ) ) ;

			$this->prepare_current_url() ;
			$this->get_perpage_count() ;
			$this->get_current_pagenum() ;
			$this->get_current_page_items() ;
			$this->prepare_pagination_args() ;
			$this->prepare_column_headers() ;
		}

		/**
		 * Get per page count
		 * */
		private function get_perpage_count() {

			$this->perpage = 10 ;
		}

		/**
		 * Prepare pagination
		 * */
		private function prepare_pagination_args() {

			$this->set_pagination_args(
					array(
						'total_items' => $this->total_items ,
						'per_page'    => $this->perpage ,
					)
			) ;
		}

		/**
		 * Get current page number
		 * */
		private function get_current_pagenum() {

			$this->offset = $this->perpage * ( $this->get_pagenum() - 1 ) ;
		}

		/**
		 * Prepare header columns
		 * */
		private function prepare_column_headers() {
			$columns               = $this->get_columns() ;
			$hidden                = $this->get_hidden_columns() ;
			$sortable              = $this->get_sortable_columns() ;
			$this->_column_headers = array( $columns , $hidden , $sortable ) ;
		}

		/**
		 * Initialize the columns
		 * */
		public function get_columns() {
			return array(
				'bcn_user_name'  => esc_html__( 'Username' , 'birthday-coupons-for-woocommerce' ) ,
				'bcn_email_id' => esc_html__( 'Email Id' , 'birthday-coupons-for-woocommerce' ) ,
				'bcn_action'  => esc_html__( 'Action' , 'birthday-coupons-for-woocommerce' ) ,
					) ;
		}

		/**
		 * Initialize the hidden columns
		 * */
		public function get_hidden_columns() {
			return array() ;
		}

		/**
		 * Initialize the sortable columns
		 * */
		public function get_sortable_columns() {
			return array() ;
		}

		/**
		 * Get current url
		 * */
		private function prepare_current_url() {

			$pagenum         = $this->get_pagenum() ;
			$args[ 'paged' ] = $pagenum ;
			$url             = add_query_arg( $args , $this->base_url ) ;

			$this->current_url = $url ;
		}

		/**
		 * Prepare each column data
		 * */
		protected function column_default( $item, $column_name ) {

			switch ( $column_name ) {
				case 'bcn_user_name':
					return esc_html( $item->get_user_name() ) ;
					break ;
				case 'bcn_email_id':
					return esc_html( $item->get_user_email() ) ;
					break ;
				case 'bcn_action':
					echo '<a href=' . esc_url( $this->base_url ) . ' data-unsubid=' . esc_attr( $item->get_id() ) . ' class="bcn-delete-unsubscriber">' . esc_html__( 'Remove' , 'birthday-coupons-for-woocommerce' ) . '</a>' ;
					break ;
			}
		}

		/**
		 * Initialize the columns
		 * */
		private function get_current_page_items() {
			$where = " where post_type='" . $this->post_type . "'" ;
			/**
			 * Query Where.
			 * 
			 * @since 1.0
			 */
			$where   = apply_filters( $this->table_slug . '_query_where' , $where ) ;
			/**
			 * Query limit.
			 * 
			 * @since 1.0
			 */
			$limit   = apply_filters( $this->table_slug . '_query_limit' , $this->perpage ) ;
			/**
			 * Query offset.
			 * 
			 * @since 1.0
			 */
			$offset  = apply_filters( $this->table_slug . '_query_offset' , $this->offset ) ;
			/**
			 * Query orderby.
			 * 
			 * @since 1.0
			 */
			$orderby = apply_filters( $this->table_slug . '_query_orderby' , $this->orderby ) ;

			$count_items       = $this->database->get_results( 'SELECT DISTINCT ID FROM ' . $this->database->posts . " AS p {$where} {$orderby}" ) ;
			$this->total_items = count( $count_items ) ;

			$prepare_query = $this->database->prepare( 'SELECT DISTINCT ID FROM ' . $this->database->posts . " AS p {$where} {$orderby} LIMIT %d,%d" , $offset , $limit ) ;

			$items = $this->database->get_results( $prepare_query , ARRAY_A ) ;

			$this->prepare_item_object( $items ) ;
		}

		/**
		 * Prepare item Object
		 * */
		private function prepare_item_object( $items ) {
			$prepare_items = array() ;
			if ( bcn_check_is_array( $items ) ) {
				foreach ( $items as $item ) {
					$prepare_items[] = bcn_get_unsubscriber( $item[ 'ID' ] ) ;
				}
			}

			$this->items = $prepare_items ;
		}

		/**
		 * Sort
		 * */
		public function query_orderby( $orderby ) {

			if ( empty( $_REQUEST[ 'orderby' ] ) ) { // @codingStandardsIgnoreLine.
				return $orderby ;
			}

			$order = 'DESC' ;
			if ( ! empty( $_REQUEST[ 'order' ] ) && is_string( $_REQUEST[ 'order' ] ) ) { // @codingStandardsIgnoreLine.
				if ( 'ASC' === strtoupper( wc_clean( wp_unslash( $_REQUEST[ 'order' ] ) ) ) ) { // @codingStandardsIgnoreLine.
					$order = 'ASC' ;
				}
			}

			switch ( wc_clean( wp_unslash( $_REQUEST[ 'orderby' ] ) ) ) { // @codingStandardsIgnoreLine.
				case 'status':
					$orderby = ' ORDER BY p.post_status ' . $order ;
					break ;
				case 'created':
					$orderby = ' ORDER BY p.post_date ' . $order ;
					break ;
				case 'modified':
					$orderby = ' ORDER BY p.post_modified ' . $order ;
					break ;
			}
			return $orderby ;
		}

	}

}

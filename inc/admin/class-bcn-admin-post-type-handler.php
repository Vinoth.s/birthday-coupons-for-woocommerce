<?php

/**
 * Admin Post Type Handler.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Admin_Post_Type_Handler' ) ) {

	/**
	 * Class.
	 */
	class BCN_Admin_Post_Type_Handler {

		/**
		 * Post type
		 * 
		 * @var String
		 * */
		private static $post_type = BCN_Register_Post_Type::BIRTHDAY_RULES_POSTTYPE ;

		/**
		 * Post type
		 * 
		 * @var String
		 * */
		private static $plugin_slug = 'bcn' ;

		/**
		 * Saved meta boxes.
		 * 
		 * @var Boolean
		 */
		private static $saved_meta_boxes = false ;

		/**
		 * Class initialization.
		 */
		public static function init() {
			// Load correct list table classes for current screen.
			add_action( 'current_screen' , array( __CLASS__ , 'setup_screen' ) ) ;
			add_action( 'check_ajax_referer' , array( __CLASS__ , 'setup_screen' ) ) ;
			add_action( 'save_post' , array( __CLASS__ , 'save_meta_boxes' ) , 1 , 2 ) ;
			add_filter( 'post_updated_messages' , array( __CLASS__ , 'display_post_msg' ) ) ;
		}

		/**
		 * Looks at the current screen and loads the correct list table handler.
		 *
		 * @return void
		 */
		public static function setup_screen() {
			global $bcn_list_table ;

			$screen_id = false ;

			if ( ! empty( $_REQUEST[ 'screen' ] ) ) {
				$screen_id = wc_clean( wp_unslash( $_REQUEST[ 'screen' ] ) ) ;
			} elseif ( function_exists( 'get_current_screen' ) ) {
				$screen    = get_current_screen() ;
				$screen_id = isset( $screen , $screen->id ) ? $screen->id : '' ;
			}

			$screen_id = str_replace( 'edit-' , '' , $screen_id ) ;

			switch ( $screen_id ) {
				case 'bcn_birthday':
					include_once (BCN_PLUGIN_PATH . '/inc/admin/menu/post-tables/class-bcn-birthday-list-table.php') ;

					$bcn_birthday_table = new BCN_Coupon_Details_Table() ;

					break ;
				case 'bcn_rules':
					include_once (BCN_PLUGIN_PATH . '/inc/admin/menu/post-tables/class-bcn-rules-list-table.php') ;

					$bcn_rules_table = new BCN_Rules_List_Table() ;

					break ;
			}

			// Ensure the table handler is only loaded once. Prevents multiple loads if a plugin calls check_ajax_referer many times.
			remove_action( 'current_screen' , array( __CLASS__ , 'setup_screen' ) ) ;
			remove_action( 'check_ajax_referer' , array( __CLASS__ , 'setup_screen' ) ) ;
		}

		/**
		 * Save meta boxes for this post type.
		 * 
		 * @Since: 1.0
		 * @Param Object $post.
		 * */
		public static function save_meta_boxes( $post_id, $post ) {
			$post_id = absint( $post_id ) ;

			// $post_id and $post are required.
			if ( empty( $post_id ) || empty( $post ) || self::$saved_meta_boxes ) {
				return ;
			}

			// Check the nonce.
			if ( empty( $_POST[ self::$plugin_slug . '_meta_nonce' ] ) || ! wp_verify_nonce( wc_clean( wp_unslash( $_POST[ self::$plugin_slug . '_meta_nonce' ] ) ) , self::$plugin_slug . '_save_data' ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				return ;
			}

			// Check the post being saved == the $post_id to prevent triggering this call for other save_post events.
			if ( empty( $_POST[ 'post_ID' ] ) || absint( $_POST[ 'post_ID' ] ) !== $post_id ) {
				return ;
			}

			// Check user has permission to edit.
			if ( ! current_user_can( 'edit_post' , $post_id ) ) {
				return ;
			}

			if ( 'bcn_rules' != get_post_type( $post ) ) {
				return ;
			}

			// We need this save event to run once to avoid potential endless loops. This would have been perfect:
			self::$saved_meta_boxes = true ;
			// Save meta box data.
			self::save_current_meta_boxes( $post_id , $post ) ;
			/**
						 * After save post metabox.
						 * 
						 * @since 1.0
						 */
			do_action( 'bcn_after_save_post_metabox_data' , $post_id , $post ) ;
		}

		/**
		 * Save All meta boxes data for this post type.
		 * 
		 * @Since: 1.0
		 * @Param Integer $post_id, Object $post.
		 * */
		public static function save_current_meta_boxes( $post_id, $post ) {

			if ( ! isset( $_REQUEST[ 'bcn_coupon_rules' ] ) ) {
				return ;
			}

			$status = isset( $_REQUEST[ 'bcn_rules_post_status' ] ) ? wc_clean( wp_unslash( $_REQUEST[ 'bcn_rules_post_status' ] ) ) : 'bcn_active' ;
			$rule   = isset( $_REQUEST[ 'bcn_coupon_rules' ] ) ? wc_clean( wp_unslash( $_REQUEST[ 'bcn_coupon_rules' ] ) ) : array() ;

			$postargs = array(
				'post_status' => $status
					) ;

			$rule[ 'bcn_enable' ]              = isset( $rule[ 'bcn_enable' ] ) ? 'yes' : 'no' ;
			$rule[ 'bcn_award_once_per_user' ] = isset( $rule[ 'bcn_award_once_per_user' ] ) ? 'yes' : 'no' ;
			$rule[ 'bcn_purchase_history' ]    = isset( $rule[ 'bcn_purchase_history' ] ) ? 'yes' : 'no' ;

			bcn_update_rule( $post_id , $rule , $postargs ) ;
		}

		/**
		 * Display Post Action message
		 * 
		 * @Since: 1.0
		 * @Param Array $messages
		 * @Return Array
		 * */
		public static function display_post_msg( $messages ) {

			$messages[ self::$post_type ] = array(
				1  => esc_html__( 'Rule updated.' , 'birthday-coupons-for-woocommerce' ) ,
				4  => esc_html__( 'Rule updated.' , 'birthday-coupons-for-woocommerce' ) ,
				6  => esc_html__( 'Rule updated.' , 'birthday-coupons-for-woocommerce' ) ,
				7  => esc_html__( 'Rule saved.' , 'birthday-coupons-for-woocommerce' ) ,
				10 => esc_html__( 'Rule draft updated.' , 'birthday-coupons-for-woocommerce' )
					) ;

			return $messages ;
		}

	}

	BCN_Admin_Post_Type_Handler::init() ;
}

<?php

/**
 * Handles the Cron.
 * */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly
}

if ( ! class_exists( 'BCN_Cron_Handler' ) ) {

	/**
	 * Class.
	 * */
	class BCN_Cron_Handler {

		/**
		 *  Class initialization.
		 * */
		public static function init() {
			// Maybe set the Cron schedule event.
			add_filter( 'cron_schedules', array ( __CLASS__, 'cron_interval' ) ) ;
			// Maybe set the WP schedule event.
			add_action( 'init', array ( __CLASS__, 'maybe_set_wp_schedule_event' ), 10 ) ;
			// Handle the delivery emails.
			add_action( 'bcn_cron', array ( __CLASS__, 'handle_wp_cron' ) ) ;
			// Handle Expiry email before coupon expired.
			add_action( 'bcn_send_mail_before_expiry', array ( __CLASS__, 'send_mail_before_expiry' ), 10 ) ;
		}

		/**
		 *  Set Cron Interval for Birthday Coupon
		 */
		public static function cron_interval( $schedules ) {

			$schedules[ 'bcn_hourly' ] = array (
				'interval'	 => 60,
				'display'	 => 'BCN BC Hourly'
					) ;

			$schedules[ 'bcn_minutes' ] = array (
				'interval'	 => 60,
				'display'	 => 'BCN BC Minutes'
					) ;

			return $schedules ;
		}

		/**
		 * Maybe set the WP schedule event.
		 * 
		 * @return void.
		 * */
		public static function maybe_set_wp_schedule_event() {
			if ( wp_next_scheduled( 'bcn_birthday_cron' ) == false ) {
				wp_schedule_event( time(), 'bcn_hourly', 'bcn_birthday_cron' ) ;
			}

			if ( wp_next_scheduled( 'bcn_cron' ) == false ) {
				wp_schedule_event( time(), 'bcn_minutes', 'bcn_cron' ) ;
			}
		}

		/**
		 * Handles the WP cron.
		 * 
		 * @return void.
		 * */
		public static function handle_wp_cron() {

			// Update the WP cron current date. 
			update_option( 'bcn_update_wp_cron_last_updated_date', BCN_Date_Time::get_mysql_date_time_format( 'now', true ) ) ;

			// May be handle the expired coupon emails.
			self::maybe_handle_coupon_expiry() ;
		}

		/**
		 * May be handle the expiry coupon emails .
		 * 
		 * @return void.
		 * */
		public static function maybe_handle_coupon_expiry() {
			$current_date_object = BCN_Date_Time::get_date_time_object( 'now' ) ;
			$args				 = array () ;

			$args[ 'meta_query' ] = array (
				'relation' => 'AND',
				array (
					'key'		 => 'bcn_expiry_date',
					'value'		 => $current_date_object->format( 'Y-m-d' ),
					'compare'	 => '<=',
					'type'		 => 'DATE',
				),
				array (
					'key'		 => 'bcn_expired',
					'compare'	 => 'NOT EXISTS',
				),
					) ;

			$coupons_list = bcn_get_coupon_ids( $args ) ;

			// Return if no post ids.
			if ( ! bcn_check_is_array( $coupons_list ) ) {
				return ;
			}

			$meta_args = array (
				'bcn_expired' => 'yes'
					) ;

			$post_args = array (
				'post_status' => 'bcn_expired'
					) ;

			foreach ( $coupons_list as $birthday_coupon_id ) {

				bcn_update_coupon( $birthday_coupon_id, $meta_args, $post_args ) ;
				/**
				 * Coupon expired.
				 * 
				 * @since 1.0
				 */
				do_action( 'bcn_coupon_expired', $birthday_coupon_id ) ;
			}
		}

		/*
		 * Handle Expiry Remainder Email.
		 */

		public static function send_mail_before_expiry() {

			$args = array () ;

			$no_of_days			 = get_option( 'bcn_email_expiration_days' ) ;
			$date_to_send_mail	 = strtotime( '+' . $no_of_days . 'days', strtotime( gmdate( 'Y-m-d' ) ) ) ;

			$args[ 'meta_query' ] = array (
				'relation' => 'AND',
				array (
					'key'		 => 'bcn_expiry_date',
					'value'		 => gmdate( 'Y-m-d', $date_to_send_mail ),
					'compare'	 => '==',
					'type'		 => 'DATE',
				),
				array (
					'key'		 => 'bcn_expiry_remainder',
					'compare'	 => 'NOT EXISTS',
				),
					) ;

			$coupons_list = bcn_get_coupon_ids( $args ) ;

			// Return if no post ids.
			if ( ! bcn_check_is_array( $coupons_list ) ) {
				return ;
			}

			$meta_args = array (
				'bcn_expiry_remainder' => 'yes'
					) ;


			foreach ( $coupons_list as $birthday_coupon_id ) {

				bcn_update_coupon( $birthday_coupon_id, $meta_args ) ;
				/**
				 * Expiry remainder mail.
				 * 
				 * @since 1.0
				 */
				do_action( 'bcn_expiry_remainder_email', $birthday_coupon_id ) ;
			}
		}

	}

	BCN_Cron_Handler::init() ;
}

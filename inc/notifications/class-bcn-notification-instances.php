<?php

/**
 * Notifications Instances Class
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Notification_Instances' ) ) {

	/**
	 * Class BCN_Notification_Instances.
	 */
	class BCN_Notification_Instances {
		/*
		 * Notifications.
		 */

		private static $notifications = array() ;

		/*
		 * Get Notifications.
		 */

		public static function get_notifications() {

			if ( ! self::$notifications ) {
				self::load_notifications() ;
			}

			return self::$notifications ;
		}

		/*
		 * Load all Notifications.
		 */

		public static function load_notifications() {

			if ( ! class_exists( 'BCN_Notifications' ) ) {
				include BCN_PLUGIN_PATH . '/inc/abstracts/abstract-bcn-notifications.php' ;
			}

			$default_notification_classes = array(
				'guest-email'            => 'BCN_Guest_Email_Notification' ,
				'user-email'             => 'BCN_User_Email_Notification' ,
				'expiry-remainder-email' => 'BCN_Expiry_Remainder_Email_Notification' ,
				'expired-email'          => 'BCN_Expired_Email_Notification' ,
					) ;

			foreach ( $default_notification_classes as $file_name => $notification_class ) {

				// include file.
				include 'class-' . $file_name . '.php' ;

				// add notification.
				self::add_notification( new $notification_class() ) ;
			}
		}

		/**
		 * Add a Module.
		 */
		public static function add_notification( $notification ) {

			self::$notifications[ $notification->get_id() ] = $notification ;

			return new self() ;
		}

		/**
		 * Get notification by id.
		 */
		public static function get_notification_by_id( $notification_id ) {

			$notifications = self::get_notifications() ;

			return isset( $notifications[ $notification_id ] ) ? $notifications[ $notification_id ] : false ;
		}

	}

}


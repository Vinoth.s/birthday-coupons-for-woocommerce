<?php

/**
 * Guest Email
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Guest_Email_Notification' ) ) {

	/**
	 * Class BCN_Guest_Email_Notification.
	 */
	class BCN_Guest_Email_Notification extends BCN_Notifications {

		/**
		 * Class Constructor.
		 */
		public function __construct() {

			$this->id = 'guest_email' ;

			// Triggers for this email.
			add_action( 'bcn_after_birthday_coupon_creation_for_user' , array( $this , 'trigger' ) , 10 , 7 ) ;

			parent::__construct() ;
		}

		/**
		 * Get Enabled.
		 */
		public function get_enabled() {

			return get_option( 'bcn_general_guest_coupon' ) ;
		}

		/*
		 * Default Subject.
		 */

		public function get_default_subject() {

			return get_option( 'bcn_email_subject_for_guest' ) ;
		}

		/*
		 * Default Message.
		 */

		public function get_default_message() {

			return get_option( 'bcn_email_msg_for_guest' ) ;
		}

		/**
		 * Get email headers.
		 */
		public function get_headers() {
			$headers = 'Content-Type: ' . $this->get_content_type() . "\r\n" ;
			$cc      = get_option( 'bcn_email_cc_for_guest' ) ;
			$bcc     = get_option( 'bcn_email_bcc_for_guest' ) ;

			if ( ! empty( $cc ) ) {
				$headers .= 'Cc: ' . $cc . "\r\n" ;
			}
			if ( ! empty( $bcc ) ) {
				$headers .= 'Bcc: ' . $bcc . "\r\n" ;
			}

			return $headers ;
		}

		/**
		 * Get content type.
		 */
		public function get_content_type() {

			return 'text/html' ;
		}

		/**
		 * Trigger the sending of this email.
		 */
		public function trigger( $user_name, $user_email, $coupon_id, $coupon_code, $coupon_value, $discount_type, $user_id ) {

			if ( ! $this->is_enabled() ) {
				return ;
			}

			if ( ! empty( $user_id ) ) {
				return ;
			}

			$this->recipient                          = $user_email ;
			$this->placeholders[ '{user_name}' ]      = $user_name ;
			$this->placeholders[ '{site_url}' ]       = get_site_url() ;
			$this->placeholders[ '{coupon_name}' ]    = $coupon_code ;
			$this->placeholders[ '{coupon_value}' ]   = ( 'percent' == $discount_type ) ? $coupon_value . '%' : wp_kses_post( bcn_price( $coupon_value ) ) ;
			$coupon_data                              = array( 'coupon_obj' => bcn_get_coupon_details( $coupon_id ) ) ;
			$this->placeholders[ '{coupon_details}' ] = bcn_get_template_html( 'popup-layout.php' , $coupon_data ) ;

			if ( $this->get_recipient() ) {
				$this->send_email( $this->get_recipient() , $this->get_subject() , $this->get_formatted_message() , $this->get_headers() , $this->get_attachments() ) ;
			}
		}

	}

}

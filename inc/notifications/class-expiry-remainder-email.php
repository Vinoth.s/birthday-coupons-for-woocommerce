<?php

/**
 * Expiry Remainder Email
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Expiry_Remainder_Email_Notification' ) ) {

	/**
	 * Class BCN_Expiry_Remainder_Email_Notification.
	 */
	class BCN_Expiry_Remainder_Email_Notification extends BCN_Notifications {

		/**
		 * Class Constructor.
		 */
		public function __construct() {

			$this->id = 'expiry_remainder_email' ;

			// Triggers for this email.
			add_action( 'bcn_expiry_remainder_email' , array( $this , 'trigger' ) , 10 ) ;

			parent::__construct() ;
		}

		/**
		 * Get Enabled.
		 */
		public function get_enabled() {

			return get_option( 'bcn_email_expiry_remainder_email' ) ;
		}

		/*
		 * Default Subject.
		 */

		public function get_default_subject() {

			return get_option( 'bcn_email_subject_for_expiry_notification' ) ;
		}

		/*
		 * Default Message.
		 */

		public function get_default_message() {

			return get_option( 'bcn_email_msg_for_expiry_notification' ) ;
		}

		/**
		 * Trigger the sending of this email.
		 */
		public function trigger( $birthday_coupon_id ) {
			if ( ! $this->is_enabled() ) {
				return ;
			}

			$coupon_obj = bcn_get_coupon( $birthday_coupon_id ) ;

			if ( ! is_object( $coupon_obj ) ) {
				return ;
			}
						
						$rule_obj = $coupon_obj->get_rule() ;

			$coupon_code = $coupon_obj->get_coupon_code() ;
			$coupon_id   = $coupon_obj->get_coupon_id() ;
						$coupon_value = $coupon_obj->get_coupon_value();

			$this->recipient                              = $coupon_obj->get_user_email() ;
			$this->placeholders[ '{coupon_name}' ]        = $coupon_obj->get_coupon_code() ;
			$this->placeholders[ '{coupon_value}' ]       = ( 'percent' == $rule_obj->get_coupon_type() ) ? $coupon_value . '%' : wp_kses_post( bcn_price( $coupon_value ) ) ;
			$this->placeholders[ '{coupon_expiry_date}' ] = esc_html( $coupon_obj->get_expiry_date() ) ;
			$coupon_data                                  = array( 'coupon_obj' => bcn_get_coupon_details( $coupon_id ) ) ;
			$this->placeholders[ '{coupon_details}' ]     = bcn_get_template_html( 'popup-layout.php' , $coupon_data ) ;

			if ( $this->get_recipient() ) {
				$this->send_email( $this->get_recipient() , $this->get_subject() , $this->get_formatted_message() , $this->get_headers() , $this->get_attachments() ) ;
			}
		}

	}

}

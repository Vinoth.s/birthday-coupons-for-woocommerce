<?php

/**
 * Initialize the Plugin.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Install' ) ) {

	/**
	 * Class.
	 */
	class BCN_Install {

		/**
		 *  Class initialization.
		 */
		public static function init() {
			add_action( 'woocommerce_init', array ( __CLASS__, 'check_version' ) ) ;
			add_filter( 'plugin_action_links_' . BCN_PLUGIN_SLUG , array( __CLASS__ , 'settings_link' ) ) ;
		}
		
		
		/**
		 * Check current version of the plugin is updated when activating plugin, if not run updater.
		 */
		public static function check_version() {
			if ( version_compare( get_option( 'bcn_version' ), BCN_VERSION, '>=' ) ) {
				return ;
			}

			self::install() ;
		}

		/**
		 * Install.
		 */
		public static function install() {
			self::set_default_values() ; // default values.
			self::update_version() ;
		}

		/**
		 * Update current version.
		 */
		private static function update_version() {
			update_option( 'bcn_version' , BCN_VERSION ) ;
		}

		/**
		 *  Settings link.
		 */
		public static function settings_link( $links ) {
			$setting_page_link = '<a href="' . bcn_get_settings_page_url() . '">' . esc_html__( 'Settings' , 'birthday-coupons-for-woocommerce' ) . '</a>' ;

			array_unshift( $links , $setting_page_link ) ;

			return $links ;
		}

		/**
		 *  Set the settings default values.
		 */
		public static function set_default_values() {

			if ( ! class_exists( 'BCN_Settings' ) ) {
				include_once( BCN_PLUGIN_PATH . '/inc/admin/menu/class-bcn-settings.php' ) ;
			}

			// Get the settings.
			$settings = BCN_Settings::get_settings_pages() ;

			foreach ( $settings as $setting ) {
				$sections = $setting->get_sections() ;

				if ( bcn_check_is_array( $sections ) ) {
					foreach ( $sections as $section_key => $section ) {
						$settings_array = $setting->get_settings( $section_key ) ;
						foreach ( $settings_array as $value ) {
							//Check if the default and id key is exists.
							if ( isset( $value[ 'default' ] ) && isset( $value[ 'id' ] ) ) {
								//Check if option are saved or not.
								if ( get_option( $value[ 'id' ] ) === false ) {
									add_option( $value[ 'id' ] , $value[ 'default' ] ) ;
								}
							}
						}
					}
				} else {
					$settings_fields = $setting->get_settings( $setting->get_id() ) ;
					foreach ( $settings_fields as $value ) {
						//Check if default and id key is exists.
						if ( isset( $value[ 'default' ] ) && isset( $value[ 'id' ] ) ) {
							//Check if option are saved or not.
							if ( get_option( $value[ 'id' ] ) === false ) {
								add_option( $value[ 'id' ] , $value[ 'default' ] ) ;
							}
						}
					}
				}
			}
		}

	}

	BCN_Install::init() ;
}

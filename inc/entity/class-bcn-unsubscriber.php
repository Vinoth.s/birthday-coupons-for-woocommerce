<?php

/**
 * Unsubscriber.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Unsubscriber' ) ) {

	/**
	 * BCN_Unsubscriber Class.
	 */
	class BCN_Unsubscriber extends BCN_Post {

		/**
		 * Post Type.
		 */
		protected $post_type = BCN_Register_Post_Type::BCN_UNSUBSCRIBER_POSTTYPE ;

		/**
		 * Post Status.
		 */
		protected $post_status = 'publish' ;

		/**
		 * User ID.
		 */
		protected $bcn_user_id ;

		/*
		 * User Name.
		 */
		protected $bcn_user_name ;

		/*
		 * User Email.
		 */
		protected $bcn_user_email ;

		/*
		 * Unsubscribe Email.
		 */
		protected $bcn_unsubscribe_email ;

		/**
		 * Meta data keys
		 */
		protected $meta_data_keys = array(
			'bcn_user_id'           => '' ,
			'bcn_user_name'         => '' ,
			'bcn_user_email'        => '' ,
			'bcn_unsubscribe_email' => '' ,
				) ;

		/**
		 * Set Id.
		 */
		public function set_id( $value ) {

			$this->id = $value ;
		}

		/**
		 * Set User Id.
		 */
		public function set_user_id( $value ) {

			$this->bcn_user_id = $value ;
		}

		/**
		 * Set User Name.
		 */
		public function set_user_name( $value ) {

			$this->bcn_user_name = $value ;
		}

		/**
		 * Set User Email.
		 */
		public function set_user_email( $value ) {

			$this->bcn_user_email = $value ;
		}

		/**
		 * Set Unsubscribe Email.
		 */
		public function set_unsubscribe_email( $value ) {

			$this->bcn_unsubscribe_email = $value ;
		}

		/**
		 * Get Id.
		 */
		public function get_id() {

			return $this->id ;
		}

		/**
		 * Get User Id.
		 */
		public function get_user_id() {

			return $this->bcn_user_id ;
		}

		/**
		 * Get User Name.
		 */
		public function get_user_name() {

			return $this->bcn_user_name ;
		}

		/**
		 * Get User Email.
		 */
		public function get_user_email() {

			return $this->bcn_user_email ;
		}

		/**
		 * Get Unsubscribe Email.
		 */
		public function get_unsubscribe_email() {

			return $this->bcn_unsubscribe_email ;
		}

	}

}
	

<?php

/**
 * Rules.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Coupons' ) ) {

	/**
	 * BCN_Coupons Class.
	 */
	class BCN_Coupons extends BCN_Post {

		/**
		 * Post Type.
		 */
		protected $post_type = BCN_Register_Post_Type::COUPONS_POSTTYPE ;

		/**
		 * Post Status.
		 */
		protected $post_status = 'bcn_unused' ;

		/**
		 * Birthday Coupon ID.
		 */
		protected $bcn_birthday_coupon_id ;

		/**
		 * Matched Rule.
		 */
		protected $bcn_rule ;

		/**
		 * Coupon Id.
		 */
		protected $bcn_coupon_id ;

		/**
		 * Coupon Code.
		 */
		protected $bcn_coupon_code ;

		/**
		 * Coupon Value.
		 */
		protected $bcn_coupon_value ;

		/**
		 * Coupon Issued Date.
		 */
		protected $bcn_issued_date ;

		/**
		 * Birthday Date.
		 */
		protected $bcn_birthday_date ;

		/**
		 * Birthday Date/Month.
		 */
		protected $bcn_birthday_month ;

		/**
		 * Coupon Expiry Date.
		 */
		protected $bcn_expiry_date ;

		/*
		 * Coupon User Email.
		 */
		protected $bcn_user_email ;

		/*
		 * Coupon Used Order Ids.
		 */
		protected $bcn_used_order ;

		/*
		 * Coupon Expired Meta.
		 */
		protected $bcn_expired ;

		/*
		 * Coupon Expiry Remainder Meta.
		 */
		protected $bcn_expiry_remainder ;

		/**
		 * Meta data keys
		 */
		protected $meta_data_keys = array(
			'bcn_rule'               => '' ,
			'bcn_birthday_coupon_id' => '' ,
			'bcn_coupon_id'          => '' ,
			'bcn_coupon_code'        => '' ,
			'bcn_coupon_value'       => '' ,
			'bcn_issued_date'        => '' ,
			'bcn_birthday_date'      => '' ,
			'bcn_birthday_month'     => '' ,
			'bcn_expiry_date'        => '' ,
			'bcn_user_email'         => '' ,
			'bcn_used_orders'        => array() ,
			'bcn_expired'            => '' ,
			'bcn_expiry_remainder'   => ''
				) ;

		/**
		 * Set Id.
		 */
		public function set_id( $value ) {

			$this->id = $value ;
		}

		/**
		 * Set Coupon Id.
		 */
		public function set_coupon_id( $value ) {

			$this->bcn_coupon_id = $value ;
		}

		/**
		 * Set Coupon Code.
		 */
		public function set_coupon_code( $value ) {

			$this->bcn_coupon_code = $value ;
		}

		/**
		 * Set Coupon Value.
		 */
		public function set_coupon_value( $value ) {

			$this->bcn_coupon_value = $value ;
		}

		/**
		 * Set Issued Date.
		 */
		public function set_issued_date( $value ) {

			$this->bcn_issued_date = $value ;
		}

		/**
		 * Set Birthday Date.
		 */
		public function set_birthday_date( $value ) {

			$this->bcn_birthday_date = $value ;
		}
		
		/**
		 * Set Birthday Date/Month.
		 */
		public function set_birthday_month( $value ) {

			$this->bcn_birthday_month = $value ;
		}

		/**
		 * Set Expiry Date.
		 */
		public function set_expiry_date( $value ) {

			$this->bcn_expiry_date = $value ;
		}

		/**
		 * Set User Email.
		 */
		public function set_user_email( $value ) {

			$this->bcn_user_email = $value ;
		}

		/**
		 * Set Used Order Ids.
		 */
		public function set_used_orders( $value ) {

			$this->bcn_used_orders = $value ;
		}

		/**
		 * Set Birthday Coupon Ids.
		 */
		public function set_birthday_coupon_id( $value ) {

			$this->bcn_birthday_coupon_id = $value ;
		}

		/**
		 * Get Id.
		 */
		public function get_id() {

			return $this->id ;
		}

		/**
		 * Get Rule.
		 */
		public function get_rule() {

			return $this->bcn_rule ;
		}

		/**
		 * Get Coupon Id.
		 */
		public function get_coupon_id() {

			return $this->bcn_coupon_id ;
		}

		/**
		 * Get Coupon Code.
		 */
		public function get_coupon_code() {

			return $this->bcn_coupon_code ;
		}

		/**
		 * Get Coupon Value.
		 */
		public function get_coupon_value() {

			return $this->bcn_coupon_value ;
		}

		/**
		 * Get Issued Date.
		 */
		public function get_issued_date() {

			return $this->bcn_issued_date ;
		}

		/**
		 * Get Birthday Date.
		 */
		public function get_birthday_date() {

			return $this->bcn_birthday_date ;
		}
		
		/**
		 * Get Birthday Date/Month.
		 */
		public function get_birthday_month() {

			return $this->bcn_birthday_month ;
		}

		/**
		 * Get Expiry Date.
		 */
		public function get_expiry_date() {

			return $this->bcn_expiry_date ;
		}

		/**
		 * Get User Email.
		 */
		public function get_user_email() {

			return $this->bcn_user_email ;
		}

		/**
		 * Get Used Order Ids.
		 */
		public function get_used_orders() {

			return $this->bcn_used_orders ;
		}

		/**
		 * Get Birthday Coupon Ids.
		 */
		public function get_birthday_coupon_id() {

			return $this->bcn_birthday_coupon_id ;
		}

		/**
		 * Get Email Remainder for Expired.
		 */
		public function get_expired() {

			return $this->bcn_expired ;
		}

		/**
		 * Get Email Remainder for Expired.
		 */
		public function get_expiry_remainder() {

			return $this->bcn_expiry_remainder ;
		}

	}

}
	

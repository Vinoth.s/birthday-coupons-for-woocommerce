<?php

/**
 * Birthday.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Birthday' ) ) {

	/**
	 * BCN_Birthday Class.
	 */
	class BCN_Birthday extends BCN_Post {

		/**
		 * Post Type.
		 */
		protected $post_type = BCN_Register_Post_Type::BCN_BIRTHDAY_POSTTYPE ;

		/**
		 * Post Status.
		 */
		protected $post_status = 'publish' ;

		/**
		 * User Id.
		 */
		protected $bcn_user_id ;

		/**
		 * User Id.
		 */
		protected $bcn_user_email ;

		/**
		 * User Name.
		 */
		protected $bcn_user_name ;

		/**
		 * Coupon Count.
		 */
		protected $bcn_coupon_count ;

		/**
		 * Birthday Date.
		 */
		protected $bcn_birthday_date ;

		/**
		 * Birthday Update on Date.
		 */
		protected $bcn_birthday_updated_on ;

		/**
		 * Birthday Date/Month.
		 */
		protected $bcn_birthday_month ;

		/**
		 * Coupon Issued Date.
		 */
		protected $bcn_issued_date ;

		/**
		 * Coupon Issued Date.
		 */
		protected $bcn_issued_year ;

		/**
		 * Last Coupon Issued Year.
		 */
		protected $bcn_last_issued_year ;

		/**
		 * Meta data keys
		 */
		protected $meta_data_keys = array(
			'bcn_user_id'             => '' ,
			'bcn_user_email'          => '' ,
			'bcn_user_name'           => '' ,
			'bcn_coupon_count'        => '' ,
			'bcn_birthday_date'       => '' ,
			'bcn_birthday_month'      => '' ,
			'bcn_birthday_updated_on' => '' ,
			'bcn_issued_date'         => '' ,
			'bcn_issued_year'         => array() ,
			'bcn_last_issued_year'    => '' ,
				) ;

		/**
		 * Set Id.
		 */
		public function set_id( $value ) {

			$this->id = $value ;
		}

		/**
		 * Set User Id.
		 */
		public function set_user_id( $value ) {

			$this->bcn_user_id = $value ;
		}

		/**
		 * Set User Email.
		 */
		public function set_user_email( $value ) {

			$this->bcn_user_email = $value ;
		}
		
		/**
		 * Set User Name.
		 */
		public function set_user_name( $value ) {

			$this->bcn_user_name = $value ;
		}

		/**
		 * Set Coupon Count.
		 */
		public function set_coupon_count( $value ) {

			$this->bcn_coupon_count = $value ;
		}

		/**
		 * Set Birthday Date.
		 */
		public function set_birthday_date( $value ) {

			$this->bcn_birthday_date = $value ;
		}

		/**
		 * Set Birthday Updated on.
		 */
		public function set_birthday_updated_on( $value ) {

			$this->bcn_birthday_updated_on = $value ;
		}

		/**
		 * Set Birthday Date/Month.
		 */
		public function set_birthday_month( $value ) {

			$this->bcn_birthday_month = $value ;
		}

		/**
		 * Set Issued Date.
		 */
		public function set_issued_date( $value ) {

			$this->bcn_issued_date = $value ;
		}

		/**
		 * Set Issued Year.
		 */
		public function set_issued_year( $value ) {

			$this->bcn_issued_year = $value ;
		}

		/**
		 * Set Last Issued Year.
		 */
		public function set_last_issued_year( $value ) {

			$this->bcn_last_issued_year = $value ;
		}

		/**
		 * Get Id.
		 */
		public function get_id() {

			return $this->id ;
		}

		/**
		 * Get User Id.
		 */
		public function get_user_id() {

			return $this->bcn_user_id ;
		}

		/**
		 * Get User Email.
		 */
		public function get_user_email() {

			return $this->bcn_user_email ;
		}
		
		/**
		 * Get User Name.
		 */
		public function get_user_name() {

			return $this->bcn_user_name ;
		}

		/**
		 * Get Coupon Count.
		 */
		public function get_coupon_count() {

			return $this->bcn_coupon_count ;
		}

		/**
		 * Get Birthday Date.
		 */
		public function get_birthday_date() {

			return $this->bcn_birthday_date ;
		}

		/**
		 * Get Birthday Updated on.
		 */
		public function get_birthday_updated_on() {

			return $this->bcn_birthday_updated_on ;
		}

		/**
		 * Get Birthday Date/Month.
		 */
		public function get_birthday_month() {

			return $this->bcn_birthday_month ;
		}

		/**
		 * Get Issued Date.
		 */
		public function get_issued_date() {

			return $this->bcn_issued_date ;
		}

		/**
		 * Get Issued Year.
		 */
		public function get_issued_year() {

			return $this->bcn_issued_year ;
		}

		/**
		 * Get Last Issued Year.
		 */
		public function get_last_issued_year() {

			return $this->bcn_last_issued_year ;
		}

	}

}
	

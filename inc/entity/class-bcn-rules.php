<?php

/**
 * Rules.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Rules' ) ) {

	/**
	 * BCN_Rules Class.
	 */
	class BCN_Rules extends BCN_Post {

		/**
		 * Post Type.
		 */
		protected $post_type = BCN_Register_Post_Type::BIRTHDAY_RULES_POSTTYPE ;

		/**
		 * Post Status.
		 */
		protected $post_status = 'bcn_inactive' ;

		/**
		 * Enable Rule.
		 */
		protected $bcn_enable ;

		/**
		 * Rule Name.
		 */
		protected $bcn_name ;

		/**
		 * Product Filter Type.
		 */
		protected $bcn_product_filter_type ;

		/**
		 * Included Product.
		 */
		protected $bcn_inc_product ;

		/**
		 * Excluded Product.
		 */
		protected $bcn_exc_product ;

		/**
		 * Included Category.
		 */
		protected $bcn_inc_category = array() ;

		/**
		 * Excluded Category.
		 */
		protected $bcn_exc_category = array() ;

		/**
		 * User Filter Type.
		 */
		protected $bcn_user_filter_type ;

		/**
		 * Included User.
		 */
		protected $bcn_inc_user ;

		/**
		 * Excluded User.
		 */
		protected $bcn_exc_user ;

		/**
		 * Included User Role.
		 */
		protected $bcn_inc_user_role ;

		/**
		 * Excluded User Role.
		 */
		protected $bcn_exc_user_role ;

		/**
		 * Award Only Once Per User.
		 */
		protected $bcn_award_once_per_user ;

		/**
		 * Minimum Cart Total for Coupon.
		 */
		protected $bcn_min_spend ;

		/**
		 * Maximum Cart Total for Coupon.
		 */
		protected $bcn_max_spend ;

		/**
		 * Purchase History.
		 */
		protected $bcn_purchase_history ;

		/**
		 * Purchase History Type.
		 */
		protected $bcn_purchase_history_type ;

		/**
		 * No of Order Placed.
		 */
		protected $bcn_no_of_order ;

		/**
		 * Amount Spent in Site.
		 */
		protected $bcn_total_amount_spent ;

		/**
		 * Coupon Type.
		 */
		protected $bcn_coupon_type ;

		/**
		 * Coupon Percentage.
		 */
		protected $bcn_coupon_percent ;

		/**
		 * Coupon Fixed Value.
		 */
		protected $bcn_coupon_fixed ;

		/**
		 * Coupon Validity.
		 */
		protected $bcn_validity ;

		/**
		 * Meta data keys
		 */
		protected $meta_data_keys = array(
			'bcn_enable'                => '' ,
			'bcn_name'                  => '' ,
			'bcn_product_filter_type'   => '' ,
			'bcn_inc_product'           => array() ,
			'bcn_exc_product'           => array() ,
			'bcn_inc_category'          => array() ,
			'bcn_exc_category'          => array() ,
			'bcn_user_filter_type'      => '' ,
			'bcn_inc_user'              => array() ,
			'bcn_exc_user'              => array() ,
			'bcn_inc_user_role'         => array() ,
			'bcn_exc_user_role'         => array() ,
			'bcn_award_once_per_user'   => '' ,
			'bcn_min_spend'             => '' ,
			'bcn_max_spend'             => '' ,
			'bcn_purchase_history'      => '' ,
			'bcn_purchase_history_type' => '' ,
			'bcn_no_of_order'           => '' ,
			'bcn_total_amount_spent'    => '' ,
			'bcn_coupon_type'           => '' ,
			'bcn_coupon_percent'        => '' ,
			'bcn_coupon_fixed'          => '' ,
			'bcn_validity'              => '' ,
				) ;

		/**
		 * Set Id.
		 */
		public function set_id( $value ) {

			$this->id = $value ;
		}

		/**
		 * Set Enable.
		 */
		public function set_enable( $value ) {

			$this->bcn_enable = $value ;
		}

		/**
		 * Set Name.
		 */
		public function set_name( $value ) {

			$this->bcn_name = $value ;
		}

		/**
		 * Set Product Filter Type.
		 */
		public function set_product_filter_type( $value ) {

			$this->bcn_product_filter_type = $value ;
		}

		/**
		 * Set Include Product.
		 */
		public function set_inc_product( $value ) {

			$this->bcn_inc_product = $value ;
		}

		/**
		 * Set Exclude Product.
		 */
		public function set_exc_product( $value ) {

			$this->bcn_exc_product = $value ;
		}

		/**
		 * Set Include Category.
		 */
		public function set_inc_category( $value ) {

			$this->bcn_inc_category = $value ;
		}

		/**
		 * Set Exclude Category.
		 */
		public function set_exc_category( $value ) {

			$this->bcn_exc_category = $value ;
		}

		/**
		 * Set User Filter Type.
		 */
		public function set_user_filter_type( $value ) {

			$this->bcn_user_filter_type = $value ;
		}

		/**
		 * Set Include User.
		 */
		public function set_inc_user( $value ) {

			$this->bcn_inc_user = $value ;
		}

		/**
		 * Set Exclude User.
		 */
		public function set_exc_user( $value ) {

			$this->bcn_exc_user = $value ;
		}

		/**
		 * Set Include User Role.
		 */
		public function set_inc_user_role( $value ) {

			$this->bcn_inc_user_role = $value ;
		}

		/**
		 * Set Exclude User Role.
		 */
		public function set_exc_user_role( $value ) {

			$this->bcn_exc_user_role = $value ;
		}

		/**
		 * Set Award Once Per User.
		 */
		public function set_award_once_per_user( $value ) {

			$this->bcn_award_once_per_user = $value ;
		}

		/**
		 * Set Minimum Cart Total for Coupon.
		 */
		public function set_min_spend( $value ) {

			$this->bcn_min_spend = $value ;
		}

		/**
		 * Set Maximum Cart Total for Coupon.
		 */
		public function set_max_spend( $value ) {

			$this->bcn_max_spend = $value ;
		}

		/**
		 * Set Purchase History.
		 */
		public function set_purchase_history( $value ) {

			$this->bcn_purchase_history = $value ;
		}

		/**
		 * Set Purchase History Type.
		 */
		public function set_purchase_history_type( $value ) {

			$this->bcn_purchase_history_type = $value ;
		}

		/**
		 * Set No of Order.
		 */
		public function set_no_of_order( $value ) {

			$this->bcn_no_of_order = $value ;
		}

		/**
		 * Set Total Amount Spent in Site.
		 */
		public function set_total_amount_spent( $value ) {

			$this->bcn_total_amount_spent = $value ;
		}

		/**
		 * Set Coupon Type.
		 */
		public function set_coupon_type( $value ) {

			$this->bcn_coupon_type = $value ;
		}

		/**
		 * Set Coupon Percent.
		 */
		public function set_coupon_percent( $value ) {

			$this->bcn_coupon_percent = $value ;
		}

		/**
		 * Set Coupon Fixed.
		 */
		public function set_coupon_fixed( $value ) {

			$this->bcn_coupon_fixed = $value ;
		}

		/**
		 * Set Coupon Validity.
		 */
		public function set_validity( $value ) {

			$this->bcn_validity = $value ;
		}

		/**
		 * Get Id.
		 */
		public function get_id() {

			return $this->id ;
		}

		/**
		 * Get Enable.
		 */
		public function get_enable() {

			return $this->bcn_enable ;
		}

		/**
		 * Get Name.
		 */
		public function get_name() {

			return $this->bcn_name ;
		}

		/**
		 * Get Product Filter Type.
		 */
		public function get_product_filter_type() {

			return $this->bcn_product_filter_type ;
		}

		/**
		 * Get Include Product.
		 */
		public function get_inc_product() {

			return $this->bcn_inc_product ;
		}

		/**
		 * Get Exclude Product.
		 */
		public function get_exc_product() {

			return $this->bcn_exc_product ;
		}

		/**
		 * Get Include Category.
		 */
		public function get_inc_category() {

			return $this->bcn_inc_category ;
		}

		/**
		 * Get Exclude Category.
		 */
		public function get_exc_category() {

			return $this->bcn_exc_category ;
		}

		/**
		 * Get User Filter Type.
		 */
		public function get_user_filter_type() {

			return $this->bcn_user_filter_type ;
		}

		/**
		 * Get Include User.
		 */
		public function get_inc_user() {

			return $this->bcn_inc_user ;
		}

		/**
		 * Get Exclude User.
		 */
		public function get_exc_user() {

			return $this->bcn_exc_user ;
		}

		/**
		 * Get Include User Role.
		 */
		public function get_inc_user_role() {

			return $this->bcn_inc_user_role ;
		}

		/**
		 * Get Exclude User Role.
		 */
		public function get_exc_user_role() {

			return $this->bcn_exc_user_role ;
		}

		/**
		 * Get Award Once Per User.
		 */
		public function get_award_once_per_user() {

			return $this->bcn_award_once_per_user ;
		}

		/**
		 * Get Minimum Cart Total for Coupon.
		 */
		public function get_min_spend() {

			return $this->bcn_min_spend ;
		}

		/**
		 * Get Maximum Cart Total for Coupon.
		 */
		public function get_max_spend() {

			return $this->bcn_max_spend ;
		}

		/**
		 * Get Purchase History.
		 */
		public function get_purchase_history() {

			return $this->bcn_purchase_history ;
		}

		/**
		 * Get Purchase History Type.
		 */
		public function get_purchase_history_type() {

			return $this->bcn_purchase_history_type ;
		}

		/**
		 * Get No of Order.
		 */
		public function get_no_of_order() {

			return $this->bcn_no_of_order ;
		}

		/**
		 * Get Total Amount Spent in Site.
		 */
		public function get_total_amount_spent() {

			return $this->bcn_total_amount_spent ;
		}

		/**
		 * Get Coupon Type.
		 */
		public function get_coupon_type() {

			return $this->bcn_coupon_type ;
		}

		/**
		 * Get Coupon Percent.
		 */
		public function get_coupon_percent() {

			return $this->bcn_coupon_percent ;
		}

		/**
		 * Get Coupon Fixed.
		 */
		public function get_coupon_fixed() {

			return $this->bcn_coupon_fixed ;
		}

		/**
		 * Get Coupon Validity.
		 */
		public function get_validity() {

			return $this->bcn_validity ;
		}

	}

}
	

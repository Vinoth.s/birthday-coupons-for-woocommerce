<?php

/**
 * Common functions.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

include_once( 'bcn-layout-functions.php' ) ;
include_once( 'bcn-template-functions.php' ) ;
include_once( 'bcn-post-functions.php' ) ;

if ( ! function_exists( 'bcn_check_is_array' ) ) {

	/**
	 * Check if resource is array.
	 *
	 * @return bool
	 */
	function bcn_check_is_array( $data ) {
		return ( is_array( $data ) && ! empty( $data ) ) ;
	}

}

if ( ! function_exists( 'bcn_price' ) ) {

	/**
	 *  Display Price based wc_price function.
	 *
	 *  @return string
	 */
	function bcn_price( $price, $echo = false ) {

		if ( $echo ) {
			echo wp_kses_post( wc_price( $price ) ) ;
		}

		return wc_price( $price ) ;
	}

}

if ( ! function_exists( 'bcn_get_rule_default_data' ) ) {

	/**
	 * Get the rule data.
	 *
	 * @return array
	 * */
	function bcn_get_rule_default_data( $rule = array() ) {

		$default_args = array(
			'type'                 => '1' ,
			'order_type'           => '1' ,
			'user_type'            => '1' ,
			'validity_type'        => '1' ,
			'categories'           => array() ,
			'products'             => array() ,
			'user_roles'           => '' ,
			'user_method'          => '1' ,
			'user_register_period' => '1' ,
			'price'                => '' ,
			'product_count'        => '' ,
			'order_count'          => '' ,
			'specific_weekdays'    => array() ,
			'rule_period_start'    => '' ,
			'rule_period_end'      => '' ,
				) ;


		return wp_parse_args( $rule , $default_args ) ;
	}

}

if ( ! function_exists( 'bcn_get_edit_post_link' ) ) {

	/**
	 * Get the edit post link.
	 *
	 * @return string
	 */
	function bcn_get_edit_post_link( $post_id, $name, $page = 'backend' ) {

		if ( 'frontend' == $page ) {
			$args = add_query_arg( 'view-order' , $post_id , get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ) ;
		} else {
			$args = add_query_arg(
					array(
				'post'   => $post_id ,
				'action' => 'edit' ,
					) , admin_url( 'post.php' )
					) ;
		}

		return '<a href="' . esc_url( $args ) . '" >' . $name . '</a>' ;
	}

}

if ( ! function_exists( 'check_is_logged_in_user' ) ) {

	function check_is_logged_in_user() {
		if ( is_user_logged_in() ) {
			if ( 'yes' != get_option( 'bcn_general_logged_in_user_coupon' ) ) {
				return false ;
			}
		} else {
			if ( 'yes' != get_option( 'bcn_general_guest_coupon' ) ) {
				return false ;
			}
		}

		return true ;
	}

}

if ( ! function_exists( 'bcn_customize_array_position' ) ) {

	function bcn_customize_array_position( $array, $key, $new_value ) {
		$keys  = array_keys( $array ) ;
		$index = array_search( $key , $keys ) ;
		$pos   = false === $index ? count( $array ) : $index + 1 ;

		$new_value = is_array( $new_value ) ? $new_value : array( $new_value ) ;

		return array_merge( array_slice( $array , 0 , $pos ) , $new_value , array_slice( $array , $pos ) ) ;
	}

}

if ( ! function_exists( 'bcn_get_coupon_discount_type' ) ) {

	/**
	 * Display Discount Type
	 */
	function bcn_get_coupon_discount_type( $key = '' ) {
		$discount_type = array(
			'fixed_cart' => esc_html__( 'Fixed Discount' , 'birthday-coupons-for-woocommerce' ) ,
			'percent'    => esc_html__( 'Percentage Discount' , 'birthday-coupons-for-woocommerce' ) ,
				) ;

		if ( ! empty( $key ) && isset( $discount_type[ $key ] ) ) {
			return $discount_type[ $key ] ;
		}

		return $discount_type ;
	}

}

if ( ! function_exists( 'bcn_get_product_name' ) ) {

	/**
	 * Get Product name with link
	 * 
	 * @return String
	 * */
	function bcn_get_product_name( $product_ids ) {

		if ( ! bcn_check_is_array( $product_ids ) ) {
			return '-' ;
		}

		end( $product_ids ) ;

		$last_key = key( $product_ids ) ;
		$views    = '' ;

		foreach ( $product_ids as $key => $product_id ) {
			$views .= '<a href="' . admin_url( 'post.php?post=' . absint( $product_id ) . '&action=edit' ) . '" >' . get_the_title( $product_id ) . ' (#' . $product_id . ')</a>' ;

			if ( $last_key == $key ) {
				break ;
			}

			$views .= ' , ' ;
		}

		return $views ;
	}

}

if ( ! function_exists( 'bcn_get_category_name' ) ) {

	/**
	 * Get Category name with link
	 * 
	 * @return boolean
	 * */
	function bcn_get_category_name( $category_ids ) {

		if ( ! bcn_check_is_array( $category_ids ) ) {
			return '-' ;
		}

		end( $category_ids ) ;

		$last_key = key( $category_ids ) ;
		$views    = '' ;

		foreach ( $category_ids as $key => $category_id ) {
			$category_name = bcn_get_categories( $category_id ) ;

			$views .= $category_name ;

			if ( $last_key == $key ) {
				break ;
			}

			$views .= ' , ' ;
		}

		return $views ;
	}

}

if ( ! function_exists( 'bcn_get_coupon_details' ) ) {

	function bcn_get_coupon_details( $coupon_id ) {
		if ( empty( $coupon_id ) ) {
			return ;
		}

		$coupon_obj = new WC_Coupon( $coupon_id ) ;

		if ( ! is_object( $coupon_obj ) ) {
			return ;
		}

		$minimum_amount    = $coupon_obj->get_minimum_amount() ;
		$maximum_amount    = $coupon_obj->get_maximum_amount() ;
		$usage_count       = $coupon_obj->get_usage_count() ;
		$usage_limit       = $coupon_obj->get_usage_limit() ;
		$discount_type     = bcn_get_coupon_discount_type( $coupon_obj->get_discount_type() ) ;
		$discount          = ( 'fixed_cart' == $coupon_obj->get_discount_type() || 'fixed_product' == $coupon_obj->get_discount_type() ) ? bcn_price( $coupon_obj->get_amount() , false ) : $coupon_obj->get_amount() . '%' ;
		$expiry_date       = empty( $coupon_obj->get_date_expires() ) ? '-' : BCN_Date_Time::get_wp_format_datetime( $coupon_obj->get_date_expires() , 'Y-m-d H:i:A' , true , true ) ;
		$individual_use    = $coupon_obj->get_individual_use() ;
		$free_shipping     = $coupon_obj->get_free_shipping() ;
		$exclude_sale_item = $coupon_obj->get_exclude_sale_items() ;
		$include_products  = bcn_get_product_name( $coupon_obj->get_product_ids() ) ;
		$exclude_products  = bcn_get_product_name( $coupon_obj->get_excluded_product_ids() ) ;
		$include_category  = bcn_get_category_name( $coupon_obj->get_product_categories() ) ;
		$exclude_category  = bcn_get_category_name( $coupon_obj->get_excluded_product_categories() ) ;
		$allowed_emails    = implode( ',' , $coupon_obj->get_email_restrictions() ) ;

		$over_view_datas = array(
			'min_amount'        => array(
				'label' => esc_html__( 'Minimum Amount' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => bcn_price( $minimum_amount , false ) ,
				'disp'  => empty( $minimum_amount ) ? false : true
			) ,
			'max_amount'        => array(
				'label' => esc_html__( 'Maximum Amount' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => bcn_price( $maximum_amount , false ) ,
				'disp'  => empty( $maximum_amount ) ? false : true
			) ,
			'discount_type'     => array(
				'label' => esc_html__( 'Discount Type' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $discount_type ,
				'disp'  => true
			) ,
			'discount_amount'   => array(
				'label' => esc_html__( 'Discount Amount' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $discount ,
				'disp'  => true
			) ,
			'individual_use'    => array(
				'label' => esc_html__( 'Individual Use' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => empty( $individual_use ) ? esc_html__( 'No' , 'birthday-coupons-for-woocommerce' ) : esc_html__( 'Yes' , 'birthday-coupons-for-woocommerce' ) ,
				'disp'  => true
			) ,
			'usage_count'       => array(
				'label' => esc_html__( 'Usage Count' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $usage_count ,
				'disp'  => empty( $usage_count ) ? false : true
			) ,
			'usage_limit'       => array(
				'label' => esc_html__( 'Usage Limit' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $usage_limit ,
				'disp'  => empty( $usage_limit ) ? false : true
			) ,
			'expiry_date'       => array(
				'label' => esc_html__( 'Expiry Date' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $expiry_date ,
				'disp'  => true
			) ,
			'free_shipping'     => array(
				'label' => esc_html__( 'Free Shipping' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => empty( $free_shipping ) ? esc_html__( 'No' , 'birthday-coupons-for-woocommerce' ) : esc_html__( 'Yes' , 'birthday-coupons-for-woocommerce' ) ,
				'disp'  => true
			) ,
			'exclude_sale_item' => array(
				'label' => esc_html__( 'Exclude Sale Item' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => empty( $exclude_sale_item ) ? esc_html__( 'No' , 'birthday-coupons-for-woocommerce' ) : esc_html__( 'Yes' , 'birthday-coupons-for-woocommerce' ) ,
				'disp'  => true
			) ,
			'include_products'  => array(
				'label' => esc_html__( 'Include Products' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $include_products ,
				'disp'  => ( '-' == $include_products ) ? false : true
			) ,
			'exclude_products'  => array(
				'label' => esc_html__( 'Exclude Products' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $exclude_products ,
				'disp'  => ( '-' == $exclude_products ) ? false : true
			) ,
			'include_category'  => array(
				'label' => esc_html__( 'Include Categories' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $include_category ,
				'disp'  => ( '-' == $include_category ) ? false : true
			) ,
			'exclude_category'  => array(
				'label' => esc_html__( 'Exclude Categories' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $exclude_category ,
				'disp'  => ( '-' == $exclude_category ) ? false : true
			) ,
			'allowed_emails'    => array(
				'label' => esc_html__( 'Allowed Email' , 'birthday-coupons-for-woocommerce' ) ,
				'value' => $allowed_emails ,
				'disp'  => ( '' == $allowed_emails ) ? false : true
			) ,
				) ;

		return $over_view_datas ;
	}

}

if ( ! function_exists( 'bcn_get_settings_page_url' ) ) {

	/**
	 * Get Settings page URL.
	 *
	 * @return URL
	 */
	function bcn_get_settings_page_url( $args = array() ) {

		$url = add_query_arg( array( 'page' => 'bcn_settings' ) , admin_url( 'admin.php' ) ) ;

		if ( bcn_check_is_array( $args ) ) {
			$url = add_query_arg( $args , $url ) ;
		}

		return $url ;
	}

}

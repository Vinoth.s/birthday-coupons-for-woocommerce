<?php

/**
 * Template functions.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! function_exists( 'bcn_get_template' ) ) {

	/**
	 *  Get other templates from themes.
	 * 
	 * @return void
	 */
	function bcn_get_template( $template_name, $args = array() ) {

		wc_get_template( $template_name , $args , BCN_FOLDER_NAME . '/' , BCN()->templates() ) ;
	}

}

if ( ! function_exists( 'bcn_get_template_html' ) ) {

	/**
	 *  Like bcn_get_template, but returns the HTML instead of outputting.
	 *
	 *  @return string
	 */
	function bcn_get_template_html( $template_name, $args = array() ) {

		ob_start() ;
		bcn_get_template( $template_name , $args ) ;
		return ob_get_clean() ;
	}

}

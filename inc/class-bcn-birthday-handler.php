<?php

/**
 * Birthday Coupon Handler
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Birthday_Coupon_Handler' ) ) {

	/**
	 * BCN_Birthday_Coupon_Handler Class.
	 */
	class BCN_Birthday_Coupon_Handler {

		/**
		 * Class Initialization.
		 */
		public static function init() {
			//Award Coupon for User.
			add_action( 'bcn_birthday_cron' , array( __CLASS__ , 'award_coupon' ) ) ;

			// Validate Birthday Date in Registration Form.
			if ( 'yes' === get_option( 'bcn_general_mandatory_field', 'no' ) ) {
				add_filter( 'woocommerce_process_registration_errors', array( __CLASS__, 'validate_register_form' ) );
				add_filter( 'woocommerce_save_account_details_errors', array( __CLASS__, 'validate_register_form' ) );
				add_action( 'woocommerce_after_checkout_validation', array( __CLASS__, 'checkout_validation' ), 10, 2 );
			}

			//Save Birthday Date in Registration Form.
			add_action( 'user_register' , array( __CLASS__ , 'update_birthday_date' ) , 10 , 1 ) ;
			//Save Birthday Date in Account Details.
			add_action( 'woocommerce_save_account_details' , array( __CLASS__ , 'update_birthday_date' ) , 10 , 1 ) ;
			//Update Order Ids in which Coupon Used
			add_action( 'woocommerce_checkout_update_order_meta' , array( __CLASS__ , 'update_coupon_used_orders' ) ) ;
			//Delete all birthday related data
			add_action( 'delete_user' , array( __CLASS__ , 'delete_birthday_data_if_user_deleted' ) ) ;
		}

		/**
		 * Award Coupon for User.
		 */
		public static function award_coupon() {
			if ( ! check_is_logged_in_user() ) {
				return ;
			}

			$args = array(
				'meta_query' => array(
					'relation' => 'AND' ,
					array(
						'key'     => 'bcn_birthday_month' ,
						'value'   => gmdate( 'm-d' ) ,
						'compare' => '==' ,
					) ,
					array(
						'relation' => 'OR' ,
						array(
							'key'     => 'bcn_last_issued_year' ,
							'value'   => gmdate( 'Y' ) ,
							'compare' => '>=' ,
						) ,
						array(
							'key'     => 'bcn_last_issued_year' ,
							'compare' => 'NOT EXISTS' ,
						) ,
					) ,
				) ,
					) ;

			$birthday_ids = bcn_get_birthday_coupon_ids( $args ) ;

			if ( ! bcn_check_is_array( $birthday_ids ) ) {
				return ;
			}

			$rule_ids = bcn_get_rule_ids() ;

			if ( ! bcn_check_is_array( $rule_ids ) ) {
				return ;
			}

			$matched_users = array() ;

			foreach ( $birthday_ids as $birthday_id ) {

				if ( ! self::check_if_already_awarded( $birthday_id ) ) {
					continue ;
				}

				$birthday_coupon_obj = bcn_get_birthday_coupon( $birthday_id ) ;
				$user_id             = $birthday_coupon_obj->get_user_id() ;

				foreach ( $rule_ids as $id ) {
					$rule = bcn_get_rule( $id ) ;

					if ( ! is_object( $rule ) ) {
						continue ;
					}

					if ( ( 'yes' == $rule->get_award_once_per_user() ) && ( 'yes' == get_user_meta( $user_id , 'bcn_coupon_awarded' , true ) ) ) {
						continue ;
					}

					if ( ! self::birthday_date( $birthday_coupon_obj ) ) {
						return false ;
					}

					if ( ! self::validate_user( $birthday_coupon_obj , $rule ) ) {
						continue ;
					}

					$matched_users[ $birthday_id ][] = $rule ;
				}
			}

			if ( ! bcn_check_is_array( $matched_users ) ) {
				return ;
			}

			$rule_priority = get_option( 'bcn_general_rule_priority' ) ;

			foreach ( $matched_users as $birthday_ids => $rules ) {

				if ( ! bcn_check_is_array( $rules ) ) {
					continue ;
				}

				$rule = ( '1' == $rule_priority ) ? reset( $rules ) : end( $rules ) ;

				self::create_coupon( $birthday_ids , $rule ) ;
			}
		}
		/**
		 * Validate Birthday Date in My Account
		 *
		 * @since 1.0
		 * @param Array  $data Checkout Data.
		 * @param Object $errors Error messages.
		 * @return Array
		 */
		public static function checkout_validation( $data, $errors ) {
			if ( isset( $_REQUEST['bcn_birthday_date'] ) ) {
				$birthday_date = wc_clean( wp_unslash( $_REQUEST['bcn_birthday_date'] ) );

				if ( empty( $birthday_date ) ) {
					$errors->add( 'birthday_error', esc_html__( 'You must fill a birthday date.', 'birthday-coupons-for-woocommerce' ) );
				}

				return $errors;
			}
		}
		/**
		 * Validate Birthday Date in My Account
		 *
		 * @since 1.0
		 * @param Object $errors Error messages.
		 * @return Array
		 */
		public static function validate_register_form( $errors ) {
			if ( isset( $_REQUEST['bcn_birthday_date'] ) ) {
				$birthday_date = wc_clean( wp_unslash( $_REQUEST['bcn_birthday_date'] ) );

				if ( empty( $birthday_date ) ) {
					$errors->add( 'birthday_error', esc_html__( 'You must fill a birthday date.', 'birthday-coupons-for-woocommerce' ) );
				}
			}

			return $errors;
		}

		/**
		 * Save Birthday Date in Account Details
		 *
		 * @since 1.0
		 */
		public static function update_birthday_date( $user_id ) {
			$user_data  = get_userdata( $user_id ) ;
			$user_name  = $user_data->display_name ;
			$user_email = $user_data->user_email ;
			if ( isset( $_REQUEST[ 'bcn_birthday_date' ] ) && ! empty( $_REQUEST[ 'bcn_birthday_date' ] ) ) {
				$birthday_date = wc_clean( wp_unslash( $_REQUEST[ 'bcn_birthday_date' ] ) ) ;
				update_user_meta( $user_id , 'bcn_birthday_date' , $birthday_date ) ;

				if ( isset( $_REQUEST[ 'bcn_unsubscribe_email' ] ) && ! empty( $_REQUEST[ 'bcn_unsubscribe_email' ] ) ) {
					update_user_meta( $user_id , 'bcn_unsubscribe_email' , 'yes' ) ;

					$args = array(
						'meta_query' => array(
							'relation' => 'AND' ,
							array(
								'key'     => 'bcn_user_id' ,
								'value'   => $user_id ,
								'compare' => '==' ,
							) ,
							array(
								'key'     => 'bcn_unsubscribe_email' ,
								'compare' => 'EXISTS' ,
							) ,
						) ,
							) ;

					$unsubscriber = bcn_get_unsubscriber_ids( $args ) ;

					if ( ! bcn_check_is_array( $unsubscriber ) ) {
						$unsubscribe_args = array(
							'bcn_user_id'           => $user_id ,
							'bcn_user_email'        => $user_email ,
							'bcn_user_name'         => $user_name ,
							'bcn_unsubscribe_email' => 'yes' ,
								) ;

						$unsubscriber_id = bcn_create_new_unsubscriber( $unsubscribe_args ) ;
					}
				} else {
					update_user_meta( $user_id , 'bcn_unsubscribe_email' , 'no' ) ;
					self::remove_unsubscriber( $user_id ) ;
				}

				self::create_birthday_coupon( $user_id , $user_name , $user_email , $birthday_date ) ;

				if ( self::birthday_date( $birthday_date ) ) {
					self::award_coupon_if_update( $user_id , $birthday_date ) ;
				}
			}
		}

		/**
		 * Remove Unsubscriber List
		 */
		public static function remove_unsubscriber( $user_id ) {

			$args = array(
				'meta_query' => array(
					'relation' => 'AND' ,
					array(
						'key'     => 'bcn_user_id' ,
						'value'   => $user_id ,
						'compare' => '==' ,
					) ,
					array(
						'key'     => 'bcn_unsubscribe_email' ,
						'compare' => 'EXISTS' ,
					) ,
				) ,
					) ;

			$unsubscriber = bcn_get_unsubscriber_ids( $args ) ;

			if ( bcn_check_is_array( $unsubscriber ) ) {
				$unsubscriber_id = reset( $unsubscriber ) ;
				bcn_delete_unsubscriber( $unsubscriber_id ) ;
			}
		}

		/**
		 * Create Birthday Coupon Post
		 */
		public static function create_birthday_coupon( $user_id, $user_name, $user_email, $birthday_date ) {
			$args = array(
				'meta_query' => array(
					array(
						'key'     => 'bcn_user_email' ,
						'value'   => $user_email ,
						'compare' => '==' ,
					) ,
				) ,
					) ;

			$birthday_coupons = bcn_get_birthday_coupon_ids( $args ) ;

			$bdate_obj = BCN_Date_Time::get_date_time_object( $birthday_date ) ;

			if ( ! bcn_check_is_array( $birthday_coupons ) ) {
				$birthday_coupon_args = array(
					'bcn_user_id'             => $user_id ,
					'bcn_user_email'          => $user_email ,
					'bcn_user_name'           => $user_name ,
					'bcn_birthday_date'       => $birthday_date ,
					'bcn_birthday_updated_on' => gmdate( 'Y-m-d' ) ,
					'bcn_birthday_month'      => $bdate_obj->format( 'm-d' ) ,
						) ;

				$birthday_coupon_id = bcn_create_new_birthday_coupon( $birthday_coupon_args ) ;
			} else {
				$birthday_coupon_id  = reset( $birthday_coupons ) ;
				$birthday_coupon_obj = bcn_get_birthday_coupon( $birthday_coupon_id ) ;

				$birthday_coupon_args = array(
					'bcn_birthday_date'       => $birthday_date ,
					'bcn_birthday_updated_on' => gmdate( 'Y-m-d' ) ,
					'bcn_birthday_month'      => $bdate_obj->format( 'm-d' ) ,
						) ;

				bcn_update_birthday_coupon( $birthday_coupon_id , $birthday_coupon_args ) ;
			}
		}

		/**
		 * Award Coupon If User Update
		 */
		public static function award_coupon_if_update( $user_id, $birthday_date ) {
			if ( ! check_is_logged_in_user() ) {
				return ;
			}

			$bdate_obj = BCN_Date_Time::get_date_time_object( $birthday_date ) ;

			$args = array(
				'meta_query' => array(
					'relation' => 'AND' ,
					array(
						'key'     => 'bcn_user_id' ,
						'value'   => $user_id ,
						'compare' => '==' ,
					) ,
					array(
						'key'     => 'bcn_birthday_month' ,
						'value'   => $bdate_obj->format( 'm-d' ) ,
						'compare' => '==' ,
					) ,
					array(
						'relation' => 'OR' ,
						array(
							'key'     => 'bcn_last_issued_year' ,
							'value'   => gmdate( 'Y' ) ,
							'compare' => '>=' ,
						) ,
						array(
							'key'     => 'bcn_last_issued_year' ,
							'compare' => 'NOT EXISTS' ,
						) ,
					) ,
				) ,
					) ;

			$birthday_ids = bcn_get_birthday_coupon_ids( $args ) ;

			if ( ! bcn_check_is_array( $birthday_ids ) ) {
				return ;
			}

			$rule_ids = bcn_get_rule_ids() ;

			if ( ! bcn_check_is_array( $rule_ids ) ) {
				return ;
			}

			$matched_users = array() ;

			foreach ( $birthday_ids as $birthday_id ) {

				if ( ! self::check_if_already_awarded( $birthday_id ) ) {
					continue ;
				}

				$birthday_coupon_obj = bcn_get_birthday_coupon( $birthday_id ) ;

				foreach ( $rule_ids as $id ) {
					$rule = bcn_get_rule( $id ) ;

					if ( ! is_object( $rule ) ) {
						continue ;
					}

					if ( ( 'yes' == $rule->get_award_once_per_user() ) && ( 'yes' == get_user_meta( $user_id , 'bcn_coupon_awarded' , true ) ) ) {
						continue ;
					}

					if ( ! self::validate_user( $birthday_coupon_obj , $rule ) ) {
						continue ;
					}

					$matched_users[ $birthday_id ][] = $rule ;
				}
			}

			if ( ! bcn_check_is_array( $matched_users ) ) {
				return ;
			}

			$rule_priority = get_option( 'bcn_general_rule_priority' ) ;

			foreach ( $matched_users as $birthday_ids => $rules ) {

				if ( ! bcn_check_is_array( $rules ) ) {
					continue ;
				}

				$rule = ( '1' == $rule_priority ) ? reset( $rules ) : end( $rules ) ;

				self::create_coupon( $birthday_ids , $rule ) ;
			}
		}

		/**
		 * Check If Already awarded.
		 */
		public static function check_if_already_awarded( $birthday_id ) {
			$current_year = gmdate( 'Y' ) ;
			$awarded_year = ( array ) get_post_meta( $birthday_id , 'bcn_issued_year' , true ) ;
			if ( in_array( $current_year , $awarded_year ) ) {
				return false ;
			}

			return true ;
		}

		/**
		 * Validate Rule.
		 */
		public static function validate_user( $birthday_coupon_obj, $rule ) {
			if ( ! self::user_filter( $birthday_coupon_obj , $rule ) ) {
				return false ;
			}

			if ( ! self::purchase_history( $birthday_coupon_obj , $rule ) ) {
				return false ;
			}

			return true ;
		}

		/**
		 * Check If user is valid for Birthday Coupon.
		 */
		public static function user_filter( $birthday_coupon_obj, $rule ) {

			$user_id   = $birthday_coupon_obj->get_user_id() ;
			$user_meta = get_userdata( $user_id ) ;
			$user_role = is_object( $user_meta ) ? $user_meta->roles : esc_html( 'Guest' , 'birthday-coupons-for-woocommerce' ) ;

			$user_selection_type = $rule->get_user_filter_type() ;
			if ( '1' == $user_selection_type ) {
				return true ;
			} elseif ( '2' == $user_selection_type ) {
				$include_user = $rule->get_inc_user() ;
				if ( ! bcn_check_is_array( $include_user ) ) {
					return false ;
				}

				if ( in_array( $user_id , $include_user ) ) {
					return true ;
				}
			} elseif ( '3' == $user_selection_type ) {
				$exclude_user = $rule->get_exc_user() ;
				if ( ! bcn_check_is_array( $exclude_user ) ) {
					return true ;
				}

				if ( ! in_array( $user_id , $exclude_user ) ) {
					return true ;
				}
			} elseif ( '4' == $user_selection_type ) {
				$include_user_role = $rule->get_inc_user_role() ;
				$inc_roles         = array_intersect( $user_role , $include_user_role ) ;
				if ( bcn_check_is_array( $inc_roles ) ) {
					return true ;
				}
			} else {
				$exclude_user_role = $rule->get_exc_user_role() ;
				if ( ! bcn_check_is_array( $exclude_user_role ) ) {
					return true ;
				}

				$exc_roles = array_intersect( $user_role , $exclude_user_role ) ;
				if ( ! bcn_check_is_array( $exc_roles ) ) {
					return true ;
				}
			}
			return false ;
		}

		/**
		 * Check User's Birthday Date.
		 */
		public static function birthday_date( $birthday_coupon_obj ) {

			$birthday_date = is_object( $birthday_coupon_obj ) ? $birthday_coupon_obj->get_birthday_date() : $birthday_coupon_obj ;

			$bdate_obj = BCN_Date_Time::get_date_time_object( $birthday_date ) ;

			$date = gmdate( 'm-d' ) ;

			if ( '1' == get_option( 'bcn_general_send_coupon_on' ) ) {
				$birthday_date = $bdate_obj->format( 'm-d' ) ;
				if ( $birthday_date == $date ) {
					return true ;
				}
			} elseif ( '2' == get_option( 'bcn_general_send_coupon_on' ) ) {
				$no_of_days = ( int ) get_option( 'bcn_general_send_coupon_before_birthday' ) ;

				if ( empty( $no_of_days ) ) {
					return false ;
				}

				$birthday_date = $bdate_obj->modify( '-' . $no_of_days . 'days' ) ;
				$birthday_date = $birthday_date->format( 'm-d' ) ;

				if ( $date >= $birthday_date ) {
					return true ;
				}
			} else {
				$no_of_days = ( int ) get_option( 'bcn_general_send_coupon_after_birthday' ) ;

				if ( empty( $no_of_days ) ) {
					return false ;
				}

				$birthday_date = $bdate_obj->modify( '+' . $no_of_days . 'days' ) ;
				$birthday_date = $birthday_date->format( 'm-d' ) ;

				if ( $date >= $birthday_date ) {
					return true ;
				}
			}

			return false ;
		}

		/**
		 * Purchased History
		 */
		public static function purchase_history( $birthday_coupon_obj, $rule ) {
			$user_id = $birthday_coupon_obj->get_user_id() ;
			if ( 1 == $rule->get_purchase_history_type() ) {
				$order_count = wc_get_customer_order_count( $user_id ) ;
				if ( $order_count >= $rule->get_no_of_order() ) {
					return true ;
				}
			} else {
				$purchased_amount = wc_get_customer_total_spent( $user_id ) ;
				if ( $purchased_amount >= $rule->get_total_amount_spent() ) {
					return true ;
				}
			}
			return false ;
		}

		/**
		 * Create Birthday Coupon for User.
		 */
		public static function create_coupon( $birthday_id, $rule ) {

			$birthday_coupon_obj = bcn_get_birthday_coupon( $birthday_id ) ;
			$user_id             = $birthday_coupon_obj->get_user_id() ;
			$user_name           = $birthday_coupon_obj->get_user_name() ;
			$user_email          = $birthday_coupon_obj->get_user_email() ;
			$coupon_title        = self::coupon_title() ;

			$coupon_data = array(
				'post_title'  => $coupon_title ,
				'post_status' => 'publish' ,
				'post_type'   => 'shop_coupon' ,
					) ;

			$coupon_id = wp_insert_post( $coupon_data ) ;

			$discount_type = $rule->get_coupon_type() ;
			update_post_meta( $coupon_id , 'discount_type' , $discount_type ) ;

			$discount_value = ( 'percent' == $discount_type ) ? $rule->get_coupon_percent() : $rule->get_coupon_fixed() ;
			update_post_meta( $coupon_id , 'coupon_amount' , $discount_value ) ;

			$no_of_days  = $rule->get_validity() ;
			$date_obj    = BCN_Date_Time::get_date_time_object( gmdate( 'Y-m-d' ) ) ;
			$expiry_date = $date_obj->modify( '+' . $no_of_days . 'days' ) ;
			$expiry_date = $expiry_date->format( 'Y-m-d' ) ;
			update_post_meta( $coupon_id , 'expiry_date' , $expiry_date ) ;

			$minimum_amnt = $rule->get_min_spend() ;
			update_post_meta( $coupon_id , 'minimum_amount' , $minimum_amnt ) ;

			$maximum_amnt = $rule->get_max_spend() ;
			update_post_meta( $coupon_id , 'maximum_amount' , $maximum_amnt ) ;
			update_post_meta( $coupon_id , 'usage_limit_per_user' , 1 ) ;

			if ( '2' == $rule->get_product_filter_type() ) {
				$inc_productid = $rule->get_inc_product() ;
				update_post_meta( $coupon_id , 'product_ids' , implode( ',' , array_filter( array_map( 'intval' , $inc_productid ) ) ) ) ;
			} elseif ( '3' == $rule->get_product_filter_type() ) {
				$exc_productid = $rule->get_exc_product() ;
				update_post_meta( $coupon_id , 'exclude_product_ids' , implode( ',' , array_filter( array_map( 'intval' , $exc_productid ) ) ) ) ;
			} elseif ( '4' == $rule->get_product_filter_type() ) {
				$inc_category = $rule->get_inc_category() ;
				update_post_meta( $coupon_id , 'product_categories' , array_filter( array_map( 'intval' , $inc_category ) ) ) ;
			} elseif ( '5' == $rule->get_product_filter_type() ) {
				$exc_category = $rule->get_exc_category() ;
				update_post_meta( $coupon_id , 'exclude_product_categories' , array_filter( array_map( 'intval' , $exc_category ) ) ) ;
			}

			update_post_meta( $coupon_id , 'customer_email' , $user_email ) ;

			if ( 'yes' == $rule->get_award_once_per_user() ) {
				update_user_meta( $user_id , 'bcn_coupon_awarded' , 'yes' ) ;
			}

			$post_args = array(
				'post_parent' => $user_id ,
					) ;

			$args = array(
				'bcn_rule'               => $rule ,
				'bcn_birthday_coupon_id' => $birthday_id ,
				'bcn_coupon_id'          => $coupon_id ,
				'bcn_coupon_code'        => $coupon_title ,
				'bcn_coupon_value'       => $discount_value ,
				'bcn_issued_date'        => gmdate( 'Y-m-d' ) ,
				'bcn_birthday_date'      => $birthday_coupon_obj->get_birthday_date() ,
				'bcn_birthday_month'     => $birthday_coupon_obj->get_birthday_month() ,
				'bcn_expiry_date'        => $expiry_date ,
				'bcn_user_email'         => $user_email ,
					) ;

			$bc_coupon_obj = bcn_create_new_coupon( $args , $post_args ) ;

			if ( $bc_coupon_obj ) {
				$no_of_days = get_option( 'bcn_email_expiration_days' ) ;
				if ( ! empty( $expiry_date ) && ! empty( $no_of_days ) ) {
					$date_to_send_mail = strtotime( '-' . $no_of_days . 'days' , strtotime( $expiry_date ) ) ;
					wp_schedule_single_event( $date_to_send_mail , 'bcn_send_mail_before_expiry' ) ;
				}
			}

			$prev_data = $birthday_coupon_obj->get_issued_year() ;

			if ( bcn_check_is_array( $prev_data ) ) {
				$merged_data = array_merge( $prev_data , array( gmdate( 'Y' ) ) ) ;
			} else {
				$merged_data = array( gmdate( 'Y' ) ) ;
			}

			$previous_count = $birthday_coupon_obj->get_coupon_count() ;

			$birthday_coupon_args = array(
				'bcn_issued_year'      => $merged_data ,
				'bcn_coupon_count'     => ( int ) $previous_count + 1 ,
				'bcn_issued_date'      => gmdate( 'Y-m-d' ) ,
				'bcn_last_issued_year' => gmdate( 'Y' ) ,
					) ;

			bcn_update_birthday_coupon( $birthday_id , $birthday_coupon_args ) ;
			/**
			 * Coupon creation for user.
			 * 
			 * @since 1.0
			 */
			do_action( 'bcn_after_birthday_coupon_creation_for_user' , $user_name , $user_email , $coupon_id , $coupon_title , $discount_value , $discount_type , $user_id ) ;
		}

		/**
		 * Get Coupon Title.
		 */
		public static function coupon_title() {
			$prefix = get_option( 'bcn_general_coupon_prefix' ) ;
			$suffix = get_option( 'bcn_general_coupon_suffix' ) ;
			$length = get_option( 'bcn_general_coupon_length' ) ;
			$type   = get_option( 'bcn_general_coupon_code_type' ) ;

			if ( '1' == $type ) {
				$random_code = '' ;
				for ( $j = 1 ; $j <= $length ; $j ++ ) {
					$random_code .= rand( 0 , 9 ) ;
				}
			} else {
				$characters  = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' ;
				$char_length = strlen( $characters ) ;
				$random_code = '' ;
				for ( $j = 1 ; $j <= $length ; $j ++ ) {
					$random_code .= $characters[ rand( 0 , $char_length - 1 ) ] ;
				}
			}

			return $prefix . $random_code . $suffix ;
		}

		/**
		 * Update the order ids which used the collected coupons.
		 */
		public static function update_coupon_used_orders( $order_id ) {

			if ( ! check_is_logged_in_user() ) {
				return ;
			}

			$order   = new WC_Order( $order_id ) ;
			$user_id = $order->get_user_id() ;

			if ( isset( $_REQUEST[ 'bcn_birthday_date' ] ) && ! empty( $_REQUEST[ 'bcn_birthday_date' ] ) ) {
				$birthday_date = wc_clean( wp_unslash( $_REQUEST[ 'bcn_birthday_date' ] ) ) ;
				$user_email    = isset( $_REQUEST[ 'billing_email' ] ) ? wc_clean( wp_unslash( $_REQUEST[ 'billing_email' ] ) ) : '' ;
				$user_name     = isset( $_REQUEST[ 'billing_first_name' ] ) ? wc_clean( wp_unslash( $_REQUEST[ 'billing_first_name' ] ) ) : '' ;

				if ( is_user_logged_in() ) {
					update_user_meta( $user_id , 'bcn_birthday_date' , $birthday_date ) ;
				}
				
				if ( isset( $_REQUEST[ 'bcn_unsubscribe_email' ] ) && ! empty( $_REQUEST[ 'bcn_unsubscribe_email' ] ) ) {
					update_user_meta( $user_id , 'bcn_unsubscribe_email' , 'yes' ) ;

					$args = array(
						'meta_query' => array(
							'relation' => 'AND' ,
							array(
								'key'     => 'bcn_user_id' ,
								'value'   => $user_id ,
								'compare' => '==' ,
							) ,
							array(
								'key'     => 'bcn_unsubscribe_email' ,
								'compare' => 'EXISTS' ,
							) ,
						) ,
							) ;

					$unsubscriber = bcn_get_unsubscriber_ids( $args ) ;

					if ( ! bcn_check_is_array( $unsubscriber ) ) {
						$unsubscribe_args = array(
							'bcn_user_id'           => $user_id ,
							'bcn_user_email'        => $user_email ,
							'bcn_user_name'         => $user_name ,
							'bcn_unsubscribe_email' => 'yes' ,
								) ;

						$unsubscriber_id = bcn_create_new_unsubscriber( $unsubscribe_args ) ;
					}
				} else {
					update_user_meta( $user_id , 'bcn_unsubscribe_email' , 'no' ) ;
					self::remove_unsubscriber( $user_id ) ;
				}

				self::create_birthday_coupon( $user_id , $user_name , $user_email , $birthday_date ) ;

				if ( self::birthday_date( $birthday_date ) ) {
					self::award_coupon_if_update( $user_id , $birthday_date ) ;
				}
			}

			$applied_coupons = $order->get_items( 'coupon' ) ;

			if ( bcn_check_is_array( $applied_coupons ) ) {
				foreach ( $applied_coupons as $applied_coupon ) {
					$code      = $applied_coupon->get_code() ;
					$coupon    = new WC_Coupon( $code ) ;
					$coupon_id = $coupon->get_id() ;

					$args = array() ;

					$args[ 'post_parent' ] = get_current_user_id() ;
					$args[ 'meta_query' ]  = array(
						array(
							'key'     => 'bcn_coupon_id' ,
							'value'   => $coupon_id ,
							'compare' => '=' ,
						)
							) ;

					$birthday_coupons = bcn_get_coupon_ids( $args ) ;

					if ( bcn_check_is_array( $birthday_coupons ) ) {
						foreach ( $birthday_coupons as $birthday_coupon_id ) {
							$birthday_coupon_obj = bcn_get_coupon( $birthday_coupon_id ) ;

							if ( ! is_object( $birthday_coupon_obj ) ) {
								continue ;
							}

							$old_order_ids = $birthday_coupon_obj->get_used_orders() ;
							if ( bcn_check_is_array( $old_order_ids ) ) {
								$used_order_ids = array_merge( $old_order_ids , array( $order_id ) ) ;
							} else {
								$used_order_ids = array( $order_id ) ;
							}

							$meta_args = array(
								'bcn_used_orders' => $used_order_ids
									) ;


							$post_args = array(
								'post_status' => 'bcn_used'
									) ;

							bcn_update_coupon( $birthday_coupon_id , $meta_args , $post_args ) ;
						}
					}
				}
			}
		}

		/**
		 * Delete Birthday Date if user deleted.
		 */
		public static function delete_birthday_data_if_user_deleted( $user_id ) {
			$user_data  = get_userdata( $user_id ) ;
			$user_email = $user_data->user_email ;

			$args = array(
				'meta_query' => array(
					array(
						'key'     => 'bcn_user_email' ,
						'value'   => $user_email ,
						'compare' => '==' ,
					) ,
				) ,
					) ;

			$birthday_coupons = bcn_get_birthday_coupon_ids( $args ) ;

			if ( bcn_check_is_array( $birthday_coupons ) ) {
				$birthday_coupon_id = reset( $birthday_coupons ) ;
				bcn_delete_birthday_coupon( $birthday_coupon_id ) ;
			}
		}

	}

	BCN_Birthday_Coupon_Handler::init() ;
}

<?php

/**
 * Admin Custom Post Type.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Register_Post_Type' ) ) {

	/**
	 * Class.
	 */
	class BCN_Register_Post_Type {

		/**
		 * Birthday Coupon Post Type.
		 */
		const BCN_BIRTHDAY_POSTTYPE = 'bcn_birthday' ;

		/**
		 * Birthday Rules Post Type.
		 */
		const BIRTHDAY_RULES_POSTTYPE = 'bcn_rules' ;

		/**
		 * Issued Coupons Post Type.
		 */
		const COUPONS_POSTTYPE = 'bcn_coupons' ;

		/**
		 * Unsubscriber Type.
		 */
		const BCN_UNSUBSCRIBER_POSTTYPE = 'bcn_unsubscriber' ;

		/**
		 * Class initialization.
		 */
		public static function init() {

			add_action( 'init', array ( __CLASS__, 'register_custom_post_types' ) ) ;
		}

		/**
		 * Register Custom Post types.
		 */
		public static function register_custom_post_types() {
			if ( ! is_blog_installed() ) {
				return ;
			}

			$custom_post_type	 = array (
				self::BCN_BIRTHDAY_POSTTYPE		 => array ( 'BCN_Register_Post_Type', 'birthday_coupon_post_type_args' ),
				self::BIRTHDAY_RULES_POSTTYPE	 => array ( 'BCN_Register_Post_Type', 'rules_post_type_args' ),
				self::COUPONS_POSTTYPE			 => array ( 'BCN_Register_Post_Type', 'coupons_post_type_args' ),
				self::BCN_UNSUBSCRIBER_POSTTYPE	 => array ( 'BCN_Register_Post_Type', 'unsubscriber_post_type_args' ),
					) ;
			/**
			 * Custom post type.
			 * 
			 * @since 1.0
			 */
			$custom_post_type	 = apply_filters( 'bcn_add_custom_post_type', $custom_post_type ) ;

			if ( ! bcn_check_is_array( $custom_post_type ) ) {
				return ;
			}

			foreach ( $custom_post_type as $post_type => $args_function ) {
				$args = array () ;
				if ( $args_function ) {
					$args = call_user_func_array( $args_function, $args ) ;
				}

				if ( ! post_type_exists( $post_type ) ) {

					// Register custom post type.
					register_post_type( $post_type, $args ) ;
				}
			}
		}

		/**
		 * Prepare Birthday Post Type Arguments.
		 */
		public static function birthday_coupon_post_type_args() {
			/**
			 * Birthday post type args.
			 * 
			 * @since 1.0
			 */
			return apply_filters( 'bcn_birthday_post_type_args', array (
				'labels'				 => array (
					'name'				 => esc_html__( 'Birthday Coupons', 'birthday-coupons-for-woocommerce' ),
					'singular_name'		 => esc_html__( 'Birthday Coupons', 'birthday-coupons-for-woocommerce' ),
					'menu_name'			 => esc_html__( 'Birthday Coupons', 'birthday-coupons-for-woocommerce' ),
					'add_new'			 => esc_html__( 'Add New Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'add_new_item'		 => esc_html__( 'Add New Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'edit'				 => esc_html__( 'Edit Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'edit_item'			 => esc_html__( 'View Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'new_item'			 => esc_html__( 'New Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'view'				 => esc_html__( 'View Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'view_item'			 => esc_html__( 'View Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'search_items'		 => esc_html__( 'Search Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
					'not_found'			 => esc_html__( 'No Birthday Coupon found', 'birthday-coupons-for-woocommerce' ),
					'not_found_in_trash' => esc_html__( 'No Birthday Coupon found in trash', 'birthday-coupons-for-woocommerce' ),
				),
				'description'			 => esc_html__( 'Here you can able to see list of Birthday Coupon', 'birthday-coupons-for-woocommerce' ),
				'public'				 => true,
				'show_ui'				 => true,
				'supports'				 => false,
				'capability_type'		 => 'post',
				'show_in_menu'			 => 'bcn_birthday',
				'publicly_queryable'	 => false,
				'exclude_from_search'	 => true,
				'hierarchical'			 => false, // Hierarchical causes memory issues - WP loads all records!
				'show_in_nav_menus'		 => false,
				'capabilities'			 => array (
					'publish_posts'			 => 'publish_posts',
					'edit_posts'			 => 'edit_posts',
					'edit_others_posts'		 => 'edit_others_posts',
					'delete_posts'			 => 'delete_posts',
					'delete_others_posts'	 => 'delete_others_posts',
					'read_private_posts'	 => 'read_private_posts',
					'edit_post'				 => 'edit_post',
					'delete_post'			 => 'delete_post',
					'read_post'				 => 'read_post',
					'create_posts'			 => 'do_not_allow',
				),
				'map_meta_cap'			 => true,
					)
					) ;
		}

		/**
		 * Prepare Rules Post type arguments
		 */
		public static function rules_post_type_args() {
			/**
			 * Rules post type args.
			 * 
			 * @since 1.0
			 */
			return apply_filters( 'bcn_rules_post_type_args', array (
				'labels'				 => array (
					'name'				 => esc_html__( 'Rule(s)', 'birthday-coupons-for-woocommerce' ),
					'singular_name'		 => esc_html__( 'Rule(s)', 'birthday-coupons-for-woocommerce' ),
					'menu_name'			 => esc_html__( 'Rule(s)', 'birthday-coupons-for-woocommerce' ),
					'add_new'			 => esc_html__( 'Add New Rule', 'birthday-coupons-for-woocommerce' ),
					'add_new_item'		 => esc_html__( 'Add New Rule', 'birthday-coupons-for-woocommerce' ),
					'edit'				 => esc_html__( 'Edit Rule', 'birthday-coupons-for-woocommerce' ),
					'edit_item'			 => esc_html__( 'View Rule', 'birthday-coupons-for-woocommerce' ),
					'new_item'			 => esc_html__( 'New Rule', 'birthday-coupons-for-woocommerce' ),
					'view'				 => esc_html__( 'View Rule', 'birthday-coupons-for-woocommerce' ),
					'view_item'			 => esc_html__( 'View Rule', 'birthday-coupons-for-woocommerce' ),
					'search_items'		 => esc_html__( 'Search Rule(s)', 'birthday-coupons-for-woocommerce' ),
					'not_found'			 => esc_html__( 'No Rule found', 'birthday-coupons-for-woocommerce' ),
					'not_found_in_trash' => esc_html__( 'No Rule found in trash', 'birthday-coupons-for-woocommerce' ),
				),
				'description'			 => esc_html__( 'Here you can able to see list of Rules', 'birthday-coupons-for-woocommerce' ),
				'public'				 => true,
				'show_ui'				 => true,
				'supports'				 => false,
				'capability_type'		 => 'post',
				'show_in_menu'			 => 'bcn_birthday',
				'publicly_queryable'	 => false,
				'exclude_from_search'	 => true,
				'hierarchical'			 => false, // Hierarchical causes memory issues - WP loads all records!
				'show_in_nav_menus'		 => false,
				'capabilities'			 => array (
					'publish_posts'			 => 'publish_posts',
					'edit_posts'			 => 'edit_posts',
					'edit_others_posts'		 => 'edit_others_posts',
					'delete_posts'			 => 'delete_posts',
					'delete_others_posts'	 => 'delete_others_posts',
					'read_private_posts'	 => 'read_private_posts',
					'edit_post'				 => 'edit_post',
					'delete_post'			 => 'delete_post',
					'read_post'				 => 'read_post',
				),
				'map_meta_cap'			 => true,
					)
					) ;
		}

		/**
		 * Prepare Coupons Post type arguments
		 */
		public static function coupons_post_type_args() {
			/**
			 * Coupons post type args.
			 * 
			 * @since 1.0
			 */
			return apply_filters( 'bcn_coupons_post_type_args', array (
				'label'				 => esc_html__( 'Coupons', 'birthday-coupons-for-woocommerce' ),
				'public'			 => false,
				'hierarchical'		 => false,
				'supports'			 => false,
				'capability_type'	 => 'post',
				'rewrite'			 => false,
					)
					) ;
		}

		/**
		 * Prepare Unsubscriber Post type arguments
		 */
		public static function unsubscriber_post_type_args() {
			/**
			 * Un subscribe post type args.
			 * 
			 * @since 1.0
			 */
			return apply_filters( 'bcn_unsubscriber_post_type_args', array (
				'label'				 => esc_html__( 'Unsubscriber', 'birthday-coupons-for-woocommerce' ),
				'public'			 => false,
				'hierarchical'		 => false,
				'supports'			 => false,
				'capability_type'	 => 'post',
				'rewrite'			 => false,
					)
					) ;
		}

	}

	BCN_Register_Post_Type::init() ;
}

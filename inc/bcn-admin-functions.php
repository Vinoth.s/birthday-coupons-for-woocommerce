<?php

/**
 * Common functions.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! function_exists( 'bcn_page_screen_ids' ) ) {

	/**
	 * Get page screen IDs.
	 *
	 * @return array
	 */
	function bcn_page_screen_ids() {

				$screen_id = sanitize_title( esc_html__( 'Birthday Coupons' , 'birthday-coupons-for-woocommerce' ) ) ;
				
				/**
			 * Page screen ids.
			 * 
			 * @since 1.0
			 */
		return apply_filters(
				'bcn_page_screen_ids' , array(
			'bcn_coupon' ,
			'bcn_birthday',
						'bcn_rules',
			$screen_id . '_page_bcn_settings'
				)
				) ;
	}

}

if ( ! function_exists( 'bcn_get_wc_categories' ) ) {

	/**
	 * Get the WC Categories.
	 *
	 * @return array
	 */
	function bcn_get_wc_categories() {
		$categories    = array() ;
		$wc_categories = get_terms( 'product_cat' ) ;

		if ( ! bcn_check_is_array( $wc_categories ) ) {
			return $categories ;
		}

		foreach ( $wc_categories as $category ) {
			$categories[ $category->term_id ] = $category->name ;
		}

		return $categories ;
	}

}

if ( ! function_exists( 'bcn_get_wp_user_roles' ) ) {

	/**
	 * Get the WordPress User Roles.
	 *
	 * @return array
	 */
	function bcn_get_wp_user_roles() {
		global $wp_roles ;
		$user_roles = array() ;

		if ( ! isset( $wp_roles->roles ) || ! bcn_check_is_array( $wp_roles->roles ) ) {
			return $user_roles ;
		}

		foreach ( $wp_roles->roles as $slug => $role ) {
			$user_roles[ $slug ] = $role[ 'name' ] ;
		}

		return $user_roles ;
	}

}

if ( ! function_exists( 'bcn_get_allowed_setting_tabs' ) ) {

	/**
	 * Get setting tabs.
	 *
	 * @return array
	 */
	function bcn_get_allowed_setting_tabs() {
		/**
					 * Settings tabs array.
					 * 
					 * @since 1.0
					 */
		return apply_filters( 'bcn_settings_tabs_array' , array() ) ;
	}

}

if ( ! function_exists( 'bcn_get_products_link' ) ) {

	/**
	 * Get the products link.
	 *
	 * @return string
	 */
	function bcn_get_products_link( $product_ids ) {

		$products_link = '' ;

		if ( ! bcn_check_is_array( $product_ids ) ) {
			return $products_link ;
		}

		foreach ( $product_ids as $product_id ) {
			$product = wc_get_product( $product_id ) ;

			//Return if the product does not exist.
			if ( ! $product ) {
				continue ;
			}

			$products_link .= bcn_get_edit_post_link( $product_id , $product->get_name() ) . ' , ' ;
		}

		return $products_link ;
	}

}

if ( ! function_exists( 'bcn_wc_help_tip' ) ) {

	/**
	 *  Display tool help based on WC help tip
	 *
	 *  @return string
	 */
	function bcn_wc_help_tip( $tip, $allow_html = false, $echo = true ) {

		$formatted_tip = wc_help_tip( $tip , $allow_html ) ;

		if ( $echo ) {
			echo wp_kses_post( $formatted_tip ) ;
		}

		return $formatted_tip ;
	}

}

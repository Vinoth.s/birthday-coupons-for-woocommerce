<?php

/*
 * Post Function.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! function_exists( 'bcn_create_new_birthday_coupon' ) ) {

	/**
	 * Create New Birthday Coupon.
	 *
	 * @return Object
	 */
	function bcn_create_new_birthday_coupon( $meta_args, $post_args = array() ) {

		$object = new BCN_Birthday() ;

		return $object->create( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_get_birthday_coupon' ) ) {

	/**
	 * Get Birthday Coupons.
	 *
	 * @return Object
	 */
	function bcn_get_birthday_coupon( $id ) {

		return new BCN_Birthday( $id ) ;
	}

}

if ( ! function_exists( 'bcn_get_birthday_coupon_ids' ) ) {

	/**
	 * Get Birthday Coupon Ids.
	 *
	 * @return Array
	 */
	function bcn_get_birthday_coupon_ids( $args = array() ) {
		$default_args = array(
			'numberposts' => -1 ,
			'post_type'   => BCN_Register_Post_Type::BCN_BIRTHDAY_POSTTYPE ,
			'post_status' => 'publish' ,
			'order'       => 'ASC' ,
			'fields'      => 'ids' ,
				) ;

		$parsed_data = wp_parse_args( $args , $default_args ) ;

		return get_posts( $parsed_data ) ;
	}

}

if ( ! function_exists( 'bcn_update_birthday_coupon' ) ) {

	/**
	 * Update Birthday Coupon.
	 *
	 * @return Object
	 */
	function bcn_update_birthday_coupon( $id, $meta_args, $post_args = array() ) {

		$object = new BCN_Birthday( $id ) ;

		return $object->update( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_delete_birthday_coupon' ) ) {

	/**
	 * Delete Birthday Coupon.
	 *
	 * @return bool
	 */
	function bcn_delete_birthday_coupon( $id, $force = true ) {

		wp_delete_post( $id , $force ) ;

		return true ;
	}

}

if ( ! function_exists( 'bcn_get_rule' ) ) {

	/**
	 * Get Rule.
	 *
	 * @return Object
	 */
	function bcn_get_rule( $id ) {

		return new BCN_Rules( $id ) ;
	}

}

if ( ! function_exists( 'bcn_get_rule_ids' ) ) {

	/**
	 * Get Rule Ids.
	 *
	 * @return Array
	 */
	function bcn_get_rule_ids( $args = array() ) {
		$default_args = array(
			'numberposts' => -1 ,
			'post_type'   => BCN_Register_Post_Type::BIRTHDAY_RULES_POSTTYPE ,
			'post_status' => 'bcn_active' ,
			'order'       => 'DESC' ,
			'fields'      => 'ids' ,
				) ;

		$parsed_data = wp_parse_args( $args , $default_args ) ;

		return get_posts( $parsed_data ) ;
	}

}

if ( ! function_exists( 'bcn_update_rule' ) ) {

	/**
	 * Update Rule.
	 *
	 * @return Object
	 */
	function bcn_update_rule( $id, $meta_args, $post_args = array() ) {

		$object = new BCN_Rules( $id ) ;

		return $object->update( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_delete_rule' ) ) {

	/**
	 * Delete Rule.
	 *
	 * @return bool
	 */
	function bcn_delete_rule( $id, $force = true ) {

		wp_delete_post( $id , $force ) ;

		return true ;
	}

}

if ( ! function_exists( 'bcn_create_new_coupon' ) ) {

	/**
	 * Create New Coupon.
	 *
	 * @return Object
	 */
	function bcn_create_new_coupon( $meta_args, $post_args = array() ) {

		$object = new BCN_Coupons() ;

		return $object->create( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_get_coupon' ) ) {

	/**
	 * Get Coupon.
	 *
	 * @return Object
	 */
	function bcn_get_coupon( $id ) {

		return new BCN_Coupons( $id ) ;
	}

}

if ( ! function_exists( 'bcn_get_coupon_ids' ) ) {

	/**
	 * Get Coupon Ids.
	 *
	 * @return Array
	 */
	function bcn_get_coupon_ids( $args = array() ) {
		$default_args = array(
			'numberposts' => -1 ,
			'post_type'   => BCN_Register_Post_Type::COUPONS_POSTTYPE ,
			'post_status' => array( 'bcn_unused' , 'bcn_used', 'bcn_expired' ) ,
			'order'       => 'ASC' ,
			'fields'      => 'ids' ,
				) ;

		$parsed_data = wp_parse_args( $args , $default_args ) ;

		return get_posts( $parsed_data ) ;
	}

}

if ( ! function_exists( 'bcn_update_coupon' ) ) {

	/**
	 * Update Birthday Coupon.
	 *
	 * @return Object
	 */
	function bcn_update_coupon( $id, $meta_args, $post_args = array() ) {

		$object = new BCN_Coupons( $id ) ;

		return $object->update( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_delete_coupon' ) ) {

	/**
	 * Delete Coupon.
	 *
	 * @return bool
	 */
	function bcn_delete_coupon( $id, $force = true ) {

		wp_delete_post( $id , $force ) ;

		return true ;
	}

}

if ( ! function_exists( 'bcn_create_new_unsubscriber' ) ) {

	/**
	 * Create New Unsubscriber.
	 *
	 * @return Object
	 */
	function bcn_create_new_unsubscriber( $meta_args, $post_args = array() ) {

		$object = new BCN_Unsubscriber() ;

		return $object->create( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_get_unsubscriber' ) ) {

	/**
	 * Get Unsubscriber.
	 *
	 * @return Object
	 */
	function bcn_get_unsubscriber( $id ) {

		return new BCN_Unsubscriber( $id ) ;
	}

}

if ( ! function_exists( 'bcn_get_unsubscriber_ids' ) ) {

	/**
	 * Get Unsubscriber Ids.
	 *
	 * @return Array
	 */
	function bcn_get_unsubscriber_ids( $args = array() ) {
		$default_args = array(
			'numberposts' => -1 ,
			'post_type'   => BCN_Register_Post_Type::BCN_UNSUBSCRIBER_POSTTYPE, 
			'post_status' => array( 'publish' ) ,
			'order'       => 'ASC' ,
			'fields'      => 'ids' ,
				) ;

		$parsed_data = wp_parse_args( $args , $default_args ) ;

		return get_posts( $parsed_data ) ;
	}

}

if ( ! function_exists( 'bcn_update_unsubscriber' ) ) {

	/**
	 * Update Unsubscriber.
	 *
	 * @return Object
	 */
	function bcn_update_unsubscriber( $id, $meta_args, $post_args = array() ) {

		$object = new BCN_Unsubscriber( $id ) ;

		return $object->update( $meta_args , $post_args ) ;
	}

}

if ( ! function_exists( 'bcn_delete_unsubscriber' ) ) {

	/**
	 * Delete Unsubscriber.
	 *
	 * @return bool
	 */
	function bcn_delete_unsubscriber( $id, $force = true ) {

		wp_delete_post( $id , $force ) ;

		return true ;
	}

}

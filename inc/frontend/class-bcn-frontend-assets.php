<?php

/**
 * Enqueue Front End Enqueue Files
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Frontend_Assets' ) ) {

	/**
	 * BCN_Frontend_Assets Class.
	 */
	class BCN_Frontend_Assets {

		/**
		 * BCN_Frontend_Assets Class Initialization.
		 */
		public static function init() {

			add_action( 'wp_enqueue_scripts' , array( __CLASS__ , 'external_js_files' ) , 99 ) ;
			add_action( 'wp_enqueue_scripts' , array( __CLASS__ , 'external_css_files' ) ) ;
		}

		/**
		 * Enqueue external css files.
		 */
		public static function external_css_files() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;

			if ( is_account_page() ) {
				// Lightcase CSS.
				wp_enqueue_style( 'bcn-lightcase' , BCN_PLUGIN_URL . '/assets/css/lightcase' . $suffix . '.css' , array() , BCN_VERSION ) ;
			}
			wp_enqueue_style( 'jquery-ui-style' , WC()->plugin_url() . '/assets/css/jquery-ui/jquery-ui.min.css' , array() , WC_VERSION ) ;
			wp_enqueue_style( 'bcn-frontend' , BCN_PLUGIN_URL . '/assets/css/frontend.css' , array() , BCN_VERSION ) ;

			wp_register_style( 'bcn-inline-style' , false , array() , BCN_VERSION ) ; // phpcs:ignore
			wp_enqueue_style( 'bcn-inline-style' ) ;

			self::add_inline_style() ;
			/**
			 * Frontend after enqueue css.
			 * 
			 * @since 1.0
			 */
			do_action( 'bcn_frontend_after_enqueue_css' , $suffix ) ;
		}

		/**
		 * Add Inline Style.
		 */
		public static function add_inline_style() {
			$contents = get_option( 'bcn_advanced_custom_css' , '' ) ;

			wp_add_inline_style( 'bcn-inline-style' , $contents ) ;
		}

		/**
		 * Enqueue Front end required JS files.
		 */
		public static function external_js_files() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;

			$enqueue_array = array(
				'bcn-lightcase' => array(
					'callable' => array( 'BCN_Frontend_Assets' , 'enqueue_lightcase' ) ,
					'restrict' => is_account_page() ,
				) ,
				'bcn-frontend-enhanced'   => array(
					'callable' => array( 'BCN_Frontend_Assets' , 'bcn_enhanced' ) ,
					'restrict' => true ,
				) ,
					) ;
			/**
			 * Frontend enqueue scripts.
			 * 
			 * @since 1.0
			 */
			$enqueue_array = apply_filters( 'bcn_frontend_enqueue_scripts' , $enqueue_array ) ;

			if ( ! bcn_check_is_array( $enqueue_array ) ) {
				return ;
			}

			foreach ( $enqueue_array as $key => $enqueue ) {
				if ( ! bcn_check_is_array( $enqueue ) ) {
					continue ;
				}

				if ( $enqueue[ 'restrict' ] ) {
					call_user_func_array( $enqueue[ 'callable' ] , array() ) ;
				}
			}
			/**
			 * Frontend after enqueue js.
			 * 
			 * @since 1.0
			 */
			do_action( 'bcn_frontend_after_enqueue_js' , $suffix ) ;
		}

		/**
		 * Enqueue Lightcase.
		 */
		public static function enqueue_lightcase() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min' ;
			// Lightcase.
			wp_register_script( 'lightcase' , BCN_PLUGIN_URL . '/assets/js/lightcase' . $suffix . '.js' , array( 'jquery' ) , BCN_VERSION ) ;
			// Enhanced lightcase.
			wp_enqueue_script( 'bcn-lightcase' , BCN_PLUGIN_URL . '/assets/js/bcn-lightcase-enhanced.js' , array( 'jquery' , 'jquery-blockui' , 'lightcase' ) , BCN_VERSION ) ;
			wp_localize_script(
					'bcn-lightcase' , 'bcn_lightcase_param' , array(
				'bcn_info_nonce' => wp_create_nonce( 'bcn-info-nonce' ) ,
				'ajaxurl'        => BCN_ADMIN_AJAX_URL
					)
			) ;
		}
		
		/**
		 * Enqueue bcn_enhanced scripts.
		 */
		public static function bcn_enhanced() {
			wp_enqueue_script( 'bcn-frontend-enhanced' , BCN_PLUGIN_URL . '/assets/js/bcn-enhanced.js' , array( 'jquery' , 'select2' , 'jquery-ui-datepicker' ) , BCN_VERSION ) ;
			wp_localize_script(
					'bcn-frontend-enhanced' , 'bcn_enhanced_select_params' , array(
				'search_nonce' => wp_create_nonce( 'bcn-search-nonce' ) ,
				'ajaxurl'      => BCN_ADMIN_AJAX_URL
					)
			) ;
		}

	}

	BCN_Frontend_Assets::init() ;
}

<?php

/**
 * Birthday Coupon Handler
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Myaccount_Handler' ) ) {

	/**
	 * BCN_Myaccount_Handler Class.
	 */
	class BCN_Myaccount_Handler {

		/**
		 * Wallet endpoint.
		 */
		public static $coupon_endpoint = 'bcn-mycoupons' ;

		/**
		 * Class Initialization.
		 */
		public static function init() {
			//Add custom rewrite endpoint
			add_action( 'init' , array( __CLASS__ , 'bcn_rewrite_endpoint' ) ) ;
			//Flush rewrite rules
			add_action( 'wp_loaded' , array( __CLASS__ , 'flush_rewrite_rules' ) ) ;
			//Add custom query vars
			add_filter( 'query_vars' , array( __CLASS__ , 'bcn_query_vars' ) , 0 ) ;
			//Add custom Myaccount Menu
			add_filter( 'woocommerce_account_menu_items' , array( __CLASS__ , 'bcn_myaccount_menu' ) ) ;
			//Customize the myaccount menu title
			add_filter( 'the_title' , array( __CLASS__ , 'customize_menu_title' ) ) ;
			//Display the My Coupon menu content
			add_action( 'woocommerce_account_' . self::$coupon_endpoint . '_endpoint' , array( __CLASS__ , 'coupon_menu_content' ) , 11 ) ;
		}

		/**
		 * Custom rewrite endpoint
		 */
		public static function bcn_rewrite_endpoint() {
			add_rewrite_endpoint( self::$coupon_endpoint , EP_ROOT | EP_PAGES ) ;
		}

		/**
		 * Add custom Query variable
		 */
		public static function bcn_query_vars( $vars ) {
			$vars[] = self::$coupon_endpoint ;

			return $vars ;
		}

		/**
		 * Flush Rewrite Rules 
		 */
		public static function flush_rewrite_rules() {
			flush_rewrite_rules() ;
		}

		/**
		 * Custom My account Menus
		 */
		public static function bcn_myaccount_menu( $menus ) {
			if ( ! is_user_logged_in() ) {
				return $menus ;
			}

			$coupon_menu = array( self::$coupon_endpoint => 'My Coupons' ) ;
			$menus       = bcn_customize_array_position( $menus , 'dashboard' , $coupon_menu ) ;

			return $menus ;
		}

		/**
		 * Customize the My account menu title
		 */
		public static function customize_menu_title( $title ) {
			global $wp_query ;

			if ( is_main_query() && in_the_loop() && is_account_page() ) {
				if ( isset( $wp_query->query_vars[ self::$coupon_endpoint ] ) ) {
					$title = 'My Coupons' ;
				}

				remove_filter( 'the_title' , array( __CLASS__ , 'customize_menu_title' ) ) ;
			}

			return $title ;
		}

		/**
		 * Display the Coupon menu content
		 */
		public static function coupon_menu_content() {

			$args = array(
				'post_parent' => get_current_user_id() ,
					) ;

			$my_coupon_ids = bcn_get_coupon_ids( $args ) ;

			$post_per_page = 10 ;
			$current_page  = isset( $_REQUEST[ 'page_no' ] ) ? wc_clean( wp_unslash( absint( $_REQUEST[ 'page_no' ] ) ) ) : '1' ;
			$offset        = ( $post_per_page * $current_page ) - $post_per_page ;
			$page_count    = ceil( count( $my_coupon_ids ) / $post_per_page ) ;

			$table_args = array(
				'my_coupon_ids' => array_slice( $my_coupon_ids , $offset , $post_per_page ) ,
				'pagination'    => array(
					'page_count'      => $page_count ,
					'current_page'    => $current_page ,
					'prev_page_count' => ( ( $current_page - 1 ) == 0 ) ? ( $current_page ) : ( $current_page - 1 ) ,
					'next_page_count' => ( ( $current_page + 1 ) <= ( $page_count ) ) ? ( $current_page + 1 ) : ( $current_page ) )
					) ;

			echo wp_kses_post( bcn_get_template_html( 'my-coupons.php' , $table_args ) ) ;
		}

	}

	BCN_Myaccount_Handler::init() ;
}

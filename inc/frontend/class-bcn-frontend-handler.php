<?php

/**
 * Frontend Handler
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Frontend_Handler' ) ) {

	/**
	 * BCN_Frontend_Handler Class.
	 */
	class BCN_Frontend_Handler {

		/**
		 * Class Initialization.
		 */
		public static function init() {
			//Display Birthday Field in Register Form
			add_action( 'woocommerce_register_form' , array( __CLASS__ , 'birthday_field' ) ) ;
			//Display Birthday Field in Edit Account Form
			add_action( 'woocommerce_edit_account_form' , array( __CLASS__ , 'birthday_field' ) ) ;
			//Display Birthday Field in Add New User Form
			add_action( 'user_new_form' , array( __CLASS__ , 'birthday_field_in_user_form' ) ) ;
			//Display Birthday Field in Edit User Form
			add_action( 'show_user_profile' , array( __CLASS__ , 'birthday_field_in_user_form' ) ) ;
			//Display Birthday Field in Edit User Form
			add_action( 'edit_user_profile' , array( __CLASS__ , 'birthday_field_in_user_form' ) ) ;
			//Display Birthday Field in Checkout
			add_action( 'woocommerce_after_order_notes' , array( __CLASS__ , 'birthday_field_in_checkout' ) ) ;
		}

		/**
		 * Display Birthday Field in Register Field
		 */
		public static function birthday_field() {
			$birthday_date     = get_user_meta( get_current_user_id(), 'bcn_birthday_date', true );
			$unsubscribe_email = get_user_meta( get_current_user_id(), 'bcn_unsubscribe_email', true );

			$args = array(
				'id'                => 'bcn_birthday_date' ,
				'value'             => $birthday_date ,
				'custom_attributes' => empty( $birthday_date ) ? array() : array( 'readonly' => 'readonly' ) ,
					) ;

			$class_name = 'woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide' ;

			include BCN_ABSPATH . 'templates/birthday-field.php' ;
		}

		/**
		 * Display Birthday Field in User Form
		 */
		public static function birthday_field_in_user_form() {
			$birthday_date = get_user_meta( get_current_user_id() , 'bcn_birthday_date' , true ) ;

			include BCN_ABSPATH . 'inc/admin/menu/views/birthday/birthday-field.php' ;
		}

		/**
		 * Display Birthday Field in Checkout
		 */
		public static function birthday_field_in_checkout() {
					
			if ( ! check_is_logged_in_user() ) {
				return ;
			}

			$args = array(
				'id' => 'bcn_birthday_date' ,
					) ;
						
						$unsubscribe_email = get_user_meta( get_current_user_id() , 'bcn_unsubscribe_email' , true ) ;

			$class_name = 'form-row' ;

			if ( is_user_logged_in() ) {
				$birthday_date = get_user_meta( get_current_user_id() , 'bcn_birthday_date' , true ) ;
				if ( empty( $birthday_date ) ) {
					include BCN_ABSPATH . 'templates/birthday-field.php' ;
				}
			} else {
				include BCN_ABSPATH . 'templates/birthday-field.php' ;
			}
		}

	}

	BCN_Frontend_Handler::init() ;
}

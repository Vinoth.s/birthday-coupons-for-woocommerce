<?php

/**
 * Birthday Coupon for WooCommerce Main Class.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Coupon' ) ) {

	/**
	 * Main BCN_Coupon Class.
	 * */
	final class BCN_Coupon {

		/**
		 * Version.
		 * */
		private $version = '1.8' ;

		/**
		 * The single instance of the class.
		 * */
		protected static $_instance = null ;
		
		
		/**
		 * WC minimum version.
		 *
		 * @var string
		 */
		public static $wc_minimum_version = '3.5' ;

		/**
		 * WP minimum version.
		 *
		 * @var string
		 */
		public static $wp_minimum_version = '4.6' ;

		/**
		 * Load FP_Order_Bump Class in Single Instance.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self() ;
			}

			return self::$_instance ;
		}

		/* Cloning has been forbidden */

		public function __clone() {
			_doing_it_wrong( __FUNCTION__, 'You are not allowed to perform this action!!!', '1.0' ) ;
		}

		/**
		 * Unserialize the class data has been forbidden.
		 * */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, 'You are not allowed to perform this action!!!', '1.0' ) ;
		}

		/**
		 * Constructor.
		 * */
		public function __construct() {

			/* Include once will help to avoid fatal error by load the files when you call init hook */
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' ) ;

			$this->header_already_sent_problem() ;
			$this->define_constants() ;
			$this->include_files() ;
			$this->init_hooks() ;
		}

		/**
		 * Function to prevent header error that says you have already sent the header.
		 */
		private function header_already_sent_problem() {
			ob_start() ;
		}

		/**
		 * Load plugin the translate files.
		 * */
		private function load_plugin_textdomain() {
			if ( function_exists( 'determine_locale' ) ) {
				$locale = determine_locale() ;
			} else {
				// @todo Remove when start supporting WP 5.0 or later.
				$locale = is_admin() ? get_user_locale() : get_locale() ;
			}
			/**
			 * Plugin locale.
			 * 
			 * @since 1.0
			 */
			$locale = apply_filters( 'plugin_locale', $locale, 'birthday-coupons-for-woocommerce' ) ;

			unload_textdomain( 'birthday-coupons-for-woocommerce' ) ;
			load_textdomain( 'birthday-coupons-for-woocommerce', WP_LANG_DIR . '/birthday-coupons-for-woocommerce/birthday-coupons-for-woocommerce-' . $locale . '.mo' ) ;
			load_plugin_textdomain( 'birthday-coupons-for-woocommerce', false, dirname( plugin_basename( BCN_PLUGIN_FILE ) ) . '/languages' ) ;
		}

		/**
		 * Prepare the constants value array.
		 * */
		private function define_constants() {

			$constant_array	 = array (
				'BCN_VERSION'		 => $this->version,
				'BCN_FOLDER_NAME'	 => 'birthday-coupons-for-woocommerce',
				'BCN_ABSPATH'		 => dirname( BCN_PLUGIN_FILE ) . '/',
				'BCN_ADMIN_URL'		 => admin_url( 'admin.php' ),
				'BCN_ADMIN_AJAX_URL' => admin_url( 'admin-ajax.php' ),
				'BCN_PLUGIN_SLUG'	 => plugin_basename( BCN_PLUGIN_FILE ),
				'BCN_PLUGIN_PATH'	 => untrailingslashit( plugin_dir_path( BCN_PLUGIN_FILE ) ),
				'BCN_PLUGIN_URL'	 => untrailingslashit( plugins_url( '/', BCN_PLUGIN_FILE ) ),
					) ;
			/**
			 * Define Constants.
			 * 
			 * @since 1.0
			 */
			$constant_array	 = apply_filters( 'bcn_define_constants', $constant_array ) ;

			if ( is_array( $constant_array ) && ! empty( $constant_array ) ) {
				foreach ( $constant_array as $name => $value ) {
					$this->define_constant( $name, $value ) ;
				}
			}
		}

		/**
		 * Define the Constants value.
		 * */
		private function define_constant( $name, $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value ) ;
			}
		}

		/**
		 * Include required files.
		 * */
		private function include_files() {

			// Function.
			include_once( BCN_ABSPATH . 'inc/bcn-common-functions.php' ) ;

			// Abstract classes.
			include_once( BCN_ABSPATH . 'inc/abstracts/abstract-bcn-post.php' ) ;

			include_once( BCN_ABSPATH . 'inc/class-bcn-register-post-types.php' ) ;
			include_once( BCN_ABSPATH . 'inc/class-bcn-register-post-status.php' ) ;

			include_once( BCN_ABSPATH . 'inc/class-bcn-install.php' ) ;
			include_once( BCN_ABSPATH . 'inc/privacy/class-bcn-privacy.php' ) ;
			include_once( BCN_ABSPATH . 'inc/class-bcn-date-time.php' ) ;
			include_once( BCN_ABSPATH . 'inc/class-bcn-query.php' ) ;
			include_once( BCN_ABSPATH . 'inc/class-bcn-cron-handler.php' ) ;

			// Entity.
			include_once( BCN_ABSPATH . 'inc/entity/class-bcn-birthday.php' ) ;
			include_once( BCN_ABSPATH . 'inc/entity/class-bcn-rules.php' ) ;
			include_once( BCN_ABSPATH . 'inc/entity/class-bcn-coupons.php' ) ;
			include_once( BCN_ABSPATH . 'inc/entity/class-bcn-unsubscriber.php' ) ;

			//Notification
			include_once( BCN_ABSPATH . 'inc/notifications/class-bcn-notification-instances.php' ) ;

			include_once( BCN_ABSPATH . 'inc/class-bcn-birthday-handler.php' ) ;

			if ( is_admin() ) {
				$this->include_admin_files() ;
			}

			if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
				$this->include_frontend_files() ;
			}
		}

		/**
		 * Include admin files.
		 * */
		private function include_admin_files() {
			// Function.
			include_once( BCN_ABSPATH . 'inc/bcn-admin-functions.php' ) ;

			include_once( BCN_ABSPATH . 'inc/admin/class-bcn-admin-assets.php' ) ;
			include_once( BCN_ABSPATH . 'inc/admin/class-bcn-admin-ajax.php' ) ;
			include_once( BCN_ABSPATH . 'inc/admin/menu/class-bcn-menu-management.php' ) ;
			include_once( BCN_ABSPATH . 'inc/admin/class-bcn-admin-post-type-handler.php' ) ;
		}

		/**
		 * Include frontend files.
		 * */
		private function include_frontend_files() {
			// Function.
			include_once( BCN_ABSPATH . 'inc/frontend/class-bcn-frontend-assets.php' ) ;
			include_once( BCN_ABSPATH . 'inc/frontend/class-bcn-frontend-handler.php' ) ;
			include_once( BCN_ABSPATH . 'inc/frontend/class-bcn-myaccount-handler.php' ) ;
		}

		/**
		 * Define the hooks.
		 * */
		private function init_hooks() {

			// Init the plugin.
			add_action( 'init', array ( $this, 'init' ) ) ;

			BCN_Notification_Instances::get_notifications() ;

			// Register the plugin.
			register_activation_hook( BCN_PLUGIN_FILE, array ( 'BCN_Install', 'install' ) ) ;
		}

		/**
		 * Init.
		 * 
		 * @return void
		 * */
		public function init() {

			$this->load_plugin_textdomain() ;
		}

		/**
		 * Templates.
		 * */
		public function templates() {
			return BCN_PLUGIN_PATH . '/templates/' ;
		}

	}

}

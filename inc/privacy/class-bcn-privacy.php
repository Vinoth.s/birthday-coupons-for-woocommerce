<?php
/*
 * GDPR Compliance
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly
}

if ( ! class_exists( 'BCN_Privacy' ) ) :

	/**
	 * BCN_Privacy class
	 */
	class BCN_Privacy {

		/**
		 * BCN_Privacy constructor.
		 */
		public function __construct() {
			$this->init_hooks() ;
		}

		/**
		 * Register plugin
		 */
		public function init_hooks() {
			// This hook registers Booking System privacy content
			add_action( 'admin_init' , array( __CLASS__ , 'register_privacy_content' ) , 20 ) ;
		}

		/**
		 * Register Privacy Content
		 */
		public static function register_privacy_content() {
			if ( ! function_exists( 'wp_add_privacy_policy_content' ) ) {
				return ;
			}

			$content = self::get_privacy_message() ;
			if ( $content ) {
				wp_add_privacy_policy_content( esc_html__( 'Birthday Coupons for WooCommerce' , 'birthday-coupons-for-woocommerce' ) , $content ) ;
			}
		}

		/**
		 * Prepare Privacy Content
		 */
		public static function get_privacy_message() {

			return self::get_privacy_message_html() ;
		}

		/**
		 * Get Privacy Content
		 */
		public static function get_privacy_message_html() {
			ob_start() ;
			?>
			<p><?php esc_html_e( 'This includes the basics of what personal data your store may be collecting, storing and sharing. Depending on what settings are enabled and which additional plugins are used, the specific information shared by your store will vary.' , 'birthday-coupons-for-woocommerce' ) ; ?></p>
			<h2><?php esc_html_e( 'WHAT DOES THE PLUGIN DO?' , 'birthday-coupons-for-woocommerce' ) ; ?></h2>
			<p><?php esc_html_e( 'Birthday Coupons for WooCommerce helps you to send coupons to users for their birthday which can be used to receive discounts in purchase.' , 'birthday-coupons-for-woocommerce' ) ; ?> </p>
			<h2><?php esc_html_e( 'WHAT WE COLLECT AND STORE?' , 'birthday-coupons-for-woocommerce' ) ; ?></h2>
			<h4><?php esc_html_e( 'Username & Email ID' , 'birthday-coupons-for-woocommerce' ) ; ?></h4>
			<p><?php esc_html_e( 'To identify the user for whom the coupon is displayed.' , 'birthday-coupons-for-woocommerce' ) ; ?></p>
			<?php
			$contents = ob_get_contents() ;
			ob_end_clean() ;

			return $contents ;
		}

	}

	new BCN_Privacy() ;

endif;

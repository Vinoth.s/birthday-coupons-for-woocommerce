<?php

/**
 * Register Custom Post Status.
 *
 * @package
 * */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

if ( ! class_exists( 'BCN_Register_Post_Status' ) ) {

	/**
	 * BCN_Register_Post_Status Class.
	 * */
	class BCN_Register_Post_Status {

		/**
		 * Class initialization.
		 */
		public static function init() {
			add_action( 'init' , array( __CLASS__ , 'register_custom_post_status' ) ) ;
		}

		/**
		 * Register Custom Post Status.
		 * */
		public static function register_custom_post_status() {
			$custom_post_statuses = array(
				'bcn_active'         => array( 'BCN_Register_Post_Status' , 'active_post_status_args' ) ,
				'bcn_inactive'       => array( 'BCN_Register_Post_Status' , 'inactive_post_status_args' ) ,
				'bcn_unused'         => array( 'BCN_Register_Post_Status' , 'unused_post_status_args' ) ,
				'bcn_used'           => array( 'BCN_Register_Post_Status' , 'used_post_status_args' ) ,
				'bcn_expired'        => array( 'BCN_Register_Post_Status' , 'expired_post_status_args' ) ,
				'bcn_partially_used' => array( 'BCN_Register_Post_Status' , 'partially_used_post_status_args' ) ,
					) ;
			/**
						 * Custom Post status.
						 * 
						 * @since 1.0
						 */
			$custom_post_statuses = apply_filters( 'bcn_add_custom_post_status' , $custom_post_statuses ) ;

			// Return if no post status to register.
			if ( ! bcn_check_is_array( $custom_post_statuses ) ) {
				return ;
			}

			foreach ( $custom_post_statuses as $post_status => $args_function ) {

				$args = array() ;
				if ( $args_function ) {
					$args = call_user_func_array( $args_function , array() ) ;
				}

				// Register post status.
				register_post_status( $post_status , $args ) ;
			}
		}

		/**
		 * Active Custom Post Status arguments.
		 * */
		public static function active_post_status_args() {
			/**
			 * Active post status args.
			 * 
			 * @since 1.0
			 */
			$args = apply_filters(
					'bcn_active_post_status_args' , array(
				'label'                     => esc_html_x( 'Active' , 'coupons-pro-for-woocommerce' ) ,
				'public'                    => true ,
				'exclude_from_search'       => false ,
				'show_in_admin_all_list'    => true ,
				'show_in_admin_status_list' => true ,
				/* translators: %s: count */
				'label_count'               => _n_noop( 'Active <span class="count">(%s)</span>' , 'Active <span class="count">(%s)</span>' , 'coupons-pro-for-woocommerce' ) ,
					)
					) ;

			return $args ;
		}

		/**
		 * Inactive Custom Post Status arguments.
		 * */
		public static function inactive_post_status_args() {
			/**
			 * Inactive post status args.
			 * 
			 * @since 1.0
			 */
			$args = apply_filters(
					'bcn_inactive_post_status_args' , array(
				'label'                     => esc_html_x( 'Deactive' , 'coupons-pro-for-woocommerce' ) ,
				'public'                    => true ,
				'exclude_from_search'       => false ,
				'show_in_admin_all_list'    => true ,
				'show_in_admin_status_list' => true ,
				/* translators: %s: count */
				'label_count'               => _n_noop( 'Deactive <span class="count">(%s)</span>' , 'Deactive <span class="count">(%s)</span>' , 'coupons-pro-for-woocommerce' ) ,
					)
					) ;

			return $args ;
		}

		/**
		 * Unused Custom Post Status arguments.
		 * */
		public static function unused_post_status_args() {
			/**
			 * Unused post status args.
			 * 
			 * @since 1.0
			 */
			$args = apply_filters(
					'bcn_unused_post_status_args' , array(
				'label'                     => esc_html_x( 'Not Used' , 'coupons-pro-for-woocommerce' ) ,
				'public'                    => true ,
				'exclude_from_search'       => false ,
				'show_in_admin_all_list'    => true ,
				'show_in_admin_status_list' => true ,
				/* translators: %s: count */
				'label_count'               => _n_noop( 'Not Used <span class="count">(%s)</span>' , 'Not Used <span class="count">(%s)</span>' , 'coupons-pro-for-woocommerce' ) ,
					)
					) ;

			return $args ;
		}

		/**
		 * Used Custom Post Status arguments.
		 * */
		public static function used_post_status_args() {
			/**
			 * Used post status args.
			 * 
			 * @since 1.0
			 */
			$args = apply_filters(
					'bcn_used_post_status_args' , array(
				'label'                     => esc_html_x( 'Used' , 'coupons-pro-for-woocommerce' ) ,
				'public'                    => true ,
				'exclude_from_search'       => false ,
				'show_in_admin_all_list'    => true ,
				'show_in_admin_status_list' => true ,
				/* translators: %s: count */
				'label_count'               => _n_noop( 'Used <span class="count">(%s)</span>' , 'Used <span class="count">(%s)</span>' , 'coupons-pro-for-woocommerce' ) ,
					)
					) ;

			return $args ;
		}

		/**
		 * Expired Custom Post Status arguments.
		 * */
		public static function expired_post_status_args() {
			/**
			 * Expired post status args.
			 * 
			 * @since 1.0
			 */
			$args = apply_filters(
					'bcn_expired_post_status_args' , array(
				'label'                     => esc_html_x( 'Expired' , 'coupons-pro-for-woocommerce' ) ,
				'public'                    => true ,
				'exclude_from_search'       => false ,
				'show_in_admin_all_list'    => true ,
				'show_in_admin_status_list' => true ,
				/* translators: %s: count */
				'label_count'               => _n_noop( 'Expired <span class="count">(%s)</span>' , 'Expired <span class="count">(%s)</span>' , 'coupons-pro-for-woocommerce' ) ,
					)
					) ;

			return $args ;
		}

		/**
		 * Partially Used Custom Post Status arguments.
		 * */
		public static function partially_used_post_status_args() {
			/**
			 * Partially used post status args.
			 * 
			 * @since 1.0
			 */
			$args = apply_filters(
					'bcn_partially_used_post_status_args' , array(
				'label'                     => esc_html_x( 'Partially Used' , 'coupons-pro-for-woocommerce' ) ,
				'public'                    => true ,
				'exclude_from_search'       => false ,
				'show_in_admin_all_list'    => true ,
				'show_in_admin_status_list' => true ,
				/* translators: %s: count */
				'label_count'               => _n_noop( 'Partially Used <span class="count">(%s)</span>' , 'Partially Used <span class="count">(%s)</span>' , 'coupons-pro-for-woocommerce' ) ,
					)
					) ;

			return $args ;
		}

	}

	BCN_Register_Post_Status::init() ;
}

<?php
/* Admin HTML Birthday Coupon Settings */

if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
?>
<table>
	<tr>
		<th><?php echo esc_html(get_option( 'bcn_localization_coupon_code_label' )) ; ?></th>
		<th><?php echo esc_html(get_option( 'bcn_localization_coupon_value_label' )) ; ?></th>
		<th><?php echo esc_html(get_option( 'bcn_localization_coupon_restriction_label' )) ; ?></th>
		<th><?php echo esc_html(get_option( 'bcn_localization_coupon_issued_label' )) ; ?></th>
		<th><?php echo esc_html(get_option( 'bcn_localization_coupon_expires_label' )) ; ?></th>
		<th><?php echo esc_html(get_option( 'bcn_localization_coupon_status_label' )) ; ?></th>
		<th><?php echo esc_html(get_option( 'bcn_localization_used_order_label' )) ; ?></th>
	</tr>
	<tbody>
		<?php
		if ( bcn_check_is_array( $my_coupon_ids ) ) :
			foreach ( $my_coupon_ids as $my_coupon_id ) :

				$coupon_obj = bcn_get_coupon( $my_coupon_id ) ;

				if ( ! is_object( $coupon_obj ) ) {
					continue ;
				}

				$rule_obj = $coupon_obj->get_rule() ;

				if ( ! is_object( $rule_obj ) ) {
					continue ;
				}
				?>
				<tr>
					<td>
						<?php echo esc_html( $coupon_obj->get_coupon_code() ) ; ?>
					</td>
					<td>
						<?php
						if ( 'fixed_cart' == $rule_obj->get_coupon_type() ) {
							$coupon_value = bcn_price( wp_kses_post( $coupon_obj->get_coupon_value() ) , false ) ;
						} else {
							$coupon_value = wp_kses_post( $coupon_obj->get_coupon_value() . ' %' ) ;
						}
						echo wp_kses_post( $coupon_value ) ;
						?>
					</td>
					<td>
						<button class="bcn-popup-more-info" data-postid="<?php echo esc_attr( $coupon_obj->get_coupon_id() ) ; ?>"><?php esc_html_e( 'More Info' , 'birthday-coupons-for-woocommerce') ; ?></button>
					</td>
					<td>
						<?php echo esc_html( $coupon_obj->get_issued_date() ) ; ?>
					</td>
					<td>
						<?php echo esc_html( $coupon_obj->get_expiry_date() ) ; ?>
					</td>
					<td>
						<?php echo wp_kses_post( bcn_display_status( $coupon_obj->get_status() ) ) ; ?>
					</td>
					<td>
						<?php echo bcn_check_is_array( $coupon_obj->get_used_orders() ) ? esc_html( implode( ',' , $coupon_obj->get_used_orders() ) ) : '-' ; ?>
					</td>
				</tr>
				<?php
			endforeach ;
		endif ;
		?>
	</tbody>
	<?php if ( $pagination[ 'page_count' ] > 1 ) : ?>
		<tfoot>
			<tr>
				<td colspan="6" class="footable-visible">
					<?php echo wp_kses_post( bcn_get_template_html( 'pagination.php' , $pagination ) ) ; ?>
				</td>
			</tr>
		</tfoot>
	<?php endif ; ?>
</table>
<div class="bcn-hide">
	<div id="bcn_more_info_modal"></div>
</div>
<?php

<?php
/**
 * This template displays the contents popup layout.
 *
 * This template can be overridden by copying it to yourtheme/birthday-coupons-for-woocommerce/popup-layout.php
 *
 * To maintain compatibility, Free Gifts for WooCommerce will update the template files and you have to copy the updated files to your theme
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}
?>

<h3>
	<?php esc_html_e( 'Coupon Details' , 'birthday-coupons-for-woocommerce' ) ; ?>
</h3>

<table class="bcn-popup-more-info-wrapper woocommerce">
	<?php
	if ( bcn_check_is_array( $coupon_obj ) ) {
		foreach ( $coupon_obj as $coupon_data ) {
			if ( $coupon_data[ 'disp' ] ) {
				?>
				<tr>
					<th>
						<?php echo wp_kses_post( $coupon_data[ 'label' ] ) ; ?>
					</th>
					<td>
						<?php echo wp_kses_post( $coupon_data[ 'value' ] ) ; ?>
					</td>
				</tr>
				<?php
			}
		}
	}
	?>
</table>
<?php


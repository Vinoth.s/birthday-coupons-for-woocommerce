<?php
/* Admin HTML Birthday Coupon Settings */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<p class="<?php echo esc_attr( $class_name ); ?> bcn-birthday-field">
	<label for="bcn_birthday_date_label"><?php echo wp_kses_post( get_option( 'bcn_localization_birthday_field_label' ) ); ?></label>
	<?php bcn_get_datepicker_html( $args ); ?>
	<?php if ( 'yes' === get_option( 'bcn_localization_enable_reason' ) ) { ?>
		<span><em><?php echo wp_kses_post( get_option( 'bcn_localization_birthday_field_reason_label' ) ); ?></em></span>
	<?php } ?>
</p>
<p class="<?php echo esc_attr( $class_name ); ?> bcn-unsubscribe-field">
	<?php if ( 'yes' === get_option( 'bcn_unsubscribedemails_unsubscribe_emails' ) ) { ?>
		<input type="checkbox" name="bcn_unsubscribe_email" <?php echo checked( $unsubscribe_email, 'yes', true ); ?>/>
		<label for="bcn_unsubcribe_email_label"><?php echo wp_kses_post( get_option( 'bcn_localization_unsubscribe_email_label' ) ); ?></label>
	<?php } ?>
</p>
<?php

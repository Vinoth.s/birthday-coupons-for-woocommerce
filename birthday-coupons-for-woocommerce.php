<?php

/**
 * Plugin Name: Birthday Coupons for WooCommerce
 * Description: Birthday Coupons for WooCommerce helps you to send coupons to users for their birthday which can be used to receive discounts in purchase.
 * Version: 1.8
 * Author: FantasticPlugins
 * Author URI: http://fantasticplugins.com
 * Text Domain: birthday-coupons-for-woocommerce
 * Domain Path: /languages
 * Woo: 8354506:d56f96b28777651dbafeea45051d7326
 * Tested up to: 6.1.1
 * WC tested up to: 7.2.0
 * WC requires at least: 3.5
 * Copyright: © 2020 FantasticPlugins
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit ; // Exit if accessed directly.
}

/* Include once will help to avoid fatal error by load the files when you call init hook */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ) ;


// Include main class file.
if ( ! class_exists( 'BCN_Coupon' ) ) {
	include_once( 'inc/class-birthday-coupon.php' ) ;
}

if ( ! function_exists( 'bcn_is_plugin_active' ) ) {

	/**
	 * Is plugin active?
	 * 
	 * @return bool
	 */
	function bcn_is_plugin_active() {
		if ( bcn_is_valid_wordpress_version() && bcn_is_woocommerce_active() && bcn_is_valid_woocommerce_version() ) {
			return true ;
		}

		add_action( 'admin_notices', 'bcn_display_warning_message' ) ;

		return false ;
	}

}

if ( ! function_exists( 'bcn_is_woocommerce_active' ) ) {

	/**
	 * Function to check whether WooCommerce is active or not.
	 * 
	 * @return bool
	 */
	function bcn_is_woocommerce_active() {
		$return = true ;
		// This condition is for multi site installation.
		if ( is_multisite() && ! is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) && ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			$return = false ;
			// This condition is for single site installation.
		} elseif ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			$return = false ;
		}

		return $return ;
	}

}

if ( ! function_exists( 'bcn_is_valid_wordpress_version' ) ) {

	/**
	 * Is valid WordPress version?
	 * 
	 * @return bool
	 */
	function bcn_is_valid_wordpress_version() {
		if ( version_compare( get_bloginfo( 'version' ), BCN_Coupon::$wp_minimum_version, '<' ) ) {
			return false ;
		}

		return true ;
	}

}

if ( ! function_exists( 'bcn_is_valid_woocommerce_version' ) ) {

	/**
	 * Is valid WooCommerce version?
	 * 
	 * @return bool
	 */
	function bcn_is_valid_woocommerce_version() {
		if ( version_compare( get_option( 'woocommerce_version' ), BCN_Coupon::$wc_minimum_version, '<' ) ) {
			return false ;
		}

		return true ;
	}

}

if ( ! function_exists( 'bcn_display_warning_message' ) ) {

	/**
	 * Display the WooCommere is not active warning message.
	 */
	function bcn_display_warning_message() {
		$notice = '' ;

		if ( ! bcn_is_valid_wordpress_version() ) {
			$notice = sprintf( 'This version of Birthday Coupons for WooCommerce requires WordPress %1s or newer.', BCN_Coupon::$wp_minimum_version ) ;
		} elseif ( ! bcn_is_woocommerce_active() ) {
			$notice = 'Birthday Coupons for WooCommerce Plugin will not work until WooCommerce Plugin is Activated. Please Activate the WooCommerce Plugin.' ;
		} elseif ( ! bcn_is_valid_woocommerce_version() ) {
			$notice = sprintf( 'This version of Birthday Coupons for WooCommerce requires WooCommerce %1s or newer.', BCN_Coupon::$wc_minimum_version ) ;
		}

		if ( $notice ) {
			echo '<div class="error">' ;
			echo '<p>' . wp_kses_post( $notice ) . '</p>' ;
			echo '</div>' ;
		}
	}

}

// Check WooCommerce plugin is active.
if ( ! bcn_is_plugin_active() ) {
	return ;
}

// Define constant.
if ( ! defined( 'BCN_PLUGIN_FILE' ) ) {
	define( 'BCN_PLUGIN_FILE', __FILE__ ) ;
}


// Return Birthday Coupon class object.
if ( ! function_exists( 'BCN' ) ) {

	function BCN() {
		return BCN_Coupon::instance() ;
	}

}

// Initialize the plugin.
BCN() ;


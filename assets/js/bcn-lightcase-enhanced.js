
jQuery( function ( $ ) {
    'use strict' ;
    try {

        $( document.body ).on( 'bcn-enhanced-lightcase' , function ( ) {
            
            var lightcases = $( '.bcn-popup-more-info' ) ;
            if ( !lightcases.length ) {
                    return ;
            }

            $( '.bcn-popup-more-info' ).lightcase( {
                href : '#bcn_more_info_modal' ,
                onFinish : {
                    foo : function () {
                        var data = ( {
                            action : 'bcn_additional_details' ,
                            post_id : $( this ).attr( 'data-postid' ) ,
                            bcn_security : bcn_lightcase_param.bcn_info_nonce ,
                        } ) ;

                        $.post( bcn_lightcase_param.ajaxurl , data , function ( res ) {
                            if ( true === res.success ) {
                                $( '#bcn_more_info_modal' ).append( res.data.html ) ;
                                lightcase.resize() ;
                            } else {
                                alert( res.data.error ) ;
                            }
                        }
                        ) ;


                    }
                } ,
            } ) ;

        } ) ;

        $( document.body ).trigger( 'bcn-enhanced-lightcase' ) ;
    } catch ( err ) {
        window.console.log( err ) ;
    }

} ) ;

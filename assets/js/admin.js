jQuery( function ( $ ) {

    var BCN_Toggle = {
        init : function ( ) {
            this.trigger_on_page_load() ;

            //Rules Settings
            $( document ).on( 'click' , ".bcn-rules-content h3" , this.toggle_section ) ;
            $( document ).on( 'change' , ".bcn-product-filter-type" , this.product_filter ) ;
            $( document ).on( 'change' , ".bcn-user-filter-type" , this.user_filter ) ;
            $( document ).on( 'change' , ".bcn-purchase-history" , this.purchase_history ) ;
            $( document ).on( 'change' , ".bcn-purchase-history-type" , this.purchase_history_type ) ;
            $( document ).on( 'change' , ".bcn-coupon-type" , this.coupon_type ) ;

            //General
            $( document ).on( 'change' , '#bcn_general_send_coupon_on' , this.send_coupon_on ) ;
            
            //Localization
            $( document ).on( 'change' , "#bcn_localization_enable_reason" , this.enable_reason ) ;

            //Delete Unsubscriber.
            $( document ).on( 'click' , ".bcn-delete-unsubscriber" , this.delete_unsubscriber ) ;
        } ,

        trigger_on_page_load : function () {
            BCN_Toggle.toggle_send_coupon_on( '#bcn_general_send_coupon_on' ) ;
            BCN_Toggle.toggle_enable_reason( '#bcn_localization_enable_reason' ) ;
            $( '.bcn-product-filter-type' ).each( function () {
                BCN_Toggle.toggle_product_filter( $( this ) ) ;
            } ) ;
            $( '.bcn-user-filter-type' ).each( function () {
                BCN_Toggle.toggle_user_filter( $( this ) ) ;
            } ) ;
            $( '.bcn-purchase-history' ).each( function () {
                BCN_Toggle.toggle_purchase_history( $( this ) ) ;
            } ) ;
            $( '.bcn-coupon-type' ).each( function () {
                BCN_Toggle.toggle_coupon_type( $( this ) ) ;
            } ) ;
        } ,
        toggle_section : function () {
            $( this ).nextUntil( 'h3' ).toggle() ;
        } ,
        product_filter : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_product_filter( $this ) ;
        } ,
        toggle_product_filter : function ( $this ) {
            if ( $( $this ).val() == '1' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-category' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-category' ).hide() ;
            } else if ( $( $this ).val() == '2' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-product' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-category' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-category' ).hide() ;
            } else if ( $( $this ).val() == '3' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-product' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-category' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-category' ).hide() ;
            } else if ( $( $this ).val() == '4' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-category' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-category' ).hide() ;
            } else {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-product' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-category' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-category' ).show() ;
            }
        } ,
        user_filter : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_user_filter( $this ) ;
        } ,
        toggle_user_filter : function ( $this ) {
            if ( $( $this ).val() == '1' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-userrole' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-userrole' ).hide() ;
            } else if ( $( $this ).val() == '2' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-user' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-userrole' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-userrole' ).hide() ;
            } else if ( $( $this ).val() == '3' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-user' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-userrole' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-userrole' ).hide() ;
            } else if ( $( $this ).val() == '4' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-userrole' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-userrole' ).hide() ;
            } else {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-user' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-inc-userrole' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-exc-userrole' ).show() ;
            }
        } ,
        purchase_history : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_purchase_history( $this ) ;
        } ,
        toggle_purchase_history : function ( $this ) {
            if ( $( $this ).is( ':checked' ) == true ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-issue-coupon' ).show() ;
                $( '.bcn-purchase-history-type' ).each( function () {
                    BCN_Toggle.toggle_purchase_history_type( $( this ) ) ;
                } ) ;
            } else {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-issue-coupon' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-no-of-order' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-total-amount-spent' ).hide() ;
            }
        } ,
        purchase_history_type : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_purchase_history_type( $this ) ;
        } ,
        toggle_purchase_history_type : function ( $this ) {
            if ( $( $this ).val() == '1' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-no-of-order' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-total-amount-spent' ).hide() ;
            } else {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-no-of-order' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-total-amount-spent' ).show() ;
            }
        } ,
        coupon_type : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_coupon_type( $this ) ;
        } ,
        toggle_coupon_type : function ( $this ) {
            if ( $( $this ).val() == 'percent' ) {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-coupon-percent' ).show() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-coupon-fixed' ).hide() ;
            } else {
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-coupon-percent' ).hide() ;
                $( $this ).closest( '.bcn-rule-fields' ).find( '.bcn-coupon-fixed' ).show() ;
            }
        } ,
        enable_reason : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_enable_reason( $this ) ;
        } ,
        toggle_enable_reason : function ( $this ) {
            if ( $( $this ).is(':checked') == true) {
                $( '#bcn_localization_birthday_field_reason_label' ).closest( 'tr' ).show() ;
            } else {
                $( '#bcn_localization_birthday_field_reason_label' ).closest( 'tr' ).hide() ;
            }
        } ,
        send_coupon_on : function ( e ) {
            e.preventDefault() ;
            var $this = $( e.currentTarget ) ;
            BCN_Toggle.toggle_send_coupon_on( $this ) ;
        } ,
        toggle_send_coupon_on : function ( $this ) {
            if ( $( $this ).val() == '1' ) {
                $( '#bcn_general_send_coupon_before_birthday' ).closest( 'tr' ).hide() ;
                $( '#bcn_general_send_coupon_after_birthday' ).closest( 'tr' ).hide() ;
            } else if ( $( $this ).val() == '2' ) {
                $( '#bcn_general_send_coupon_before_birthday' ).closest( 'tr' ).show() ;
                $( '#bcn_general_send_coupon_after_birthday' ).closest( 'tr' ).hide() ;
            } else {
                $( '#bcn_general_send_coupon_before_birthday' ).closest( 'tr' ).hide() ;
                $( '#bcn_general_send_coupon_after_birthday' ).closest( 'tr' ).show() ;
            }
        } ,
        delete_unsubscriber : function ( e ) {
            e.preventDefault() ;
            if ( confirm( bcn_admin_param.unsubscribe_alert ) ) {
                var $this = $( e.currentTarget ) ;
                $( $this ).closest( 'tr' ).remove() ;
                var data = {
                    action : 'bcn_remove_unsubscriber' ,
                    unsubid : $( this ).attr( 'data-unsubid' ) ,
                    bcn_security : bcn_admin_param.unsubscribe_nonce
                } ;

                $.post( ajaxurl , data , function ( response ) {
                    if ( true === response.success ) {
                        $( $this ).closest( 'tr' ).remove() ;
                    } else {
                        window.alert( response.data.error ) ;
                    }
                } ) ;
            }
        } ,
        block : function ( id ) {
            $( id ).block( {
                message : null ,
                overlayCSS : {
                    background : '#fff' ,
                    opacity : 0.6
                }
            } ) ;
        } ,
        unblock : function ( id ) {
            $( id ).unblock() ;
        } ,
    } ;
    BCN_Toggle.init( ) ;
} ) ;

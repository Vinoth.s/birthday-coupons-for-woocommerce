/* global bcn_enhanced_select_params */

jQuery( function ( $ ) {
    'use strict' ;

    try {
        $( document.body ).on( 'bcn-enhanced-init' , function () {
            if ( $( 'select.bcn_select2' ).length ) {
                //Select2 with customization
                $( 'select.bcn_select2' ).each( function () {
                    var select2_args = {
                        allowClear : $( this ).data( 'allow_clear' ) ? true : false ,
                        placeholder : $( this ).data( 'placeholder' ) ,
                        minimumResultsForSearch : 10 ,
                    } ;
                    $( this ).select2( select2_args ) ;
                } ) ;
            }
            if ( $( 'select.bcn_select2_search' ).length ) {
                //Multiple select with ajax search
                $( 'select.bcn_select2_search' ).each( function () {
                    var select2_args = {
                        allowClear : $( this ).data( 'allow_clear' ) ? true : false ,
                        placeholder : $( this ).data( 'placeholder' ) ,
                        minimumInputLength : $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : 3 ,
                        escapeMarkup : function ( m ) {
                            return m ;
                        } ,
                        ajax : {
                            url : bcn_enhanced_select_params.ajaxurl ,
                            dataType : 'json' ,
                            delay : 250 ,
                            data : function ( params ) {
                                return {
                                    term : params.term ,
                                    action : $( this ).data( 'action' ) ? $( this ).data( 'action' ) : '' ,
                                    bcn_security : $( this ).data( 'nonce' ) ? $( this ).data( 'nonce' ) : bcn_enhanced_select_params.search_nonce ,
                                } ;
                            } ,
                            processResults : function ( data ) {
                                var terms = [ ] ;
                                if ( data ) {
                                    $.each( data , function ( id , term ) {
                                        terms.push( {
                                            id : id ,
                                            text : term
                                        } ) ;
                                    } ) ;
                                }
                                return {
                                    results : terms
                                } ;
                            } ,
                            cache : true
                        }
                    } ;

                    $( this ).select2( select2_args ) ;
                } ) ;
            }

            if ( $( '.bcn_from_date' ).length ) {
                $( '.bcn_from_date' ).each( function ( ) {

                    $( this ).datepicker( {
                        altField : $( this ).next( ".bcn_alter_datepicker_value" ) ,
                        altFormat : 'yy-mm-dd' ,
                        changeMonth : true ,
                        changeYear : true ,
                        onClose : function ( selectedDate ) {
                            var maxDate = new Date( Date.parse( selectedDate ) ) ;
                            maxDate.setDate( maxDate.getDate() ) ;
                            $( '.bcn_to_date' ).datepicker( 'option' , 'minDate' , maxDate ) ;
                        }
                    } ) ;

                } ) ;
            }

            if ( $( '.bcn_to_date' ).length ) {
                $( '.bcn_to_date' ).each( function ( ) {

                    $( this ).datepicker( {
                        altField : $( this ).next( ".bcn_alter_datepicker_value" ) ,
                        altFormat : 'yy-mm-dd' ,
                        changeMonth : true ,
                        changeYear : true ,
                        onClose : function ( selectedDate ) {
                            $( '.bcn_from_date' ).datepicker( 'option' , 'maxDate' , selectedDate ) ;
                        }
                    } ) ;

                } ) ;
            }

            if ( $( '.bcn_datepicker' ).length ) {
                $( '.bcn_datepicker' ).on( 'change' , function ( ) {
                    if ( $( this ).val() === '' ) {
                        $( this ).next( ".bcn_alter_datepicker_value" ).val( '' ) ;
                    }
                } ) ;

                $( '.bcn_datepicker' ).each( function ( ) {
                    $( this ).datepicker( {
                        altField : $( this ).next( ".bcn_alter_datepicker_value" ) ,
                        altFormat : 'yy-mm-dd' ,
                        changeMonth : true ,
                        changeYear : true ,
                        yearRange: "-100:+0"
                    } ) ;
                } ) ;
            }
        } ) ;

        $( document.body ).trigger( 'bcn-enhanced-init' ) ;
    } catch ( err ) {
        window.console.log( err ) ;
    }

} ) ;
